﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Windows.Forms;
using Homework.Properties;
using LogicLayer;
namespace Homework
{
    public partial class Signup : Form
    {
        Logic logic = new Logic();
        public Color makecolor;
        public Signup()
        {
            InitializeComponent();
        }
        #region//Dragable Codes
        private bool _Dragging = false;
        private Point _start_point = new Point(0, 0);
        private void MouseToDown(object sender, MouseEventArgs e)
        {
            _Dragging = true;
             _start_point = new Point(e.X, e.Y);
        }

        private void MouseToUp(object sender, MouseEventArgs e)
        {
            _Dragging = false;
        }

        private void MouseToMove(object sender, MouseEventArgs e)
        {
            if (_Dragging)
            {
                Point p = PointToScreen(e.Location);
                 Location = new Point(p.X - this._start_point.X, p.Y - this._start_point.Y);
            }
        }

        #endregion

        private void ButtonBack_Click(object sender, EventArgs e)
        {
            LoginForm login = new LoginForm();
                login.Show();
            this.Hide();
        }

        private void ButtonExit_Click(object sender, EventArgs e)
        {
           DialogResult result= MessageBox.Show("Do you really want to exit", "ALERT", MessageBoxButtons.YesNo);
            if (result==DialogResult.Yes)
            {
                Application.ExitThread();
                    Application.Exit();
            }
           else{}
        }

        private void ButtonSignup_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(textboxFirstName.Text)&& !string.IsNullOrEmpty(textBoxSurname.Text) && !string.IsNullOrEmpty(textBoxGmail.Text) && !string.IsNullOrEmpty(textBoxContact.Text) && !string.IsNullOrEmpty(textBoxPassword.Text) && !string.IsNullOrEmpty(textBoxRetypePassword.Text))
            {
                if (!string.IsNullOrWhiteSpace(textboxFirstName.Text)&& !string.IsNullOrWhiteSpace(textBoxSurname.Text) && !string.IsNullOrWhiteSpace(textBoxGmail.Text) && !string.IsNullOrWhiteSpace(textBoxContact.Text) && !string.IsNullOrWhiteSpace(textBoxPassword.Text) && !string.IsNullOrWhiteSpace(textBoxRetypePassword.Text))
                {
                    if (textBoxPassword.Text == textBoxRetypePassword.Text)
                    {
                        bool statement=logic.CreateAccount(textboxFirstName.Text,textBoxMiddelName.Text,textBoxSurname.Text,textBoxContact.Text,textBoxGmail.Text,textBoxUserNmae.Text, textBoxPassword.Text);
                        if (statement)
                        {
                            MessageBox.Show("Account created successfully.", "User account created");
                        }
                        else
                        {
                           MessageBox.Show("Server Down.", "Warning");
                        }
                        //After transaction
                        DialogResult result = MessageBox.Show("Do you want to login " + textboxFirstName.Text + " " + textBoxMiddelName.Text + " " + textBoxSurname.Text + " ?", "Account created Successfully", MessageBoxButtons.YesNo);
                        if (result == DialogResult.Yes)
                        {
                            int login = Convert.ToInt32(logic.GetAccess(textBoxUserNmae.Text, textBoxPassword.Text));
                            if (login != 0)
                            {
                                HomePage page = new HomePage(login,makecolor);
                                this.Hide();
                            }
                            else
                            {
                                MessageBox.Show("Server Down.", "Warning");
                            }
                        }
                        else
                        {}
                        textboxFirstName.Clear();
                            textBoxMiddelName.Clear();
                                textBoxUserNmae.Clear();
                            textBoxSurname.Clear();
                        textBoxGmail.Clear();
                            textBoxContact.Clear();
                                textBoxPassword.Clear();
                            textBoxRetypePassword.Clear();
                        textboxFirstName.Focus();
                    }
                    else
                    {
                        MessageBox.Show("Password not matched, Retype password.", "ALERT");
                            textBoxPassword.Clear();
                        textBoxRetypePassword.Clear();
                            textBoxPassword.Focus();
                    }
                }
                else
                {
                        MessageBox.Show("Please fill all the details. ", "ALERT");
                }
            }
            else
            {
                MessageBox.Show("Please fill all the details. ","ALERT");
            }
        }

        private void CheckboxShowHidePassword_CheckedChanged(object sender, EventArgs e)
        {
            if (CheckboxShowHidePassword.Checked)
            {
                textBoxPassword.UseSystemPasswordChar = false;
                    textBoxRetypePassword.UseSystemPasswordChar = false;
                CheckboxShowHidePassword.Image = Resources.Hide;
            }
            else
            {
                textBoxPassword.UseSystemPasswordChar = true;
                    textBoxRetypePassword.UseSystemPasswordChar = true;
                CheckboxShowHidePassword.Image = Resources.Show;
            }
        }

        private void TextboxFirstName_TextChanged(object sender, EventArgs e)
        {
            textBoxUserNmae.Text = textboxFirstName.Text;
        }
    }
}
