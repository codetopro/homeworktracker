﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using LogicLayer;

namespace Homework
{
    public partial class Homework : UserControl
    {
        Logic logic = new Logic();
        private string semesterId;
        int Complete;
        public Homework(string SemesterId)
        {
            InitializeComponent();
            semesterId = SemesterId;
            LoadSubjectCombobox();
            DisplayHomeworkData();
        }
        private void LoadSubjectCombobox()
        {
            try
            {
                string SQL = "SELECT  SubjectId,SubjectName FROM Subjects WHERE SemesterId=" + semesterId;
                var data = Logic.GetTableByQuery(SQL);
                comboBoxSubjectName.DataSource = data;
                comboBoxSubjectName.DisplayMember = "SubjectName";
                comboBoxSubjectName.ValueMember = "SubjectId";
                comboBoxSubjectName.SelectedIndex = -1;
            }
            catch (Exception)
            {
            }
            
        }
        private void DisplayHomeworkData()
        {
           DateTime date = Convert.ToDateTime(DateTime.Now.ToShortDateString());
           dataGridView.DataSource = Logic.DisplayHomeworkDetails(date);
           dataGridView.Columns[0].Visible = false;
        }

        private void ButtonRefresh_Click(object sender, EventArgs e)
        {
            DisplayHomeworkData();
            RESET();
        }
        private void RESET()
        {
            comboBoxSubjectName.SelectedIndex = -1;
            textBoxHomework.Clear();
            textboxHomeworkTitle.Clear();
            checkBoxIsCompleted.Checked = false;
            checkBoxTodaysHomework.Checked = false;
            dateTimePickerAssigned.ResetText();
            dateTimePickerCompleted.ResetText();
        }

        private void ButtonAddHomework_Click(object sender, EventArgs e)
        {
            if (comboBoxSubjectName.SelectedIndex!=-1 &&!string.IsNullOrEmpty(textboxHomeworkTitle.Text)&&!string.IsNullOrEmpty(textBoxHomework.Text))
            {
            bool ConditionCheck = logic.InsertHomework(Convert.ToInt32(comboBoxSubjectName.SelectedValue), textboxHomeworkTitle.Text,textBoxHomework.Text);
            if (ConditionCheck)
            {
                MessageBox.Show("Homework Added Successfully", "Homework Added");
            }
            else
            {
                MessageBox.Show("Server Down.", "Warning");
            }
            }
            else
            {
                MessageBox.Show("Please select the details");
            }
            DisplayHomeworkData();
            RESET();
        }

        private void ButtonUpdateHomework_Click(object sender, EventArgs e)
        {
            if (comboBoxSubjectName.SelectedIndex != -1 && !string.IsNullOrEmpty(textboxHomeworkTitle.Text) && !string.IsNullOrEmpty(textBoxHomework.Text))
            {
                int id = Convert.ToInt32(dataGridView.Rows[dataGridView.CurrentRow.Index].Cells[0].Value);
                DateTime date =Convert.ToDateTime(dateTimePickerCompleted.Value.ToShortDateString());
                if (checkBoxIsCompleted.Checked == true)
                {
                    Complete = 1;
                }
                else if(checkBoxIsCompleted.Checked == false)
                {
                    Complete = 0;
                }
                bool ConditionCheck = logic.UpdateHomework(Convert.ToInt32(comboBoxSubjectName.SelectedValue), textboxHomeworkTitle.Text, textBoxHomework.Text,date,Complete,id);
                if (ConditionCheck)
                {
                    MessageBox.Show("Homework Updated Successfully", "Homework Updated");
                }
                else
                {
                    MessageBox.Show("Server Down.", "Warning");
                }
            }
            else
            {
                MessageBox.Show("Please select the details");
            }
            DisplayHomeworkData();
            RESET();
        }

        private void CheckBoxTodaysHomework_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxTodaysHomework.Checked==true)
            {
                DateTime date = Convert.ToDateTime(DateTime.Now.ToShortDateString());
                dataGridView.DataSource = Logic.DisplayHomeworkDetails(date);
                dataGridView.Columns[0].Visible = false;
            }
            else if (checkBoxTodaysHomework.Checked==false)
            {
                DisplayHomeworkData();
            }
        }

        private void DataGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int id = Convert.ToInt32(dataGridView.Rows[dataGridView.CurrentRow.Index].Cells[0].Value);
            comboBoxSubjectName.Text = dataGridView.Rows[dataGridView.CurrentRow.Index].Cells[1].Value.ToString();
            textboxHomeworkTitle.Text= dataGridView.Rows[dataGridView.CurrentRow.Index].Cells[2].Value.ToString();
            dateTimePickerAssigned.Text= dataGridView.Rows[dataGridView.CurrentRow.Index].Cells[3].Value.ToString();
            dateTimePickerCompleted.Text= dataGridView.Rows[dataGridView.CurrentRow.Index].Cells[4].Value.ToString();
            string Homewor = "SELECT Homework FROM DailyHomeworks WHERE DailyHomeworksId=" +id;
            var data = Logic.GetTableByQuery(Homewor);
            textBoxHomework.Text= (data.Rows[0][0]).ToString();
            string IsCompleted = "SELECT IsCompleted FROM DailyHomeworks WHERE DailyHomeworksId=" + id;
            var dataM = Logic.GetTableByQuery(IsCompleted);
            int Complete= Convert.ToInt32(dataM.Rows[0][0]);
            if (Complete==1)
            {
                checkBoxIsCompleted.Checked = true;
            }
            else if(Complete==0)
            {
                checkBoxIsCompleted.Checked = false;
            }
        }
    }
}
