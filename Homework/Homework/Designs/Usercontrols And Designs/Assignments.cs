﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using LogicLayer;
using System.IO;

namespace Homework
{

    public partial class Assignments : UserControl
    {
       
        private string semesterId;
        int Complete;
        Logic logic = new Logic();
        string fileLocation, folderPath;
        public Assignments(string SemesterId)
        {
            InitializeComponent();
            semesterId = SemesterId;
            LoadSubjectCombobox();
            DisplayAssignmentData();
        }
        private void DisplayAssignmentData()
        {
            DateTime date = Convert.ToDateTime(DateTime.Now.ToShortDateString());
            dataGridViewAssignments.DataSource = Logic.DisplayAssignmentDetails(date);
            dataGridViewAssignments.Columns[0].Visible = false;
        }
        private void RESET()
        {
            comboBoxSubjectName.SelectedIndex = -1;
            textBoxAssignment.Clear();
            textboxAssignmentTitle.Clear();
            textBoxFileLocation.Clear();
            checkBoxIsCompleted.Checked = false;
            dateTimePickerAssigned.ResetText();
            dateTimePickerCompleted.ResetText();
        }

        private void LoadSubjectCombobox()
        {
            try
            {
                string SQL = "SELECT  SubjectId,SubjectName FROM Subjects WHERE SemesterId=" + semesterId;
                var data = Logic.GetTableByQuery(SQL);
                comboBoxSubjectName.DataSource = data;
                comboBoxSubjectName.DisplayMember = "SubjectName";
                comboBoxSubjectName.ValueMember = "SubjectId";
                comboBoxSubjectName.SelectedIndex = -1;
            }
            catch (Exception)
            {
            }
        }

        private void ButtonAddAssignment_Click(object sender, EventArgs e)
        {
            if (comboBoxSubjectName.SelectedIndex != -1 && !string.IsNullOrEmpty(textboxAssignmentTitle.Text) && !string.IsNullOrEmpty(textBoxAssignment.Text))
            {
                bool ConditionCheck = logic.InsertAssignment(Convert.ToInt32(comboBoxSubjectName.SelectedValue), textboxAssignmentTitle.Text, textBoxAssignment.Text);
                if (ConditionCheck)
                {
                    MessageBox.Show("Assignment Added Successfully", "Assignment Added");
                }
                else
                {
                    MessageBox.Show("Server Down.", "Warning");
                }
            }
            else
            {
                MessageBox.Show("Please select the details");
            }
            DisplayAssignmentData();
            RESET();
        }

        private void ButtonUpdateAssignment_Click(object sender, EventArgs e)
        {
            if (comboBoxSubjectName.SelectedIndex != -1 && !string.IsNullOrEmpty(textboxAssignmentTitle.Text) && !string.IsNullOrEmpty(textBoxAssignment.Text))
            {
                int id = Convert.ToInt32(dataGridViewAssignments.Rows[dataGridViewAssignments.CurrentRow.Index].Cells[0].Value);
                DateTime date = Convert.ToDateTime(dateTimePickerCompleted.Value.ToShortDateString());
                if (checkBoxIsCompleted.Checked == true)
                {
                    Complete = 1;
                }
                else if (checkBoxIsCompleted.Checked == false)
                {
                    Complete = 0;
                }
                bool ConditionCheck = logic.UpdateAssignment(Convert.ToInt32(comboBoxSubjectName.SelectedValue), textboxAssignmentTitle.Text, textBoxAssignment.Text, date, Complete, id,textBoxFileLocation.Text);
                if (ConditionCheck)
                {
                    MessageBox.Show("Assignment Updated Successfully", "Assignment Updated");
                }
                else
                {
                    MessageBox.Show("Server Down.", "Warning");
                }
            }
            else
            {
                MessageBox.Show("Please select the details");
            }
            DisplayAssignmentData();
            RESET();
        }

        private void DataGridViewAssignments_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int id = Convert.ToInt32(dataGridViewAssignments.Rows[dataGridViewAssignments.CurrentRow.Index].Cells[0].Value);
            comboBoxSubjectName.Text = dataGridViewAssignments.Rows[dataGridViewAssignments.CurrentRow.Index].Cells[1].Value.ToString();
            textboxAssignmentTitle.Text = dataGridViewAssignments.Rows[dataGridViewAssignments.CurrentRow.Index].Cells[2].Value.ToString();
            dateTimePickerAssigned.Text = dataGridViewAssignments.Rows[dataGridViewAssignments.CurrentRow.Index].Cells[3].Value.ToString();
            dateTimePickerCompleted.Text = dataGridViewAssignments.Rows[dataGridViewAssignments.CurrentRow.Index].Cells[4].Value.ToString();
            string Homewor = "SELECT Assignment FROM Assignment WHERE AssignmentId=" + id;
            var data = Logic.GetTableByQuery(Homewor);
            textBoxAssignment.Text = (data.Rows[0][0]).ToString();
            string IsCompleted = "SELECT IsCompleted FROM Assignment WHERE AssignmentId=" + id;
            var dataM = Logic.GetTableByQuery(IsCompleted);
            int Complete = Convert.ToInt32(dataM.Rows[0][0]);
            if (Complete == 1)
            {
                checkBoxIsCompleted.Checked = true;
            }
            else if (Complete == 0)
            {
                checkBoxIsCompleted.Checked = false;
            }
            string FileLocation = "SELECT FileLocation FROM Assignment WHERE AssignmentId =" + id;
            var dataa = Logic.GetTableByQuery(FileLocation);
            textBoxFileLocation.Text = (dataa.Rows[0][0]).ToString();

        }

        private void ButtonRefresh_Click(object sender, EventArgs e)
        {
            DateTime date = Convert.ToDateTime(DateTime.Now.ToShortDateString());
            dataGridViewAssignments.DataSource = Logic.DisplayAssignmentDetails(semesterId);
            dataGridViewAssignments.Columns[0].Visible = false;
            RESET();
        }

        private void ButtonFileLocation_Click(object sender, EventArgs e)
        {
            OpenFileDialog Dialog = new OpenFileDialog();
            Dialog.Title = "Select file to be upload";
            if (Dialog.ShowDialog() == DialogResult.OK)
            {
                    fileLocation = Dialog.FileName.ToString();
                    
                    folderPath = @"D:\Homework\AssignmentFiles\";
                    if (!Directory.Exists(folderPath))
                    {
                        Directory.CreateDirectory(folderPath);
                    }
                    File.Copy(fileLocation, Path.Combine(folderPath, Path.GetFileName(fileLocation)), true);
                    textBoxFileLocation.Text = (folderPath + Path.GetFileName(fileLocation)).ToString();
            }
        }
    }
}
