﻿namespace Homework
{
    partial class Homework
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            this.labelSubjectName = new System.Windows.Forms.Label();
            this.comboBoxSubjectName = new System.Windows.Forms.ComboBox();
            this.textboxHomeworkTitle = new System.Windows.Forms.TextBox();
            this.labelHomeworkTitle = new System.Windows.Forms.Label();
            this.labelAssignedDate = new System.Windows.Forms.Label();
            this.dateTimePickerAssigned = new System.Windows.Forms.DateTimePicker();
            this.dateTimePickerCompleted = new System.Windows.Forms.DateTimePicker();
            this.labelCompletedDate = new System.Windows.Forms.Label();
            this.labelid = new System.Windows.Forms.Label();
            this.checkBoxIsCompleted = new System.Windows.Forms.CheckBox();
            this.labelHomework = new System.Windows.Forms.Label();
            this.textBoxHomework = new System.Windows.Forms.TextBox();
            this.labelHomeworkDetails = new System.Windows.Forms.Label();
            this.dataGridView = new System.Windows.Forms.DataGridView();
            this.buttonAddHomework = new System.Windows.Forms.Button();
            this.buttonUpdateHomework = new System.Windows.Forms.Button();
            this.buttonRefresh = new System.Windows.Forms.Button();
            this.groupBox = new System.Windows.Forms.GroupBox();
            this.checkBoxTodaysHomework = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).BeginInit();
            this.groupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // labelSubjectName
            // 
            this.labelSubjectName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelSubjectName.AutoSize = true;
            this.labelSubjectName.Font = new System.Drawing.Font("Century Gothic", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSubjectName.ForeColor = System.Drawing.Color.Black;
            this.labelSubjectName.Location = new System.Drawing.Point(36, 13);
            this.labelSubjectName.Name = "labelSubjectName";
            this.labelSubjectName.Size = new System.Drawing.Size(141, 23);
            this.labelSubjectName.TabIndex = 15;
            this.labelSubjectName.Text = "Subject Name";
            // 
            // comboBoxSubjectName
            // 
            this.comboBoxSubjectName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxSubjectName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxSubjectName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxSubjectName.FormattingEnabled = true;
            this.comboBoxSubjectName.Location = new System.Drawing.Point(40, 53);
            this.comboBoxSubjectName.Name = "comboBoxSubjectName";
            this.comboBoxSubjectName.Size = new System.Drawing.Size(394, 33);
            this.comboBoxSubjectName.Sorted = true;
            this.comboBoxSubjectName.TabIndex = 16;
            // 
            // textboxHomeworkTitle
            // 
            this.textboxHomeworkTitle.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textboxHomeworkTitle.BackColor = System.Drawing.SystemColors.Control;
            this.textboxHomeworkTitle.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textboxHomeworkTitle.ForeColor = System.Drawing.Color.Black;
            this.textboxHomeworkTitle.Location = new System.Drawing.Point(509, 53);
            this.textboxHomeworkTitle.MaxLength = 70;
            this.textboxHomeworkTitle.Name = "textboxHomeworkTitle";
            this.textboxHomeworkTitle.Size = new System.Drawing.Size(394, 32);
            this.textboxHomeworkTitle.TabIndex = 18;
            // 
            // labelHomeworkTitle
            // 
            this.labelHomeworkTitle.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelHomeworkTitle.AutoSize = true;
            this.labelHomeworkTitle.Font = new System.Drawing.Font("Century Gothic", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelHomeworkTitle.ForeColor = System.Drawing.Color.Black;
            this.labelHomeworkTitle.Location = new System.Drawing.Point(505, 13);
            this.labelHomeworkTitle.Name = "labelHomeworkTitle";
            this.labelHomeworkTitle.Size = new System.Drawing.Size(148, 23);
            this.labelHomeworkTitle.TabIndex = 17;
            this.labelHomeworkTitle.Text = "Homework title";
            // 
            // labelAssignedDate
            // 
            this.labelAssignedDate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelAssignedDate.AutoSize = true;
            this.labelAssignedDate.Font = new System.Drawing.Font("Century Gothic", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelAssignedDate.ForeColor = System.Drawing.Color.Black;
            this.labelAssignedDate.Location = new System.Drawing.Point(36, 113);
            this.labelAssignedDate.Name = "labelAssignedDate";
            this.labelAssignedDate.Size = new System.Drawing.Size(143, 23);
            this.labelAssignedDate.TabIndex = 19;
            this.labelAssignedDate.Text = "Assigned Date";
            // 
            // dateTimePickerAssigned
            // 
            this.dateTimePickerAssigned.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dateTimePickerAssigned.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.dateTimePickerAssigned.Location = new System.Drawing.Point(40, 155);
            this.dateTimePickerAssigned.Name = "dateTimePickerAssigned";
            this.dateTimePickerAssigned.Size = new System.Drawing.Size(394, 32);
            this.dateTimePickerAssigned.TabIndex = 20;
            // 
            // dateTimePickerCompleted
            // 
            this.dateTimePickerCompleted.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dateTimePickerCompleted.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.dateTimePickerCompleted.Location = new System.Drawing.Point(509, 155);
            this.dateTimePickerCompleted.Name = "dateTimePickerCompleted";
            this.dateTimePickerCompleted.Size = new System.Drawing.Size(394, 32);
            this.dateTimePickerCompleted.TabIndex = 22;
            // 
            // labelCompletedDate
            // 
            this.labelCompletedDate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelCompletedDate.AutoSize = true;
            this.labelCompletedDate.Font = new System.Drawing.Font("Century Gothic", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCompletedDate.ForeColor = System.Drawing.Color.Black;
            this.labelCompletedDate.Location = new System.Drawing.Point(505, 113);
            this.labelCompletedDate.Name = "labelCompletedDate";
            this.labelCompletedDate.Size = new System.Drawing.Size(165, 23);
            this.labelCompletedDate.TabIndex = 21;
            this.labelCompletedDate.Text = "Completed Date";
            // 
            // labelid
            // 
            this.labelid.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelid.AutoSize = true;
            this.labelid.Location = new System.Drawing.Point(921, 0);
            this.labelid.Name = "labelid";
            this.labelid.Size = new System.Drawing.Size(19, 17);
            this.labelid.TabIndex = 23;
            this.labelid.Text = "id";
            this.labelid.Visible = false;
            // 
            // checkBoxIsCompleted
            // 
            this.checkBoxIsCompleted.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBoxIsCompleted.AutoSize = true;
            this.checkBoxIsCompleted.BackColor = System.Drawing.Color.Transparent;
            this.checkBoxIsCompleted.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxIsCompleted.ForeColor = System.Drawing.Color.Black;
            this.checkBoxIsCompleted.Location = new System.Drawing.Point(509, 212);
            this.checkBoxIsCompleted.Name = "checkBoxIsCompleted";
            this.checkBoxIsCompleted.Size = new System.Drawing.Size(237, 29);
            this.checkBoxIsCompleted.TabIndex = 24;
            this.checkBoxIsCompleted.Text = "Assignment Completed";
            this.checkBoxIsCompleted.UseVisualStyleBackColor = false;
            // 
            // labelHomework
            // 
            this.labelHomework.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.labelHomework.AutoSize = true;
            this.labelHomework.Font = new System.Drawing.Font("Century Gothic", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelHomework.ForeColor = System.Drawing.Color.Black;
            this.labelHomework.Location = new System.Drawing.Point(36, 212);
            this.labelHomework.Name = "labelHomework";
            this.labelHomework.Size = new System.Drawing.Size(109, 23);
            this.labelHomework.TabIndex = 25;
            this.labelHomework.Text = "Homework";
            // 
            // textBoxHomework
            // 
            this.textBoxHomework.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.textBoxHomework.BackColor = System.Drawing.SystemColors.Control;
            this.textBoxHomework.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxHomework.ForeColor = System.Drawing.Color.Black;
            this.textBoxHomework.Location = new System.Drawing.Point(40, 241);
            this.textBoxHomework.Multiline = true;
            this.textBoxHomework.Name = "textBoxHomework";
            this.textBoxHomework.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxHomework.Size = new System.Drawing.Size(446, 190);
            this.textBoxHomework.TabIndex = 26;
            // 
            // labelHomeworkDetails
            // 
            this.labelHomeworkDetails.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelHomeworkDetails.AutoSize = true;
            this.labelHomeworkDetails.Font = new System.Drawing.Font("Century Gothic", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelHomeworkDetails.ForeColor = System.Drawing.Color.Black;
            this.labelHomeworkDetails.Location = new System.Drawing.Point(39, 445);
            this.labelHomeworkDetails.Name = "labelHomeworkDetails";
            this.labelHomeworkDetails.Size = new System.Drawing.Size(176, 23);
            this.labelHomeworkDetails.TabIndex = 28;
            this.labelHomeworkDetails.Text = "Homework Details";
            // 
            // dataGridView
            // 
            this.dataGridView.AllowDrop = true;
            this.dataGridView.AllowUserToAddRows = false;
            this.dataGridView.AllowUserToDeleteRows = false;
            this.dataGridView.AllowUserToOrderColumns = true;
            this.dataGridView.AllowUserToResizeColumns = false;
            this.dataGridView.AllowUserToResizeRows = false;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.Transparent;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.GradientActiveCaption;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle6;
            this.dataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridView.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridView.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.RaisedHorizontal;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.Transparent;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.Transparent;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView.DefaultCellStyle = dataGridViewCellStyle8;
            this.dataGridView.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.dataGridView.Location = new System.Drawing.Point(0, 486);
            this.dataGridView.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dataGridView.MultiSelect = false;
            this.dataGridView.Name = "dataGridView";
            this.dataGridView.ReadOnly = true;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.Color.Transparent;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView.RowHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.dataGridView.RowHeadersVisible = false;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle10.BackColor = System.Drawing.Color.Transparent;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.Color.Black;
            this.dataGridView.RowsDefaultCellStyle = dataGridViewCellStyle10;
            this.dataGridView.RowTemplate.DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridView.RowTemplate.DefaultCellStyle.BackColor = System.Drawing.Color.White;
            this.dataGridView.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataGridView.RowTemplate.DefaultCellStyle.ForeColor = System.Drawing.Color.Black;
            this.dataGridView.RowTemplate.DefaultCellStyle.SelectionBackColor = System.Drawing.Color.Black;
            this.dataGridView.RowTemplate.DefaultCellStyle.SelectionForeColor = System.Drawing.Color.White;
            this.dataGridView.RowTemplate.Height = 24;
            this.dataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView.Size = new System.Drawing.Size(943, 287);
            this.dataGridView.StandardTab = true;
            this.dataGridView.TabIndex = 29;
            this.dataGridView.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridView_CellClick);
            // 
            // buttonAddHomework
            // 
            this.buttonAddHomework.BackColor = System.Drawing.Color.White;
            this.buttonAddHomework.Dock = System.Windows.Forms.DockStyle.Top;
            this.buttonAddHomework.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonAddHomework.Font = new System.Drawing.Font("Century Gothic", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonAddHomework.Location = new System.Drawing.Point(3, 18);
            this.buttonAddHomework.Name = "buttonAddHomework";
            this.buttonAddHomework.Size = new System.Drawing.Size(388, 37);
            this.buttonAddHomework.TabIndex = 30;
            this.buttonAddHomework.Text = "Add Homework";
            this.buttonAddHomework.UseVisualStyleBackColor = false;
            this.buttonAddHomework.Click += new System.EventHandler(this.ButtonAddHomework_Click);
            // 
            // buttonUpdateHomework
            // 
            this.buttonUpdateHomework.BackColor = System.Drawing.Color.White;
            this.buttonUpdateHomework.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.buttonUpdateHomework.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonUpdateHomework.Font = new System.Drawing.Font("Century Gothic", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonUpdateHomework.Location = new System.Drawing.Point(3, 96);
            this.buttonUpdateHomework.Name = "buttonUpdateHomework";
            this.buttonUpdateHomework.Size = new System.Drawing.Size(388, 39);
            this.buttonUpdateHomework.TabIndex = 31;
            this.buttonUpdateHomework.Text = "Update Homework";
            this.buttonUpdateHomework.UseVisualStyleBackColor = false;
            this.buttonUpdateHomework.Click += new System.EventHandler(this.ButtonUpdateHomework_Click);
            // 
            // buttonRefresh
            // 
            this.buttonRefresh.BackColor = System.Drawing.Color.White;
            this.buttonRefresh.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonRefresh.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonRefresh.Font = new System.Drawing.Font("Century Gothic", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonRefresh.Location = new System.Drawing.Point(3, 55);
            this.buttonRefresh.Name = "buttonRefresh";
            this.buttonRefresh.Size = new System.Drawing.Size(388, 41);
            this.buttonRefresh.TabIndex = 32;
            this.buttonRefresh.Text = "Refresh";
            this.buttonRefresh.UseVisualStyleBackColor = false;
            this.buttonRefresh.Click += new System.EventHandler(this.ButtonRefresh_Click);
            // 
            // groupBox
            // 
            this.groupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox.Controls.Add(this.buttonRefresh);
            this.groupBox.Controls.Add(this.buttonUpdateHomework);
            this.groupBox.Controls.Add(this.buttonAddHomework);
            this.groupBox.Location = new System.Drawing.Point(509, 279);
            this.groupBox.Name = "groupBox";
            this.groupBox.Size = new System.Drawing.Size(394, 138);
            this.groupBox.TabIndex = 33;
            this.groupBox.TabStop = false;
            // 
            // checkBoxTodaysHomework
            // 
            this.checkBoxTodaysHomework.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBoxTodaysHomework.AutoSize = true;
            this.checkBoxTodaysHomework.BackColor = System.Drawing.Color.Transparent;
            this.checkBoxTodaysHomework.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxTodaysHomework.ForeColor = System.Drawing.Color.Black;
            this.checkBoxTodaysHomework.Location = new System.Drawing.Point(701, 439);
            this.checkBoxTodaysHomework.Name = "checkBoxTodaysHomework";
            this.checkBoxTodaysHomework.Size = new System.Drawing.Size(202, 29);
            this.checkBoxTodaysHomework.TabIndex = 34;
            this.checkBoxTodaysHomework.Text = "Today\'s Homework";
            this.checkBoxTodaysHomework.UseVisualStyleBackColor = false;
            this.checkBoxTodaysHomework.CheckedChanged += new System.EventHandler(this.CheckBoxTodaysHomework_CheckedChanged);
            // 
            // Homework
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.checkBoxTodaysHomework);
            this.Controls.Add(this.groupBox);
            this.Controls.Add(this.dataGridView);
            this.Controls.Add(this.labelHomeworkDetails);
            this.Controls.Add(this.textBoxHomework);
            this.Controls.Add(this.labelHomework);
            this.Controls.Add(this.checkBoxIsCompleted);
            this.Controls.Add(this.labelid);
            this.Controls.Add(this.dateTimePickerCompleted);
            this.Controls.Add(this.labelCompletedDate);
            this.Controls.Add(this.dateTimePickerAssigned);
            this.Controls.Add(this.labelAssignedDate);
            this.Controls.Add(this.textboxHomeworkTitle);
            this.Controls.Add(this.labelHomeworkTitle);
            this.Controls.Add(this.comboBoxSubjectName);
            this.Controls.Add(this.labelSubjectName);
            this.DoubleBuffered = true;
            this.Name = "Homework";
            this.Size = new System.Drawing.Size(943, 773);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).EndInit();
            this.groupBox.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label labelSubjectName;
        public System.Windows.Forms.ComboBox comboBoxSubjectName;
        public System.Windows.Forms.TextBox textboxHomeworkTitle;
        private System.Windows.Forms.Label labelHomeworkTitle;
        private System.Windows.Forms.Label labelAssignedDate;
        private System.Windows.Forms.DateTimePicker dateTimePickerAssigned;
        private System.Windows.Forms.DateTimePicker dateTimePickerCompleted;
        private System.Windows.Forms.Label labelCompletedDate;
        public System.Windows.Forms.Label labelid;
        public System.Windows.Forms.CheckBox checkBoxIsCompleted;
        private System.Windows.Forms.Label labelHomework;
        public System.Windows.Forms.TextBox textBoxHomework;
        private System.Windows.Forms.Label labelHomeworkDetails;
        public System.Windows.Forms.DataGridView dataGridView;
        private System.Windows.Forms.Button buttonAddHomework;
        private System.Windows.Forms.Button buttonUpdateHomework;
        private System.Windows.Forms.Button buttonRefresh;
        private System.Windows.Forms.GroupBox groupBox;
        public System.Windows.Forms.CheckBox checkBoxTodaysHomework;
    }
}
