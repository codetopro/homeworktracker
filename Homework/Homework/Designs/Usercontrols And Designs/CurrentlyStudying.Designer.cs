﻿namespace Homework
{
    partial class CurrentlyStudying
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupBoxHomework = new System.Windows.Forms.GroupBox();
            this.textBoxHomeworkSubjectName = new System.Windows.Forms.TextBox();
            this.dataGridViewHomework = new System.Windows.Forms.DataGridView();
            this.labelHomeworkDetails = new System.Windows.Forms.Label();
            this.dateTimePickerAssignedHomework = new System.Windows.Forms.DateTimePicker();
            this.labelAssignedDate = new System.Windows.Forms.Label();
            this.textboxHomeworkTitle = new System.Windows.Forms.TextBox();
            this.labelHomeworkTitle = new System.Windows.Forms.Label();
            this.labelSubjectName = new System.Windows.Forms.Label();
            this.groupBoxAssignment = new System.Windows.Forms.GroupBox();
            this.textBoxAssignmentSubjectName = new System.Windows.Forms.TextBox();
            this.dataGridViewAssignments = new System.Windows.Forms.DataGridView();
            this.labelAssignmentDetails = new System.Windows.Forms.Label();
            this.dateTimePickerAssignedAssignment = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxAssignmentTitle = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.checkBoxIsHomework = new System.Windows.Forms.CheckBox();
            this.checkBoxIsAssignment = new System.Windows.Forms.CheckBox();
            this.labelid = new System.Windows.Forms.Label();
            this.groupBoxHomework.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewHomework)).BeginInit();
            this.groupBoxAssignment.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewAssignments)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBoxHomework
            // 
            this.groupBoxHomework.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBoxHomework.Controls.Add(this.textBoxHomeworkSubjectName);
            this.groupBoxHomework.Controls.Add(this.dataGridViewHomework);
            this.groupBoxHomework.Controls.Add(this.labelHomeworkDetails);
            this.groupBoxHomework.Controls.Add(this.dateTimePickerAssignedHomework);
            this.groupBoxHomework.Controls.Add(this.labelAssignedDate);
            this.groupBoxHomework.Controls.Add(this.textboxHomeworkTitle);
            this.groupBoxHomework.Controls.Add(this.labelHomeworkTitle);
            this.groupBoxHomework.Controls.Add(this.labelSubjectName);
            this.groupBoxHomework.Location = new System.Drawing.Point(3, 47);
            this.groupBoxHomework.Name = "groupBoxHomework";
            this.groupBoxHomework.Size = new System.Drawing.Size(551, 695);
            this.groupBoxHomework.TabIndex = 0;
            this.groupBoxHomework.TabStop = false;
            // 
            // textBoxHomeworkSubjectName
            // 
            this.textBoxHomeworkSubjectName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxHomeworkSubjectName.BackColor = System.Drawing.SystemColors.Control;
            this.textBoxHomeworkSubjectName.Cursor = System.Windows.Forms.Cursors.Default;
            this.textBoxHomeworkSubjectName.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxHomeworkSubjectName.ForeColor = System.Drawing.Color.Black;
            this.textBoxHomeworkSubjectName.Location = new System.Drawing.Point(10, 60);
            this.textBoxHomeworkSubjectName.MaxLength = 40;
            this.textBoxHomeworkSubjectName.Name = "textBoxHomeworkSubjectName";
            this.textBoxHomeworkSubjectName.ReadOnly = true;
            this.textBoxHomeworkSubjectName.Size = new System.Drawing.Size(506, 32);
            this.textBoxHomeworkSubjectName.TabIndex = 32;
            // 
            // dataGridViewHomework
            // 
            this.dataGridViewHomework.AllowDrop = true;
            this.dataGridViewHomework.AllowUserToAddRows = false;
            this.dataGridViewHomework.AllowUserToDeleteRows = false;
            this.dataGridViewHomework.AllowUserToOrderColumns = true;
            this.dataGridViewHomework.AllowUserToResizeColumns = false;
            this.dataGridViewHomework.AllowUserToResizeRows = false;
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle11.BackColor = System.Drawing.Color.Transparent;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle11.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.GradientActiveCaption;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewHomework.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle11;
            this.dataGridViewHomework.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewHomework.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridViewHomework.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridViewHomework.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridViewHomework.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.RaisedHorizontal;
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle12.BackColor = System.Drawing.Color.Transparent;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle12.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewHomework.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle12;
            this.dataGridViewHomework.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dataGridViewHomework.ColumnHeadersVisible = false;
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle13.BackColor = System.Drawing.Color.Transparent;
            dataGridViewCellStyle13.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle13.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle13.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle13.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewHomework.DefaultCellStyle = dataGridViewCellStyle13;
            this.dataGridViewHomework.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.dataGridViewHomework.Location = new System.Drawing.Point(3, 349);
            this.dataGridViewHomework.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dataGridViewHomework.MultiSelect = false;
            this.dataGridViewHomework.Name = "dataGridViewHomework";
            this.dataGridViewHomework.ReadOnly = true;
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle14.BackColor = System.Drawing.Color.Transparent;
            dataGridViewCellStyle14.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle14.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle14.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle14.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewHomework.RowHeadersDefaultCellStyle = dataGridViewCellStyle14;
            this.dataGridViewHomework.RowHeadersVisible = false;
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle15.BackColor = System.Drawing.Color.Transparent;
            dataGridViewCellStyle15.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle15.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle15.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle15.SelectionForeColor = System.Drawing.Color.Black;
            this.dataGridViewHomework.RowsDefaultCellStyle = dataGridViewCellStyle15;
            this.dataGridViewHomework.RowTemplate.DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridViewHomework.RowTemplate.DefaultCellStyle.BackColor = System.Drawing.Color.White;
            this.dataGridViewHomework.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataGridViewHomework.RowTemplate.DefaultCellStyle.ForeColor = System.Drawing.Color.Black;
            this.dataGridViewHomework.RowTemplate.DefaultCellStyle.SelectionBackColor = System.Drawing.Color.Black;
            this.dataGridViewHomework.RowTemplate.DefaultCellStyle.SelectionForeColor = System.Drawing.Color.White;
            this.dataGridViewHomework.RowTemplate.Height = 24;
            this.dataGridViewHomework.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewHomework.Size = new System.Drawing.Size(545, 343);
            this.dataGridViewHomework.StandardTab = true;
            this.dataGridViewHomework.TabIndex = 31;
            // 
            // labelHomeworkDetails
            // 
            this.labelHomeworkDetails.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelHomeworkDetails.AutoSize = true;
            this.labelHomeworkDetails.Font = new System.Drawing.Font("Century Gothic", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelHomeworkDetails.ForeColor = System.Drawing.Color.Black;
            this.labelHomeworkDetails.Location = new System.Drawing.Point(11, 309);
            this.labelHomeworkDetails.Name = "labelHomeworkDetails";
            this.labelHomeworkDetails.Size = new System.Drawing.Size(176, 23);
            this.labelHomeworkDetails.TabIndex = 30;
            this.labelHomeworkDetails.Text = "Homework Details";
            // 
            // dateTimePickerAssignedHomework
            // 
            this.dateTimePickerAssignedHomework.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dateTimePickerAssignedHomework.Enabled = false;
            this.dateTimePickerAssignedHomework.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.dateTimePickerAssignedHomework.Location = new System.Drawing.Point(10, 245);
            this.dateTimePickerAssignedHomework.Name = "dateTimePickerAssignedHomework";
            this.dateTimePickerAssignedHomework.Size = new System.Drawing.Size(506, 32);
            this.dateTimePickerAssignedHomework.TabIndex = 28;
            // 
            // labelAssignedDate
            // 
            this.labelAssignedDate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelAssignedDate.AutoSize = true;
            this.labelAssignedDate.Font = new System.Drawing.Font("Century Gothic", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelAssignedDate.ForeColor = System.Drawing.Color.Black;
            this.labelAssignedDate.Location = new System.Drawing.Point(11, 204);
            this.labelAssignedDate.Name = "labelAssignedDate";
            this.labelAssignedDate.Size = new System.Drawing.Size(143, 23);
            this.labelAssignedDate.TabIndex = 27;
            this.labelAssignedDate.Text = "Assigned Date";
            // 
            // textboxHomeworkTitle
            // 
            this.textboxHomeworkTitle.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textboxHomeworkTitle.BackColor = System.Drawing.SystemColors.Control;
            this.textboxHomeworkTitle.Cursor = System.Windows.Forms.Cursors.Default;
            this.textboxHomeworkTitle.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textboxHomeworkTitle.ForeColor = System.Drawing.Color.Black;
            this.textboxHomeworkTitle.Location = new System.Drawing.Point(10, 158);
            this.textboxHomeworkTitle.MaxLength = 40;
            this.textboxHomeworkTitle.Name = "textboxHomeworkTitle";
            this.textboxHomeworkTitle.ReadOnly = true;
            this.textboxHomeworkTitle.Size = new System.Drawing.Size(506, 32);
            this.textboxHomeworkTitle.TabIndex = 20;
            // 
            // labelHomeworkTitle
            // 
            this.labelHomeworkTitle.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelHomeworkTitle.AutoSize = true;
            this.labelHomeworkTitle.Font = new System.Drawing.Font("Century Gothic", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelHomeworkTitle.ForeColor = System.Drawing.Color.Black;
            this.labelHomeworkTitle.Location = new System.Drawing.Point(11, 116);
            this.labelHomeworkTitle.Name = "labelHomeworkTitle";
            this.labelHomeworkTitle.Size = new System.Drawing.Size(148, 23);
            this.labelHomeworkTitle.TabIndex = 19;
            this.labelHomeworkTitle.Text = "Homework title";
            // 
            // labelSubjectName
            // 
            this.labelSubjectName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelSubjectName.AutoSize = true;
            this.labelSubjectName.Font = new System.Drawing.Font("Century Gothic", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSubjectName.ForeColor = System.Drawing.Color.Black;
            this.labelSubjectName.Location = new System.Drawing.Point(13, 18);
            this.labelSubjectName.Name = "labelSubjectName";
            this.labelSubjectName.Size = new System.Drawing.Size(141, 23);
            this.labelSubjectName.TabIndex = 17;
            this.labelSubjectName.Text = "Subject Name";
            // 
            // groupBoxAssignment
            // 
            this.groupBoxAssignment.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxAssignment.Controls.Add(this.textBoxAssignmentSubjectName);
            this.groupBoxAssignment.Controls.Add(this.dataGridViewAssignments);
            this.groupBoxAssignment.Controls.Add(this.labelAssignmentDetails);
            this.groupBoxAssignment.Controls.Add(this.dateTimePickerAssignedAssignment);
            this.groupBoxAssignment.Controls.Add(this.label3);
            this.groupBoxAssignment.Controls.Add(this.textBoxAssignmentTitle);
            this.groupBoxAssignment.Controls.Add(this.label2);
            this.groupBoxAssignment.Controls.Add(this.label1);
            this.groupBoxAssignment.Location = new System.Drawing.Point(557, 47);
            this.groupBoxAssignment.Name = "groupBoxAssignment";
            this.groupBoxAssignment.Size = new System.Drawing.Size(547, 692);
            this.groupBoxAssignment.TabIndex = 1;
            this.groupBoxAssignment.TabStop = false;
            // 
            // textBoxAssignmentSubjectName
            // 
            this.textBoxAssignmentSubjectName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxAssignmentSubjectName.BackColor = System.Drawing.SystemColors.Control;
            this.textBoxAssignmentSubjectName.Cursor = System.Windows.Forms.Cursors.Default;
            this.textBoxAssignmentSubjectName.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxAssignmentSubjectName.ForeColor = System.Drawing.Color.Black;
            this.textBoxAssignmentSubjectName.Location = new System.Drawing.Point(16, 60);
            this.textBoxAssignmentSubjectName.MaxLength = 40;
            this.textBoxAssignmentSubjectName.Name = "textBoxAssignmentSubjectName";
            this.textBoxAssignmentSubjectName.Size = new System.Drawing.Size(506, 32);
            this.textBoxAssignmentSubjectName.TabIndex = 33;
            // 
            // dataGridViewAssignments
            // 
            this.dataGridViewAssignments.AllowDrop = true;
            this.dataGridViewAssignments.AllowUserToAddRows = false;
            this.dataGridViewAssignments.AllowUserToDeleteRows = false;
            this.dataGridViewAssignments.AllowUserToOrderColumns = true;
            this.dataGridViewAssignments.AllowUserToResizeColumns = false;
            this.dataGridViewAssignments.AllowUserToResizeRows = false;
            dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle16.BackColor = System.Drawing.Color.Transparent;
            dataGridViewCellStyle16.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle16.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle16.SelectionBackColor = System.Drawing.SystemColors.GradientActiveCaption;
            dataGridViewCellStyle16.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle16.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewAssignments.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle16;
            this.dataGridViewAssignments.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewAssignments.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridViewAssignments.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridViewAssignments.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridViewAssignments.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.RaisedHorizontal;
            dataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle17.BackColor = System.Drawing.Color.Transparent;
            dataGridViewCellStyle17.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle17.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle17.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle17.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle17.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewAssignments.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle17;
            this.dataGridViewAssignments.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dataGridViewAssignments.ColumnHeadersVisible = false;
            dataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle18.BackColor = System.Drawing.Color.Transparent;
            dataGridViewCellStyle18.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle18.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle18.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle18.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle18.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewAssignments.DefaultCellStyle = dataGridViewCellStyle18;
            this.dataGridViewAssignments.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.dataGridViewAssignments.Location = new System.Drawing.Point(3, 349);
            this.dataGridViewAssignments.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dataGridViewAssignments.MultiSelect = false;
            this.dataGridViewAssignments.Name = "dataGridViewAssignments";
            this.dataGridViewAssignments.ReadOnly = true;
            dataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle19.BackColor = System.Drawing.Color.Transparent;
            dataGridViewCellStyle19.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle19.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle19.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle19.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle19.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewAssignments.RowHeadersDefaultCellStyle = dataGridViewCellStyle19;
            this.dataGridViewAssignments.RowHeadersVisible = false;
            dataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle20.BackColor = System.Drawing.Color.Transparent;
            dataGridViewCellStyle20.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle20.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle20.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle20.SelectionForeColor = System.Drawing.Color.Black;
            this.dataGridViewAssignments.RowsDefaultCellStyle = dataGridViewCellStyle20;
            this.dataGridViewAssignments.RowTemplate.DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridViewAssignments.RowTemplate.DefaultCellStyle.BackColor = System.Drawing.Color.White;
            this.dataGridViewAssignments.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataGridViewAssignments.RowTemplate.DefaultCellStyle.ForeColor = System.Drawing.Color.Black;
            this.dataGridViewAssignments.RowTemplate.DefaultCellStyle.SelectionBackColor = System.Drawing.Color.Black;
            this.dataGridViewAssignments.RowTemplate.DefaultCellStyle.SelectionForeColor = System.Drawing.Color.White;
            this.dataGridViewAssignments.RowTemplate.Height = 24;
            this.dataGridViewAssignments.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewAssignments.Size = new System.Drawing.Size(541, 340);
            this.dataGridViewAssignments.StandardTab = true;
            this.dataGridViewAssignments.TabIndex = 50;
            // 
            // labelAssignmentDetails
            // 
            this.labelAssignmentDetails.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelAssignmentDetails.AutoSize = true;
            this.labelAssignmentDetails.Font = new System.Drawing.Font("Century Gothic", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelAssignmentDetails.ForeColor = System.Drawing.Color.Black;
            this.labelAssignmentDetails.Location = new System.Drawing.Point(16, 309);
            this.labelAssignmentDetails.Name = "labelAssignmentDetails";
            this.labelAssignmentDetails.Size = new System.Drawing.Size(183, 23);
            this.labelAssignmentDetails.TabIndex = 49;
            this.labelAssignmentDetails.Text = "Assignment Details";
            // 
            // dateTimePickerAssignedAssignment
            // 
            this.dateTimePickerAssignedAssignment.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dateTimePickerAssignedAssignment.Enabled = false;
            this.dateTimePickerAssignedAssignment.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.dateTimePickerAssignedAssignment.Location = new System.Drawing.Point(16, 243);
            this.dateTimePickerAssignedAssignment.Name = "dateTimePickerAssignedAssignment";
            this.dateTimePickerAssignedAssignment.Size = new System.Drawing.Size(502, 32);
            this.dateTimePickerAssignedAssignment.TabIndex = 32;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(17, 202);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(143, 23);
            this.label3.TabIndex = 31;
            this.label3.Text = "Assigned Date";
            // 
            // textBoxAssignmentTitle
            // 
            this.textBoxAssignmentTitle.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxAssignmentTitle.BackColor = System.Drawing.SystemColors.Control;
            this.textBoxAssignmentTitle.Cursor = System.Windows.Forms.Cursors.Default;
            this.textBoxAssignmentTitle.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxAssignmentTitle.ForeColor = System.Drawing.Color.Black;
            this.textBoxAssignmentTitle.Location = new System.Drawing.Point(16, 158);
            this.textBoxAssignmentTitle.MaxLength = 40;
            this.textBoxAssignmentTitle.Name = "textBoxAssignmentTitle";
            this.textBoxAssignmentTitle.Size = new System.Drawing.Size(506, 32);
            this.textBoxAssignmentTitle.TabIndex = 32;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(16, 116);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(155, 23);
            this.label2.TabIndex = 31;
            this.label2.Text = "Assignment title";
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(16, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(141, 23);
            this.label1.TabIndex = 19;
            this.label1.Text = "Subject Name";
            // 
            // checkBoxIsHomework
            // 
            this.checkBoxIsHomework.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBoxIsHomework.AutoSize = true;
            this.checkBoxIsHomework.BackColor = System.Drawing.Color.Transparent;
            this.checkBoxIsHomework.FlatAppearance.BorderSize = 0;
            this.checkBoxIsHomework.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.checkBoxIsHomework.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxIsHomework.ForeColor = System.Drawing.Color.Black;
            this.checkBoxIsHomework.Location = new System.Drawing.Point(20, 3);
            this.checkBoxIsHomework.Name = "checkBoxIsHomework";
            this.checkBoxIsHomework.Size = new System.Drawing.Size(123, 29);
            this.checkBoxIsHomework.TabIndex = 45;
            this.checkBoxIsHomework.Text = "Homework";
            this.checkBoxIsHomework.UseVisualStyleBackColor = false;
            this.checkBoxIsHomework.CheckedChanged += new System.EventHandler(this.CheckBoxIsHomework_CheckedChanged);
            // 
            // checkBoxIsAssignment
            // 
            this.checkBoxIsAssignment.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBoxIsAssignment.AutoSize = true;
            this.checkBoxIsAssignment.BackColor = System.Drawing.Color.Transparent;
            this.checkBoxIsAssignment.FlatAppearance.BorderSize = 0;
            this.checkBoxIsAssignment.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.checkBoxIsAssignment.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxIsAssignment.ForeColor = System.Drawing.Color.Black;
            this.checkBoxIsAssignment.Location = new System.Drawing.Point(968, 3);
            this.checkBoxIsAssignment.Name = "checkBoxIsAssignment";
            this.checkBoxIsAssignment.Size = new System.Drawing.Size(133, 29);
            this.checkBoxIsAssignment.TabIndex = 46;
            this.checkBoxIsAssignment.Text = "Assignment";
            this.checkBoxIsAssignment.UseVisualStyleBackColor = false;
            this.checkBoxIsAssignment.CheckedChanged += new System.EventHandler(this.CheckBoxIsAssignment_CheckedChanged);
            // 
            // labelid
            // 
            this.labelid.AutoSize = true;
            this.labelid.Location = new System.Drawing.Point(544, 4);
            this.labelid.Name = "labelid";
            this.labelid.Size = new System.Drawing.Size(49, 17);
            this.labelid.TabIndex = 47;
            this.labelid.Text = "labelid";
            this.labelid.Visible = false;
            // 
            // CurrentlyStudying
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.labelid);
            this.Controls.Add(this.checkBoxIsAssignment);
            this.Controls.Add(this.checkBoxIsHomework);
            this.Controls.Add(this.groupBoxAssignment);
            this.Controls.Add(this.groupBoxHomework);
            this.DoubleBuffered = true;
            this.Name = "CurrentlyStudying";
            this.Size = new System.Drawing.Size(1104, 745);
            this.groupBoxHomework.ResumeLayout(false);
            this.groupBoxHomework.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewHomework)).EndInit();
            this.groupBoxAssignment.ResumeLayout(false);
            this.groupBoxAssignment.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewAssignments)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBoxHomework;
        private System.Windows.Forms.GroupBox groupBoxAssignment;
        public System.Windows.Forms.CheckBox checkBoxIsHomework;
        public System.Windows.Forms.CheckBox checkBoxIsAssignment;
        private System.Windows.Forms.Label labelSubjectName;
        public System.Windows.Forms.TextBox textboxHomeworkTitle;
        private System.Windows.Forms.Label labelHomeworkTitle;
        private System.Windows.Forms.Label labelAssignedDate;
        public System.Windows.Forms.TextBox textBoxAssignmentTitle;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        public System.Windows.Forms.DataGridView dataGridViewAssignments;
        private System.Windows.Forms.Label labelAssignmentDetails;
        public System.Windows.Forms.DataGridView dataGridViewHomework;
        private System.Windows.Forms.Label labelHomeworkDetails;
        public System.Windows.Forms.Label labelid;
        public System.Windows.Forms.TextBox textBoxHomeworkSubjectName;
        public System.Windows.Forms.TextBox textBoxAssignmentSubjectName;
        public System.Windows.Forms.DateTimePicker dateTimePickerAssignedHomework;
        public System.Windows.Forms.DateTimePicker dateTimePickerAssignedAssignment;
    }
}
