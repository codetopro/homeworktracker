﻿namespace Homework
{
    partial class Subject
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelSemesterName = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.comboBoxSemesterName = new System.Windows.Forms.ComboBox();
            this.textboxSubjectName = new System.Windows.Forms.TextBox();
            this.textBoxTeacherName = new System.Windows.Forms.TextBox();
            this.buttonUpdateSubject = new System.Windows.Forms.Button();
            this.buttonAddSubject = new System.Windows.Forms.Button();
            this.labelid = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // labelSemesterName
            // 
            this.labelSemesterName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelSemesterName.AutoSize = true;
            this.labelSemesterName.Font = new System.Drawing.Font("Century Gothic", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSemesterName.ForeColor = System.Drawing.Color.Black;
            this.labelSemesterName.Location = new System.Drawing.Point(82, 114);
            this.labelSemesterName.Name = "labelSemesterName";
            this.labelSemesterName.Size = new System.Drawing.Size(141, 23);
            this.labelSemesterName.TabIndex = 9;
            this.labelSemesterName.Text = "Subject Name";
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(82, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(156, 23);
            this.label1.TabIndex = 10;
            this.label1.Text = "Semester Name";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(82, 215);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(158, 23);
            this.label2.TabIndex = 11;
            this.label2.Text = "Teacher\'s Name";
            // 
            // comboBoxSemesterName
            // 
            this.comboBoxSemesterName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.comboBoxSemesterName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxSemesterName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxSemesterName.FormattingEnabled = true;
            this.comboBoxSemesterName.Items.AddRange(new object[] {
            "1st Semester",
            "2nd Semester",
            "3rd Semester",
            "4th Semester",
            "5th Semester",
            "6th semester"});
            this.comboBoxSemesterName.Location = new System.Drawing.Point(86, 60);
            this.comboBoxSemesterName.Name = "comboBoxSemesterName";
            this.comboBoxSemesterName.Size = new System.Drawing.Size(394, 33);
            this.comboBoxSemesterName.Sorted = true;
            this.comboBoxSemesterName.TabIndex = 13;
            // 
            // textboxSubjectName
            // 
            this.textboxSubjectName.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textboxSubjectName.Location = new System.Drawing.Point(86, 154);
            this.textboxSubjectName.MaxLength = 40;
            this.textboxSubjectName.Name = "textboxSubjectName";
            this.textboxSubjectName.Size = new System.Drawing.Size(394, 37);
            this.textboxSubjectName.TabIndex = 14;
            // 
            // textBoxTeacherName
            // 
            this.textBoxTeacherName.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxTeacherName.Location = new System.Drawing.Point(86, 273);
            this.textBoxTeacherName.MaxLength = 40;
            this.textBoxTeacherName.Name = "textBoxTeacherName";
            this.textBoxTeacherName.Size = new System.Drawing.Size(394, 37);
            this.textBoxTeacherName.TabIndex = 15;
            // 
            // buttonUpdateSubject
            // 
            this.buttonUpdateSubject.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonUpdateSubject.BackColor = System.Drawing.SystemColors.Control;
            this.buttonUpdateSubject.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonUpdateSubject.Font = new System.Drawing.Font("Century Gothic", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonUpdateSubject.Location = new System.Drawing.Point(365, 362);
            this.buttonUpdateSubject.Name = "buttonUpdateSubject";
            this.buttonUpdateSubject.Size = new System.Drawing.Size(115, 37);
            this.buttonUpdateSubject.TabIndex = 17;
            this.buttonUpdateSubject.Text = "Update";
            this.buttonUpdateSubject.UseVisualStyleBackColor = false;
            this.buttonUpdateSubject.Click += new System.EventHandler(this.ButtonUpdateSubject_Click);
            // 
            // buttonAddSubject
            // 
            this.buttonAddSubject.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonAddSubject.BackColor = System.Drawing.SystemColors.Control;
            this.buttonAddSubject.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonAddSubject.Font = new System.Drawing.Font("Century Gothic", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonAddSubject.Location = new System.Drawing.Point(86, 362);
            this.buttonAddSubject.Name = "buttonAddSubject";
            this.buttonAddSubject.Size = new System.Drawing.Size(115, 37);
            this.buttonAddSubject.TabIndex = 16;
            this.buttonAddSubject.Text = "Add";
            this.buttonAddSubject.UseVisualStyleBackColor = false;
            this.buttonAddSubject.Click += new System.EventHandler(this.ButtonAddSubject_Click);
            // 
            // labelid
            // 
            this.labelid.AutoSize = true;
            this.labelid.Location = new System.Drawing.Point(464, 4);
            this.labelid.Name = "labelid";
            this.labelid.Size = new System.Drawing.Size(19, 17);
            this.labelid.TabIndex = 18;
            this.labelid.Text = "id";
            this.labelid.Visible = false;
            // 
            // Subject
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.labelid);
            this.Controls.Add(this.buttonUpdateSubject);
            this.Controls.Add(this.buttonAddSubject);
            this.Controls.Add(this.textBoxTeacherName);
            this.Controls.Add(this.textboxSubjectName);
            this.Controls.Add(this.comboBoxSemesterName);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.labelSemesterName);
            this.DoubleBuffered = true;
            this.Name = "Subject";
            this.Size = new System.Drawing.Size(521, 515);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelSemesterName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        public System.Windows.Forms.ComboBox comboBoxSemesterName;
        private System.Windows.Forms.Button buttonUpdateSubject;
        private System.Windows.Forms.Button buttonAddSubject;
        public System.Windows.Forms.Label labelid;
        public System.Windows.Forms.TextBox textboxSubjectName;
        public System.Windows.Forms.TextBox textBoxTeacherName;
    }
}
