﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using LogicLayer;

namespace Homework
{
    public partial class Settings : UserControl
    {
        SqlConnection con = new SqlConnection (" Data Source =.; Initial Catalog = Homework; Integrated Security = True");
        public Settings()
        {
            InitializeComponent();
        }

        private void ButtonSelectFolder_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialog.ShowDialog() == DialogResult.OK)
            {
                string location = folderBrowserDialog.SelectedPath;
                textBoxBackupLocation.Text = location;
            }
        }

        private void ButtonBackup_Click(object sender, EventArgs e)
        {

            if (!string.IsNullOrEmpty(textBoxBackupLocation.Text))
            {
                try
                {
                    DateTime date = DateTime.Now;
                    string dd = date.Month + "-" + date.Day + "-" + date.Year;
                    string servname = ".";       //SERVER NAME
                    string dbname = "Homework";            // DATABASE NAME
                    //@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\Database\KandKMart.mdf;Integrated Security=True"
                    string source = @"Data Source=" + servname + ";Integrated Security=True;Initial Catalog=" + dbname + "";
                    SqlConnection con = new SqlConnection(source);
                    con.Open();
                    string str = "USE " + dbname + ";";
                    string str1 = "BACKUP DATABASE " + dbname +
                    " TO DISK = '" + textBoxBackupLocation.Text + "\\" + dbname + "_" + dd +
                    ".Bak' WITH FORMAT,MEDIANAME = 'Z_SQLServerBackups',NAME = 'Full Backup of " + dbname + "';";
                    SqlCommand cmd1 = new SqlCommand(str, con);
                    SqlCommand cmd2 = new SqlCommand(str1, con);
                    cmd1.ExecuteNonQuery();
                    cmd2.ExecuteNonQuery();
                    MessageBox.Show("Backup Successfully Completed.", "Backup Successfull", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                    con.Close();
                    textBoxBackupLocation.Clear();
                }
                catch (Exception)
                {
                }
            }
            else
            {
                MessageBox.Show("Select The Location", "Backup Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void ButtonSelectFolderRestore_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "SQL BackUp File |*.bak";
            dialog.Title = "Select Database back up file";
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                textBoxRestoreLocation.Text = dialog.FileName;
            }
        }

        private void ButtonRestore_Click(object sender, EventArgs e)
        {
            DialogResult dialog= MessageBox.Show("Your application would exit do you want to restore database?","Warning",MessageBoxButtons.YesNo);
            if (dialog== DialogResult.Yes)
            {
                if (con.State != ConnectionState.Open)
                {
                    con.Open();
                }
                string database = con.Database.ToString();
                try
                {
                    string String1 = string.Format("ALTER DATABASE [" + database + "] SET MULTI_USER WITH ROLLBACK IMMEDIATE");
                    SqlCommand sql = new SqlCommand(String1, con);
                    sql.ExecuteNonQuery();

                    string String2 = " USE MASTER RESTORE DATABASE [" + database + "] FROM DISK'" + textBoxRestoreLocation.Text + "' WITH REPLACE;";
                    SqlCommand sql1 = new SqlCommand(String2, con);
                    sql.ExecuteNonQuery();

                    string String3 = string.Format("ALTER DATABASE [" + database + "] SET MULTI_USER ");
                    SqlCommand sql2 = new SqlCommand(String3, con);
                    sql.ExecuteNonQuery();

                    MessageBox.Show("Database Restored Successfully");
                    con.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error");
                }
                textBoxRestoreLocation.Clear();
                Application.ExitThread();
                Application.Exit();
            }
            else
            {

            }
            
        }

        private void ButtonExecuteSqlQuery_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(textBoxQuery.Text))
            {
                string query = textBoxQuery.Text;
                
                try
                {
                   dataGridView.DataSource= Logic.GetTableByQuery(query);
                }
                catch (Exception EX)
                {
                    dataGridView.DataSource = EX;
                }
            }
        }

        private void ButtonClear_Click(object sender, EventArgs e)
        {
            dataGridView.Controls.Clear();
            textBoxQuery.Clear();
        }

    }
}
