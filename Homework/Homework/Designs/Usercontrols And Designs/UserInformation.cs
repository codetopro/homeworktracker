﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using Homework.Properties;
using LogicLayer;

namespace Homework
{
    public partial class UserInformation : UserControl
    {
        public string SQLCON = System.Configuration.ConfigurationManager.ConnectionStrings["Connection"].ConnectionString.ToString();
        private int id;
        Logic logic = new Logic();
        public UserInformation(int userId)
        {
            InitializeComponent();
            id = userId;
            labelid.Text = id.ToString();
            GetUserInformations(id);
            DisplayDetails();
        }

        private void DisplayDetails()
        {
            dataGridView.DataSource = Logic.DisplayUserDetails();
            dataGridView.Columns[0].Visible = false;
        }

        private void TextboxFirstName_TextChanged(object sender, EventArgs e)
        {
            textBoxUserNmae.Text = textboxFirstName.Text;
        }

        private void GetUserInformations(int userId)
        {
            using (SqlConnection con = new SqlConnection(SQLCON))
            using (SqlCommand command = new SqlCommand("GetUserInfo", con))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@FirstName", SqlDbType.NVarChar, 70);
                command.Parameters["@FirstName"].Direction = ParameterDirection.Output;
                command.Parameters.Add("@MiddelName", SqlDbType.NVarChar, 70);
                command.Parameters["@MiddelName"].Direction = ParameterDirection.Output;
                command.Parameters.Add("@SurName", SqlDbType.NVarChar, 70);
                command.Parameters["@SurName"].Direction = ParameterDirection.Output;
                command.Parameters.Add("@Gmail", SqlDbType.NVarChar, 90);
                command.Parameters["@Gmail"].Direction = ParameterDirection.Output;
                command.Parameters.Add("@Contact", SqlDbType.NVarChar, 40);
                command.Parameters["@Contact"].Direction = ParameterDirection.Output;
                command.Parameters.Add("@Username", SqlDbType.NVarChar, 40);
                command.Parameters["@Username"].Direction = ParameterDirection.Output;
                command.Parameters.Add("@UserId", SqlDbType.Int).Value = userId;
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                    command.ExecuteNonQuery();
                    textboxFirstName.Text = command.Parameters["@FirstName"].Value.ToString().Trim();
                    textBoxMiddelName.Text = command.Parameters["@MiddelName"].Value.ToString().Trim();
                    textBoxSurname.Text = command.Parameters["@SurName"].Value.ToString().Trim();
                    textBoxGmail.Text = command.Parameters["@Gmail"].Value.ToString().Trim();
                    textBoxContact.Text = command.Parameters["@Contact"].Value.ToString().Trim();
                    textBoxUserNmae.Text = command.Parameters["@Username"].Value.ToString().Trim();
                    con.Close();
                }
            }
        }

        private void CheckboxShowHidePassword_CheckedChanged(object sender, EventArgs e)
        {
            if (CheckboxShowHidePassword.Checked)
            {
                textBoxPassword.UseSystemPasswordChar = false;
                CheckboxShowHidePassword.Image = Resources.Hide;
            }
            else
            {
                textBoxPassword.UseSystemPasswordChar = true;
                CheckboxShowHidePassword.Image = Resources.Show;
            }
        }

        private void ButtonUpdate_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(textboxFirstName.Text) && !string.IsNullOrEmpty(textBoxSurname.Text) && !string.IsNullOrEmpty(textBoxGmail.Text) && !string.IsNullOrEmpty(textBoxContact.Text) && !string.IsNullOrEmpty(textBoxPassword.Text) && !string.IsNullOrEmpty(textBoxRetypePassword.Text))
            {
                if (!string.IsNullOrWhiteSpace(textboxFirstName.Text) && !string.IsNullOrWhiteSpace(textBoxSurname.Text) && !string.IsNullOrWhiteSpace(textBoxGmail.Text) && !string.IsNullOrWhiteSpace(textBoxContact.Text) && !string.IsNullOrWhiteSpace(textBoxPassword.Text) && !string.IsNullOrWhiteSpace(textBoxRetypePassword.Text))
                {
                    if (textBoxPassword.Text == textBoxRetypePassword.Text)
                    {
                        bool statement = logic.UpdateAccount(textboxFirstName.Text, textBoxMiddelName.Text, textBoxSurname.Text, textBoxContact.Text, textBoxGmail.Text, textBoxUserNmae.Text, textBoxRetypePassword.Text,id);
                        if (statement)
                        {
                            MessageBox.Show("Account updated successfully.", "User account updated");
                        }
                        else
                        {
                            MessageBox.Show("Server Down.", "Warning");
                        }
                    }
                    else
                    {
                        MessageBox.Show("Password not matched, Retype password.", "ALERT");
                        textBoxPassword.Clear();
                        textBoxRetypePassword.Clear();
                        textBoxPassword.Focus();
                    }
                    }
                }
                else
                {
                    MessageBox.Show("Please fill all the details. ", "ALERT");
                }
            DisplayDetails();
        }
    }
}
