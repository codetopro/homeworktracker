﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using LogicLayer;
namespace Homework
{
    public partial class Subject : UserControl
    {
        Logic logic = new Logic();
        public Subject()
        {
            InitializeComponent();
            LoadSemesterCombobox();
            comboBoxSemesterName.SelectedIndex = -1;
        }
        private void LoadSemesterCombobox()
        {
            string SQL = "select  SemesterId,SemesterName from Semester";
            var data = Logic.GetTableByQuery(SQL);
            comboBoxSemesterName.DataSource = data;
            comboBoxSemesterName.DisplayMember = "SemesterName";
            comboBoxSemesterName.ValueMember = "SemesterId";
        }

        private void ButtonAddSubject_Click(object sender, EventArgs e)
        {
            if (comboBoxSemesterName.SelectedIndex != -1 && !string.IsNullOrEmpty(textboxSubjectName.Text) && !string.IsNullOrEmpty(textBoxTeacherName.Text))
            {
                bool ConditionCheck = logic.InsertSubject(textboxSubjectName.Text, textBoxTeacherName.Text, Convert.ToInt32(comboBoxSemesterName.SelectedValue));
                if (ConditionCheck)
                {
                    MessageBox.Show("Subject Added Successfully", "Subject Added");
                }
                else
                {
                    MessageBox.Show("Subject Exists.", "Warning");
                }
            }
            else
            {
                MessageBox.Show("Please select the details");
            }
        }

        private void ButtonUpdateSubject_Click(object sender, EventArgs e)
        {
            if (comboBoxSemesterName.SelectedIndex != -1 && !string.IsNullOrEmpty(textboxSubjectName.Text) && !string.IsNullOrEmpty(textBoxTeacherName.Text))
            {
                bool ConditionCheck = logic.UpdateSubject(textboxSubjectName.Text, textBoxTeacherName.Text, Convert.ToInt32(comboBoxSemesterName.SelectedValue),labelid.Text);
               if (ConditionCheck)
                {
                    MessageBox.Show("Subject Added Successfully", "Subject Added");
                }
                else
                {
                    MessageBox.Show("Updated Failure");
                }
            }
            else
            {
                MessageBox.Show("Please select the details");
            }
        }
    }
}
