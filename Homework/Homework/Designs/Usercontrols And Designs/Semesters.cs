﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using LogicLayer;

namespace Homework
{
    public partial class Semesters : UserControl
    {
        Logic logic = new Logic();
        int complete, current;
        public Semesters()
        {
            InitializeComponent();
        }

        private void checkCompleteAndCurrent()
        {
            if (checkBoxIsCompleted.Checked)
            {
                complete = 1;
            }
            else
            {
                complete = 0;
            }
            if (checkBoxIsCurrentSemester.Checked)
            {
                current = 1;
            }
            else
            {
                current = 0;
            }
        }
        private void reset()
        {
            checkBoxIsCompleted.Checked = false;
            checkBoxIsCurrentSemester.Checked = false;
            comboBoxSemesterName.SelectedIndex = -1;
            labelid.Text = "";
        }
        public void getSubjects(string id)
        {
            try
            {
                string SQL = "SELECT  SubjectId,SubjectName FROM Subjects WHERE SemesterId= " + id;
                var data = Logic.GetTableByQuery(SQL);
                dataGridView.DataSource = data;
                dataGridView.Columns[0].Visible = false;
            }
            catch (Exception)
            {
            }
           
        }

        private void ButtonUpdateSemester_Click(object sender, EventArgs e)
        {
            if (comboBoxSemesterName.SelectedIndex != -1)
            {
                checkCompleteAndCurrent();
                bool ConditionCheck = logic.UpdateSemester(comboBoxSemesterName.Text, current, complete,labelid.Text);
                if (ConditionCheck)
                {
                    MessageBox.Show("Semester Updated Successfully", "Semester Added");
                }
                else
                {
                    MessageBox.Show("Updated Failure");
                }
            }
            else
            {
                MessageBox.Show("Please select the details");
            }
            getSubjects(labelid.Text);
            reset();
        }
    

        private void ButtonAddSemester_Click(object sender, EventArgs e)
        {
            if (comboBoxSemesterName.SelectedIndex!=-1)
            {
                checkCompleteAndCurrent();
                bool ConditionCheck=logic.InsertSemester(comboBoxSemesterName.Text,current,complete);
                if (ConditionCheck)
                {
                    MessageBox.Show("Semester Added Successfully","Semester Added");
                }
                else
                {
                    MessageBox.Show("Semester Exists.", "Warning");
                   
                }
            }
            else
            {
                MessageBox.Show("Please select the details");
            }
            getSubjects(labelid.Text);
            reset();
        }
    }
}
