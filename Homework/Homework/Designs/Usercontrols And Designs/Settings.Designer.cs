﻿namespace Homework
{
    partial class Settings
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView = new System.Windows.Forms.DataGridView();
            this.panelSqlQueryExecuator = new System.Windows.Forms.Panel();
            this.buttonExecuteSqlQuery = new System.Windows.Forms.Button();
            this.textBoxQuery = new System.Windows.Forms.TextBox();
            this.labelbackup = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.buttonSelectFolder = new System.Windows.Forms.Button();
            this.labelLocation = new System.Windows.Forms.Label();
            this.textBoxBackupLocation = new System.Windows.Forms.TextBox();
            this.buttonBackup = new System.Windows.Forms.Button();
            this.labelRestore = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.buttonSelectFolderRestore = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxRestoreLocation = new System.Windows.Forms.TextBox();
            this.buttonRestore = new System.Windows.Forms.Button();
            this.labelExecuteSqlQueries = new System.Windows.Forms.Label();
            this.folderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.buttonClear = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).BeginInit();
            this.panelSqlQueryExecuator.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridView
            // 
            this.dataGridView.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.dataGridView.Location = new System.Drawing.Point(0, 75);
            this.dataGridView.Name = "dataGridView";
            this.dataGridView.RowTemplate.Height = 24;
            this.dataGridView.Size = new System.Drawing.Size(1244, 240);
            this.dataGridView.TabIndex = 0;
            // 
            // panelSqlQueryExecuator
            // 
            this.panelSqlQueryExecuator.Controls.Add(this.buttonExecuteSqlQuery);
            this.panelSqlQueryExecuator.Controls.Add(this.textBoxQuery);
            this.panelSqlQueryExecuator.Controls.Add(this.dataGridView);
            this.panelSqlQueryExecuator.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelSqlQueryExecuator.Location = new System.Drawing.Point(0, 272);
            this.panelSqlQueryExecuator.Name = "panelSqlQueryExecuator";
            this.panelSqlQueryExecuator.Size = new System.Drawing.Size(1244, 315);
            this.panelSqlQueryExecuator.TabIndex = 1;
            // 
            // buttonExecuteSqlQuery
            // 
            this.buttonExecuteSqlQuery.BackColor = System.Drawing.Color.Transparent;
            this.buttonExecuteSqlQuery.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonExecuteSqlQuery.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonExecuteSqlQuery.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonExecuteSqlQuery.Font = new System.Drawing.Font("Century Gothic", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonExecuteSqlQuery.Location = new System.Drawing.Point(0, 0);
            this.buttonExecuteSqlQuery.Name = "buttonExecuteSqlQuery";
            this.buttonExecuteSqlQuery.Size = new System.Drawing.Size(238, 75);
            this.buttonExecuteSqlQuery.TabIndex = 11;
            this.buttonExecuteSqlQuery.TabStop = false;
            this.buttonExecuteSqlQuery.Text = "Execuate SQL Query";
            this.buttonExecuteSqlQuery.UseVisualStyleBackColor = false;
            this.buttonExecuteSqlQuery.Click += new System.EventHandler(this.ButtonExecuteSqlQuery_Click);
            // 
            // textBoxQuery
            // 
            this.textBoxQuery.BackColor = System.Drawing.SystemColors.Control;
            this.textBoxQuery.Dock = System.Windows.Forms.DockStyle.Right;
            this.textBoxQuery.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxQuery.Location = new System.Drawing.Point(237, 0);
            this.textBoxQuery.Multiline = true;
            this.textBoxQuery.Name = "textBoxQuery";
            this.textBoxQuery.Size = new System.Drawing.Size(1007, 75);
            this.textBoxQuery.TabIndex = 3;
            // 
            // labelbackup
            // 
            this.labelbackup.AutoSize = true;
            this.labelbackup.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelbackup.Location = new System.Drawing.Point(5, 12);
            this.labelbackup.Name = "labelbackup";
            this.labelbackup.Size = new System.Drawing.Size(195, 25);
            this.labelbackup.TabIndex = 5;
            this.labelbackup.Text = "Backup Database :";
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.buttonSelectFolder);
            this.groupBox1.Controls.Add(this.labelLocation);
            this.groupBox1.Controls.Add(this.textBoxBackupLocation);
            this.groupBox1.Controls.Add(this.buttonBackup);
            this.groupBox1.Controls.Add(this.labelbackup);
            this.groupBox1.Location = new System.Drawing.Point(6, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(600, 162);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            // 
            // buttonSelectFolder
            // 
            this.buttonSelectFolder.BackColor = System.Drawing.Color.Transparent;
            this.buttonSelectFolder.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonSelectFolder.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSelectFolder.Font = new System.Drawing.Font("Century Gothic", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonSelectFolder.Location = new System.Drawing.Point(540, 42);
            this.buttonSelectFolder.Name = "buttonSelectFolder";
            this.buttonSelectFolder.Size = new System.Drawing.Size(54, 37);
            this.buttonSelectFolder.TabIndex = 17;
            this.buttonSelectFolder.TabStop = false;
            this.buttonSelectFolder.Text = ". . .";
            this.buttonSelectFolder.UseVisualStyleBackColor = false;
            this.buttonSelectFolder.Click += new System.EventHandler(this.ButtonSelectFolder_Click);
            // 
            // labelLocation
            // 
            this.labelLocation.AutoSize = true;
            this.labelLocation.Font = new System.Drawing.Font("Century Gothic", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelLocation.Location = new System.Drawing.Point(6, 52);
            this.labelLocation.Name = "labelLocation";
            this.labelLocation.Size = new System.Drawing.Size(99, 23);
            this.labelLocation.TabIndex = 16;
            this.labelLocation.Text = "Location :";
            // 
            // textBoxBackupLocation
            // 
            this.textBoxBackupLocation.Cursor = System.Windows.Forms.Cursors.Default;
            this.textBoxBackupLocation.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxBackupLocation.Location = new System.Drawing.Point(123, 42);
            this.textBoxBackupLocation.Name = "textBoxBackupLocation";
            this.textBoxBackupLocation.ReadOnly = true;
            this.textBoxBackupLocation.Size = new System.Drawing.Size(414, 37);
            this.textBoxBackupLocation.TabIndex = 15;
            // 
            // buttonBackup
            // 
            this.buttonBackup.BackColor = System.Drawing.Color.Transparent;
            this.buttonBackup.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonBackup.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonBackup.Font = new System.Drawing.Font("Century Gothic", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonBackup.Location = new System.Drawing.Point(3, 107);
            this.buttonBackup.Name = "buttonBackup";
            this.buttonBackup.Size = new System.Drawing.Size(594, 55);
            this.buttonBackup.TabIndex = 12;
            this.buttonBackup.TabStop = false;
            this.buttonBackup.Text = "Backup Database";
            this.buttonBackup.UseVisualStyleBackColor = false;
            this.buttonBackup.Click += new System.EventHandler(this.ButtonBackup_Click);
            // 
            // labelRestore
            // 
            this.labelRestore.AutoSize = true;
            this.labelRestore.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelRestore.Location = new System.Drawing.Point(11, 12);
            this.labelRestore.Name = "labelRestore";
            this.labelRestore.Size = new System.Drawing.Size(197, 25);
            this.labelRestore.TabIndex = 7;
            this.labelRestore.Text = "Restore Database :";
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.Transparent;
            this.groupBox2.Controls.Add(this.buttonSelectFolderRestore);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.textBoxRestoreLocation);
            this.groupBox2.Controls.Add(this.labelRestore);
            this.groupBox2.Controls.Add(this.buttonRestore);
            this.groupBox2.Location = new System.Drawing.Point(612, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(609, 165);
            this.groupBox2.TabIndex = 6;
            this.groupBox2.TabStop = false;
            // 
            // buttonSelectFolderRestore
            // 
            this.buttonSelectFolderRestore.BackColor = System.Drawing.Color.Transparent;
            this.buttonSelectFolderRestore.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonSelectFolderRestore.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSelectFolderRestore.Font = new System.Drawing.Font("Century Gothic", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonSelectFolderRestore.Location = new System.Drawing.Point(549, 52);
            this.buttonSelectFolderRestore.Name = "buttonSelectFolderRestore";
            this.buttonSelectFolderRestore.Size = new System.Drawing.Size(54, 37);
            this.buttonSelectFolderRestore.TabIndex = 17;
            this.buttonSelectFolderRestore.TabStop = false;
            this.buttonSelectFolderRestore.Text = ". . .";
            this.buttonSelectFolderRestore.UseVisualStyleBackColor = false;
            this.buttonSelectFolderRestore.Click += new System.EventHandler(this.ButtonSelectFolderRestore_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 60);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(99, 23);
            this.label2.TabIndex = 16;
            this.label2.Text = "Location :";
            // 
            // textBoxRestoreLocation
            // 
            this.textBoxRestoreLocation.Cursor = System.Windows.Forms.Cursors.Default;
            this.textBoxRestoreLocation.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxRestoreLocation.Location = new System.Drawing.Point(129, 52);
            this.textBoxRestoreLocation.Name = "textBoxRestoreLocation";
            this.textBoxRestoreLocation.ReadOnly = true;
            this.textBoxRestoreLocation.Size = new System.Drawing.Size(414, 37);
            this.textBoxRestoreLocation.TabIndex = 15;
            // 
            // buttonRestore
            // 
            this.buttonRestore.BackColor = System.Drawing.Color.Transparent;
            this.buttonRestore.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonRestore.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.buttonRestore.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonRestore.Font = new System.Drawing.Font("Century Gothic", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonRestore.Location = new System.Drawing.Point(3, 107);
            this.buttonRestore.Name = "buttonRestore";
            this.buttonRestore.Size = new System.Drawing.Size(603, 55);
            this.buttonRestore.TabIndex = 12;
            this.buttonRestore.TabStop = false;
            this.buttonRestore.Text = "Restore Database";
            this.buttonRestore.UseVisualStyleBackColor = false;
            this.buttonRestore.Click += new System.EventHandler(this.ButtonRestore_Click);
            // 
            // labelExecuteSqlQueries
            // 
            this.labelExecuteSqlQueries.AutoSize = true;
            this.labelExecuteSqlQueries.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelExecuteSqlQueries.Location = new System.Drawing.Point(11, 210);
            this.labelExecuteSqlQueries.Name = "labelExecuteSqlQueries";
            this.labelExecuteSqlQueries.Size = new System.Drawing.Size(223, 25);
            this.labelExecuteSqlQueries.TabIndex = 10;
            this.labelExecuteSqlQueries.Text = "Execute Sql Queries :";
            // 
            // buttonClear
            // 
            this.buttonClear.BackColor = System.Drawing.Color.Transparent;
            this.buttonClear.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonClear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonClear.Font = new System.Drawing.Font("Century Gothic", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonClear.Location = new System.Drawing.Point(1071, 203);
            this.buttonClear.Name = "buttonClear";
            this.buttonClear.Size = new System.Drawing.Size(150, 41);
            this.buttonClear.TabIndex = 30;
            this.buttonClear.TabStop = false;
            this.buttonClear.Text = "Clear";
            this.buttonClear.UseVisualStyleBackColor = false;
            this.buttonClear.Click += new System.EventHandler(this.ButtonClear_Click);
            // 
            // Settings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.buttonClear);
            this.Controls.Add(this.labelExecuteSqlQueries);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.panelSqlQueryExecuator);
            this.Name = "Settings";
            this.Size = new System.Drawing.Size(1244, 587);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).EndInit();
            this.panelSqlQueryExecuator.ResumeLayout(false);
            this.panelSqlQueryExecuator.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView;
        private System.Windows.Forms.Panel panelSqlQueryExecuator;
        private System.Windows.Forms.TextBox textBoxQuery;
        private System.Windows.Forms.Button buttonExecuteSqlQuery;
        private System.Windows.Forms.Label labelbackup;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button buttonBackup;
        private System.Windows.Forms.Button buttonSelectFolder;
        private System.Windows.Forms.Label labelLocation;
        private System.Windows.Forms.TextBox textBoxBackupLocation;
        private System.Windows.Forms.Label labelRestore;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button buttonSelectFolderRestore;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxRestoreLocation;
        private System.Windows.Forms.Button buttonRestore;
        private System.Windows.Forms.Label labelExecuteSqlQueries;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog;
        private System.Windows.Forms.Button buttonClear;
    }
}
