﻿namespace Homework
{
    partial class Semesters
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            this.labelSemesterName = new System.Windows.Forms.Label();
            this.checkBoxIsCompleted = new System.Windows.Forms.CheckBox();
            this.checkBoxIsCurrentSemester = new System.Windows.Forms.CheckBox();
            this.comboBoxSemesterName = new System.Windows.Forms.ComboBox();
            this.buttonAddSemester = new System.Windows.Forms.Button();
            this.buttonUpdateSemester = new System.Windows.Forms.Button();
            this.labelid = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dataGridView = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // labelSemesterName
            // 
            this.labelSemesterName.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelSemesterName.AutoSize = true;
            this.labelSemesterName.BackColor = System.Drawing.Color.Transparent;
            this.labelSemesterName.Font = new System.Drawing.Font("Century Gothic", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSemesterName.ForeColor = System.Drawing.Color.Black;
            this.labelSemesterName.Location = new System.Drawing.Point(82, 20);
            this.labelSemesterName.Name = "labelSemesterName";
            this.labelSemesterName.Size = new System.Drawing.Size(156, 23);
            this.labelSemesterName.TabIndex = 8;
            this.labelSemesterName.Text = "Semester Name";
            // 
            // checkBoxIsCompleted
            // 
            this.checkBoxIsCompleted.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.checkBoxIsCompleted.AutoSize = true;
            this.checkBoxIsCompleted.BackColor = System.Drawing.Color.Transparent;
            this.checkBoxIsCompleted.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxIsCompleted.ForeColor = System.Drawing.Color.Black;
            this.checkBoxIsCompleted.Location = new System.Drawing.Point(354, 109);
            this.checkBoxIsCompleted.Name = "checkBoxIsCompleted";
            this.checkBoxIsCompleted.Size = new System.Drawing.Size(129, 29);
            this.checkBoxIsCompleted.TabIndex = 10;
            this.checkBoxIsCompleted.Text = "Completed";
            this.checkBoxIsCompleted.UseVisualStyleBackColor = false;
            // 
            // checkBoxIsCurrentSemester
            // 
            this.checkBoxIsCurrentSemester.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.checkBoxIsCurrentSemester.AutoSize = true;
            this.checkBoxIsCurrentSemester.BackColor = System.Drawing.Color.Transparent;
            this.checkBoxIsCurrentSemester.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxIsCurrentSemester.ForeColor = System.Drawing.Color.Black;
            this.checkBoxIsCurrentSemester.Location = new System.Drawing.Point(89, 109);
            this.checkBoxIsCurrentSemester.Name = "checkBoxIsCurrentSemester";
            this.checkBoxIsCurrentSemester.Size = new System.Drawing.Size(188, 29);
            this.checkBoxIsCurrentSemester.TabIndex = 11;
            this.checkBoxIsCurrentSemester.Text = "Current Semester";
            this.checkBoxIsCurrentSemester.UseVisualStyleBackColor = false;
            // 
            // comboBoxSemesterName
            // 
            this.comboBoxSemesterName.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.comboBoxSemesterName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxSemesterName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxSemesterName.FormattingEnabled = true;
            this.comboBoxSemesterName.Items.AddRange(new object[] {
            "1st Semester",
            "2nd Semester",
            "3rd Semester",
            "4th Semester",
            "5th Semester",
            "6th semester"});
            this.comboBoxSemesterName.Location = new System.Drawing.Point(89, 57);
            this.comboBoxSemesterName.Name = "comboBoxSemesterName";
            this.comboBoxSemesterName.Size = new System.Drawing.Size(394, 33);
            this.comboBoxSemesterName.Sorted = true;
            this.comboBoxSemesterName.TabIndex = 12;
            // 
            // buttonAddSemester
            // 
            this.buttonAddSemester.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.buttonAddSemester.BackColor = System.Drawing.SystemColors.Control;
            this.buttonAddSemester.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonAddSemester.Font = new System.Drawing.Font("Century Gothic", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonAddSemester.Location = new System.Drawing.Point(86, 159);
            this.buttonAddSemester.Name = "buttonAddSemester";
            this.buttonAddSemester.Size = new System.Drawing.Size(115, 37);
            this.buttonAddSemester.TabIndex = 13;
            this.buttonAddSemester.Text = "Add";
            this.buttonAddSemester.UseVisualStyleBackColor = false;
            this.buttonAddSemester.Click += new System.EventHandler(this.ButtonAddSemester_Click);
            // 
            // buttonUpdateSemester
            // 
            this.buttonUpdateSemester.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.buttonUpdateSemester.BackColor = System.Drawing.SystemColors.Control;
            this.buttonUpdateSemester.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonUpdateSemester.Font = new System.Drawing.Font("Century Gothic", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonUpdateSemester.Location = new System.Drawing.Point(365, 159);
            this.buttonUpdateSemester.Name = "buttonUpdateSemester";
            this.buttonUpdateSemester.Size = new System.Drawing.Size(115, 37);
            this.buttonUpdateSemester.TabIndex = 14;
            this.buttonUpdateSemester.Text = "Update";
            this.buttonUpdateSemester.UseVisualStyleBackColor = false;
            this.buttonUpdateSemester.Click += new System.EventHandler(this.ButtonUpdateSemester_Click);
            // 
            // labelid
            // 
            this.labelid.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelid.AutoSize = true;
            this.labelid.Location = new System.Drawing.Point(464, 4);
            this.labelid.Name = "labelid";
            this.labelid.Size = new System.Drawing.Size(19, 17);
            this.labelid.TabIndex = 15;
            this.labelid.Text = "id";
            this.labelid.Visible = false;
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(227, 233);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(87, 23);
            this.label1.TabIndex = 16;
            this.label1.Text = "Subjects";
            // 
            // dataGridView
            // 
            this.dataGridView.AllowDrop = true;
            this.dataGridView.AllowUserToAddRows = false;
            this.dataGridView.AllowUserToDeleteRows = false;
            this.dataGridView.AllowUserToOrderColumns = true;
            this.dataGridView.AllowUserToResizeColumns = false;
            this.dataGridView.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Transparent;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.GradientActiveCaption;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridView.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridView.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.RaisedHorizontal;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.Transparent;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dataGridView.ColumnHeadersVisible = false;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.Transparent;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView.DefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridView.Location = new System.Drawing.Point(86, 283);
            this.dataGridView.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dataGridView.MultiSelect = false;
            this.dataGridView.Name = "dataGridView";
            this.dataGridView.ReadOnly = true;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.Transparent;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dataGridView.RowHeadersVisible = false;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.Transparent;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.Black;
            this.dataGridView.RowsDefaultCellStyle = dataGridViewCellStyle5;
            this.dataGridView.RowTemplate.DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridView.RowTemplate.DefaultCellStyle.BackColor = System.Drawing.Color.White;
            this.dataGridView.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataGridView.RowTemplate.DefaultCellStyle.ForeColor = System.Drawing.Color.Black;
            this.dataGridView.RowTemplate.DefaultCellStyle.SelectionBackColor = System.Drawing.Color.Black;
            this.dataGridView.RowTemplate.DefaultCellStyle.SelectionForeColor = System.Drawing.Color.White;
            this.dataGridView.RowTemplate.Height = 24;
            this.dataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView.Size = new System.Drawing.Size(397, 214);
            this.dataGridView.StandardTab = true;
            this.dataGridView.TabIndex = 19;
            // 
            // Semesters
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.dataGridView);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.labelid);
            this.Controls.Add(this.buttonUpdateSemester);
            this.Controls.Add(this.buttonAddSemester);
            this.Controls.Add(this.comboBoxSemesterName);
            this.Controls.Add(this.checkBoxIsCurrentSemester);
            this.Controls.Add(this.checkBoxIsCompleted);
            this.Controls.Add(this.labelSemesterName);
            this.DoubleBuffered = true;
            this.Name = "Semesters";
            this.Size = new System.Drawing.Size(521, 515);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelSemesterName;
        private System.Windows.Forms.Button buttonAddSemester;
        private System.Windows.Forms.Button buttonUpdateSemester;
        public System.Windows.Forms.Label labelid;
        public System.Windows.Forms.CheckBox checkBoxIsCompleted;
        public System.Windows.Forms.CheckBox checkBoxIsCurrentSemester;
        public System.Windows.Forms.ComboBox comboBoxSemesterName;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.DataGridView dataGridView;
    }
}
