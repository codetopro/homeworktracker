﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Homework
{
    public partial class CurrentlyStudying : UserControl
    {
        public CurrentlyStudying()
        {
            InitializeComponent();
            groupBoxAssignment.Visible = false;
            groupBoxHomework.Visible = true;
            checkBoxIsHomework.Checked = true;
            checkBoxIsAssignment.Checked = false;
        }
        public  void HomeworkOrAssignmentVisible()
        {
            if (checkBoxIsHomework.Checked == true)
            {
                groupBoxHomework.Visible = true;
                groupBoxAssignment.Visible = false;
                checkBoxIsAssignment.Checked = false;
            }
            else if (checkBoxIsAssignment.Checked==true)
            {
                groupBoxAssignment.Visible = true;
                groupBoxHomework.Visible = false;
                checkBoxIsHomework.Checked = false;
            }
        }
        private void CheckBoxIsHomework_CheckedChanged(object sender, EventArgs e)
        {
            HomeworkOrAssignmentVisible();
        }

        private void CheckBoxIsAssignment_CheckedChanged(object sender, EventArgs e)
        {
            HomeworkOrAssignmentVisible();
        }
    }
}
