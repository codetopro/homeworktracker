﻿namespace Homework
{
    partial class Assignments
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            this.buttonRefresh = new System.Windows.Forms.Button();
            this.buttonUpdateAssignment = new System.Windows.Forms.Button();
            this.buttonAddAssignment = new System.Windows.Forms.Button();
            this.groupBox = new System.Windows.Forms.GroupBox();
            this.dataGridViewAssignments = new System.Windows.Forms.DataGridView();
            this.labelAssignmentDetails = new System.Windows.Forms.Label();
            this.textBoxAssignment = new System.Windows.Forms.TextBox();
            this.labelAssignments = new System.Windows.Forms.Label();
            this.checkBoxIsCompleted = new System.Windows.Forms.CheckBox();
            this.labelid = new System.Windows.Forms.Label();
            this.dateTimePickerCompleted = new System.Windows.Forms.DateTimePicker();
            this.labelCompletedDate = new System.Windows.Forms.Label();
            this.dateTimePickerAssigned = new System.Windows.Forms.DateTimePicker();
            this.labelAssignedDate = new System.Windows.Forms.Label();
            this.textboxAssignmentTitle = new System.Windows.Forms.TextBox();
            this.labelAssignmentTitle = new System.Windows.Forms.Label();
            this.comboBoxSubjectName = new System.Windows.Forms.ComboBox();
            this.labelSubjectName = new System.Windows.Forms.Label();
            this.textBoxFileLocation = new System.Windows.Forms.TextBox();
            this.labelFileLocation = new System.Windows.Forms.Label();
            this.buttonFileLocation = new System.Windows.Forms.Button();
            this.groupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewAssignments)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonRefresh
            // 
            this.buttonRefresh.BackColor = System.Drawing.Color.White;
            this.buttonRefresh.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonRefresh.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonRefresh.Font = new System.Drawing.Font("Century Gothic", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonRefresh.Location = new System.Drawing.Point(3, 55);
            this.buttonRefresh.Name = "buttonRefresh";
            this.buttonRefresh.Size = new System.Drawing.Size(388, 41);
            this.buttonRefresh.TabIndex = 32;
            this.buttonRefresh.Text = "Refresh";
            this.buttonRefresh.UseVisualStyleBackColor = false;
            this.buttonRefresh.Click += new System.EventHandler(this.ButtonRefresh_Click);
            // 
            // buttonUpdateAssignment
            // 
            this.buttonUpdateAssignment.BackColor = System.Drawing.Color.White;
            this.buttonUpdateAssignment.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.buttonUpdateAssignment.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonUpdateAssignment.Font = new System.Drawing.Font("Century Gothic", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonUpdateAssignment.Location = new System.Drawing.Point(3, 96);
            this.buttonUpdateAssignment.Name = "buttonUpdateAssignment";
            this.buttonUpdateAssignment.Size = new System.Drawing.Size(388, 39);
            this.buttonUpdateAssignment.TabIndex = 31;
            this.buttonUpdateAssignment.Text = "Update Assignment";
            this.buttonUpdateAssignment.UseVisualStyleBackColor = false;
            this.buttonUpdateAssignment.Click += new System.EventHandler(this.ButtonUpdateAssignment_Click);
            // 
            // buttonAddAssignment
            // 
            this.buttonAddAssignment.BackColor = System.Drawing.Color.White;
            this.buttonAddAssignment.Dock = System.Windows.Forms.DockStyle.Top;
            this.buttonAddAssignment.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonAddAssignment.Font = new System.Drawing.Font("Century Gothic", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonAddAssignment.Location = new System.Drawing.Point(3, 18);
            this.buttonAddAssignment.Name = "buttonAddAssignment";
            this.buttonAddAssignment.Size = new System.Drawing.Size(388, 37);
            this.buttonAddAssignment.TabIndex = 30;
            this.buttonAddAssignment.Text = "Add Assignment";
            this.buttonAddAssignment.UseVisualStyleBackColor = false;
            this.buttonAddAssignment.Click += new System.EventHandler(this.ButtonAddAssignment_Click);
            // 
            // groupBox
            // 
            this.groupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox.Controls.Add(this.buttonRefresh);
            this.groupBox.Controls.Add(this.buttonUpdateAssignment);
            this.groupBox.Controls.Add(this.buttonAddAssignment);
            this.groupBox.Location = new System.Drawing.Point(509, 268);
            this.groupBox.Name = "groupBox";
            this.groupBox.Size = new System.Drawing.Size(394, 138);
            this.groupBox.TabIndex = 49;
            this.groupBox.TabStop = false;
            // 
            // dataGridViewAssignments
            // 
            this.dataGridViewAssignments.AllowDrop = true;
            this.dataGridViewAssignments.AllowUserToAddRows = false;
            this.dataGridViewAssignments.AllowUserToDeleteRows = false;
            this.dataGridViewAssignments.AllowUserToOrderColumns = true;
            this.dataGridViewAssignments.AllowUserToResizeColumns = false;
            this.dataGridViewAssignments.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Transparent;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.GradientActiveCaption;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewAssignments.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridViewAssignments.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewAssignments.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridViewAssignments.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridViewAssignments.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridViewAssignments.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.RaisedHorizontal;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.Transparent;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewAssignments.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridViewAssignments.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.Transparent;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewAssignments.DefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridViewAssignments.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.dataGridViewAssignments.Location = new System.Drawing.Point(0, 587);
            this.dataGridViewAssignments.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dataGridViewAssignments.MultiSelect = false;
            this.dataGridViewAssignments.Name = "dataGridViewAssignments";
            this.dataGridViewAssignments.ReadOnly = true;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.Transparent;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewAssignments.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dataGridViewAssignments.RowHeadersVisible = false;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.Transparent;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.Black;
            this.dataGridViewAssignments.RowsDefaultCellStyle = dataGridViewCellStyle5;
            this.dataGridViewAssignments.RowTemplate.DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridViewAssignments.RowTemplate.DefaultCellStyle.BackColor = System.Drawing.Color.White;
            this.dataGridViewAssignments.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataGridViewAssignments.RowTemplate.DefaultCellStyle.ForeColor = System.Drawing.Color.Black;
            this.dataGridViewAssignments.RowTemplate.DefaultCellStyle.SelectionBackColor = System.Drawing.Color.Black;
            this.dataGridViewAssignments.RowTemplate.DefaultCellStyle.SelectionForeColor = System.Drawing.Color.White;
            this.dataGridViewAssignments.RowTemplate.Height = 24;
            this.dataGridViewAssignments.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewAssignments.Size = new System.Drawing.Size(943, 158);
            this.dataGridViewAssignments.StandardTab = true;
            this.dataGridViewAssignments.TabIndex = 48;
            this.dataGridViewAssignments.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridViewAssignments_CellClick);
            // 
            // labelAssignmentDetails
            // 
            this.labelAssignmentDetails.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelAssignmentDetails.AutoSize = true;
            this.labelAssignmentDetails.Font = new System.Drawing.Font("Century Gothic", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelAssignmentDetails.ForeColor = System.Drawing.Color.Black;
            this.labelAssignmentDetails.Location = new System.Drawing.Point(36, 548);
            this.labelAssignmentDetails.Name = "labelAssignmentDetails";
            this.labelAssignmentDetails.Size = new System.Drawing.Size(183, 23);
            this.labelAssignmentDetails.TabIndex = 47;
            this.labelAssignmentDetails.Text = "Assignment Details";
            // 
            // textBoxAssignment
            // 
            this.textBoxAssignment.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.textBoxAssignment.BackColor = System.Drawing.SystemColors.Control;
            this.textBoxAssignment.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxAssignment.ForeColor = System.Drawing.Color.Black;
            this.textBoxAssignment.Location = new System.Drawing.Point(40, 248);
            this.textBoxAssignment.Multiline = true;
            this.textBoxAssignment.Name = "textBoxAssignment";
            this.textBoxAssignment.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxAssignment.Size = new System.Drawing.Size(394, 215);
            this.textBoxAssignment.TabIndex = 46;
            // 
            // labelAssignments
            // 
            this.labelAssignments.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.labelAssignments.AutoSize = true;
            this.labelAssignments.Font = new System.Drawing.Font("Century Gothic", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelAssignments.ForeColor = System.Drawing.Color.Black;
            this.labelAssignments.Location = new System.Drawing.Point(36, 212);
            this.labelAssignments.Name = "labelAssignments";
            this.labelAssignments.Size = new System.Drawing.Size(124, 23);
            this.labelAssignments.TabIndex = 45;
            this.labelAssignments.Text = "Assignments";
            // 
            // checkBoxIsCompleted
            // 
            this.checkBoxIsCompleted.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBoxIsCompleted.AutoSize = true;
            this.checkBoxIsCompleted.BackColor = System.Drawing.Color.Transparent;
            this.checkBoxIsCompleted.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxIsCompleted.ForeColor = System.Drawing.Color.Black;
            this.checkBoxIsCompleted.Location = new System.Drawing.Point(509, 212);
            this.checkBoxIsCompleted.Name = "checkBoxIsCompleted";
            this.checkBoxIsCompleted.Size = new System.Drawing.Size(237, 29);
            this.checkBoxIsCompleted.TabIndex = 44;
            this.checkBoxIsCompleted.Text = "Assignment Completed";
            this.checkBoxIsCompleted.UseVisualStyleBackColor = false;
            // 
            // labelid
            // 
            this.labelid.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelid.AutoSize = true;
            this.labelid.Location = new System.Drawing.Point(921, 0);
            this.labelid.Name = "labelid";
            this.labelid.Size = new System.Drawing.Size(19, 17);
            this.labelid.TabIndex = 43;
            this.labelid.Text = "id";
            this.labelid.Visible = false;
            // 
            // dateTimePickerCompleted
            // 
            this.dateTimePickerCompleted.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dateTimePickerCompleted.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.dateTimePickerCompleted.Location = new System.Drawing.Point(509, 155);
            this.dateTimePickerCompleted.Name = "dateTimePickerCompleted";
            this.dateTimePickerCompleted.Size = new System.Drawing.Size(394, 32);
            this.dateTimePickerCompleted.TabIndex = 42;
            // 
            // labelCompletedDate
            // 
            this.labelCompletedDate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelCompletedDate.AutoSize = true;
            this.labelCompletedDate.Font = new System.Drawing.Font("Century Gothic", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCompletedDate.ForeColor = System.Drawing.Color.Black;
            this.labelCompletedDate.Location = new System.Drawing.Point(505, 113);
            this.labelCompletedDate.Name = "labelCompletedDate";
            this.labelCompletedDate.Size = new System.Drawing.Size(165, 23);
            this.labelCompletedDate.TabIndex = 41;
            this.labelCompletedDate.Text = "Completed Date";
            // 
            // dateTimePickerAssigned
            // 
            this.dateTimePickerAssigned.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dateTimePickerAssigned.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.dateTimePickerAssigned.Location = new System.Drawing.Point(40, 155);
            this.dateTimePickerAssigned.Name = "dateTimePickerAssigned";
            this.dateTimePickerAssigned.Size = new System.Drawing.Size(394, 32);
            this.dateTimePickerAssigned.TabIndex = 40;
            // 
            // labelAssignedDate
            // 
            this.labelAssignedDate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelAssignedDate.AutoSize = true;
            this.labelAssignedDate.Font = new System.Drawing.Font("Century Gothic", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelAssignedDate.ForeColor = System.Drawing.Color.Black;
            this.labelAssignedDate.Location = new System.Drawing.Point(36, 113);
            this.labelAssignedDate.Name = "labelAssignedDate";
            this.labelAssignedDate.Size = new System.Drawing.Size(143, 23);
            this.labelAssignedDate.TabIndex = 39;
            this.labelAssignedDate.Text = "Assigned Date";
            // 
            // textboxAssignmentTitle
            // 
            this.textboxAssignmentTitle.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textboxAssignmentTitle.BackColor = System.Drawing.SystemColors.Control;
            this.textboxAssignmentTitle.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textboxAssignmentTitle.ForeColor = System.Drawing.Color.Black;
            this.textboxAssignmentTitle.Location = new System.Drawing.Point(509, 53);
            this.textboxAssignmentTitle.MaxLength = 40;
            this.textboxAssignmentTitle.Name = "textboxAssignmentTitle";
            this.textboxAssignmentTitle.Size = new System.Drawing.Size(394, 32);
            this.textboxAssignmentTitle.TabIndex = 38;
            // 
            // labelAssignmentTitle
            // 
            this.labelAssignmentTitle.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelAssignmentTitle.AutoSize = true;
            this.labelAssignmentTitle.Font = new System.Drawing.Font("Century Gothic", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelAssignmentTitle.ForeColor = System.Drawing.Color.Black;
            this.labelAssignmentTitle.Location = new System.Drawing.Point(505, 13);
            this.labelAssignmentTitle.Name = "labelAssignmentTitle";
            this.labelAssignmentTitle.Size = new System.Drawing.Size(155, 23);
            this.labelAssignmentTitle.TabIndex = 37;
            this.labelAssignmentTitle.Text = "Assignment title";
            // 
            // comboBoxSubjectName
            // 
            this.comboBoxSubjectName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxSubjectName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxSubjectName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxSubjectName.FormattingEnabled = true;
            this.comboBoxSubjectName.Location = new System.Drawing.Point(40, 53);
            this.comboBoxSubjectName.Name = "comboBoxSubjectName";
            this.comboBoxSubjectName.Size = new System.Drawing.Size(394, 33);
            this.comboBoxSubjectName.Sorted = true;
            this.comboBoxSubjectName.TabIndex = 36;
            // 
            // labelSubjectName
            // 
            this.labelSubjectName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelSubjectName.AutoSize = true;
            this.labelSubjectName.Font = new System.Drawing.Font("Century Gothic", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSubjectName.ForeColor = System.Drawing.Color.Black;
            this.labelSubjectName.Location = new System.Drawing.Point(36, 13);
            this.labelSubjectName.Name = "labelSubjectName";
            this.labelSubjectName.Size = new System.Drawing.Size(141, 23);
            this.labelSubjectName.TabIndex = 35;
            this.labelSubjectName.Text = "Subject Name";
            // 
            // textBoxFileLocation
            // 
            this.textBoxFileLocation.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxFileLocation.BackColor = System.Drawing.SystemColors.Control;
            this.textBoxFileLocation.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxFileLocation.ForeColor = System.Drawing.Color.Black;
            this.textBoxFileLocation.Location = new System.Drawing.Point(177, 491);
            this.textBoxFileLocation.MaxLength = 40;
            this.textBoxFileLocation.Name = "textBoxFileLocation";
            this.textBoxFileLocation.ReadOnly = true;
            this.textBoxFileLocation.Size = new System.Drawing.Size(665, 32);
            this.textBoxFileLocation.TabIndex = 52;
            // 
            // labelFileLocation
            // 
            this.labelFileLocation.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelFileLocation.AutoSize = true;
            this.labelFileLocation.Font = new System.Drawing.Font("Century Gothic", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelFileLocation.ForeColor = System.Drawing.Color.Black;
            this.labelFileLocation.Location = new System.Drawing.Point(36, 495);
            this.labelFileLocation.Name = "labelFileLocation";
            this.labelFileLocation.Size = new System.Drawing.Size(135, 23);
            this.labelFileLocation.TabIndex = 51;
            this.labelFileLocation.Text = "File Location :";
            // 
            // buttonFileLocation
            // 
            this.buttonFileLocation.BackColor = System.Drawing.Color.Transparent;
            this.buttonFileLocation.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonFileLocation.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonFileLocation.Font = new System.Drawing.Font("Century Gothic", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonFileLocation.Location = new System.Drawing.Point(849, 486);
            this.buttonFileLocation.Name = "buttonFileLocation";
            this.buttonFileLocation.Size = new System.Drawing.Size(54, 37);
            this.buttonFileLocation.TabIndex = 55;
            this.buttonFileLocation.TabStop = false;
            this.buttonFileLocation.Text = ". . .";
            this.buttonFileLocation.UseVisualStyleBackColor = false;
            this.buttonFileLocation.Click += new System.EventHandler(this.ButtonFileLocation_Click);
            // 
            // Assignments
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.buttonFileLocation);
            this.Controls.Add(this.textBoxFileLocation);
            this.Controls.Add(this.labelFileLocation);
            this.Controls.Add(this.groupBox);
            this.Controls.Add(this.dataGridViewAssignments);
            this.Controls.Add(this.labelAssignmentDetails);
            this.Controls.Add(this.textBoxAssignment);
            this.Controls.Add(this.labelAssignments);
            this.Controls.Add(this.checkBoxIsCompleted);
            this.Controls.Add(this.labelid);
            this.Controls.Add(this.dateTimePickerCompleted);
            this.Controls.Add(this.labelCompletedDate);
            this.Controls.Add(this.dateTimePickerAssigned);
            this.Controls.Add(this.labelAssignedDate);
            this.Controls.Add(this.textboxAssignmentTitle);
            this.Controls.Add(this.labelAssignmentTitle);
            this.Controls.Add(this.comboBoxSubjectName);
            this.Controls.Add(this.labelSubjectName);
            this.DoubleBuffered = true;
            this.Name = "Assignments";
            this.Size = new System.Drawing.Size(943, 745);
            this.groupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewAssignments)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button buttonRefresh;
        private System.Windows.Forms.Button buttonUpdateAssignment;
        private System.Windows.Forms.Button buttonAddAssignment;
        private System.Windows.Forms.GroupBox groupBox;
        public System.Windows.Forms.DataGridView dataGridViewAssignments;
        private System.Windows.Forms.Label labelAssignmentDetails;
        public System.Windows.Forms.TextBox textBoxAssignment;
        private System.Windows.Forms.Label labelAssignments;
        public System.Windows.Forms.CheckBox checkBoxIsCompleted;
        public System.Windows.Forms.Label labelid;
        private System.Windows.Forms.DateTimePicker dateTimePickerCompleted;
        private System.Windows.Forms.Label labelCompletedDate;
        private System.Windows.Forms.DateTimePicker dateTimePickerAssigned;
        private System.Windows.Forms.Label labelAssignedDate;
        public System.Windows.Forms.TextBox textboxAssignmentTitle;
        private System.Windows.Forms.Label labelAssignmentTitle;
        public System.Windows.Forms.ComboBox comboBoxSubjectName;
        private System.Windows.Forms.Label labelSubjectName;
        public System.Windows.Forms.TextBox textBoxFileLocation;
        private System.Windows.Forms.Label labelFileLocation;
        private System.Windows.Forms.Button buttonFileLocation;
    }
}
