﻿using Homework.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using LogicLayer;

namespace Homework
{
    public partial class HomePage : Form
    {
        private bool sliderIsEnabled = false;
        private bool IsPannelDetailVisiblle=false;
        private string UserName;
        public int Userid;
        Logic logic = new Logic();
        private string SemesterName;
        private string HomeworksNo;
        private string semesterId;
        private bool isHomework;
        private bool isCurrentlyStudying;
        private bool isAssignment;
        private  Color Changes;
        public HomePage(int userId,Color ch)
        {
            InitializeComponent();
            Userid=userId;
            Changes = ch;
            loadInformations();
            
        }
        private void loadInformations()
        {
            this.MaximizedBounds = Screen.FromHandle(this.Handle).WorkingArea;
            this.WindowState = FormWindowState.Maximized;
            panelRight.Width = 66;
            labelUserNameWithComent.Text = "";
            GetSemesteinfo();
            UserName =logic.GetFullName(Userid);
            buttonUserDetails.Text = UserName;
            buttonCalender.Text = "";
            buttonSemesters.Text = "";
            buttonSubjects.Text = "";
            buttonCurrentStudying.Text = "";
            buttonSettings.Text = "";
            buttonPnnelLeft.Text = "";
            buttonLogoff.Text = "";
            buttonAssignment.Text = "";
            buttonHomework.Text = "";
            buttonChangeColor.Text = "";
            panelDetails.Visible = false;
            panelBody.BackgroundImage = Resources.graduate;
        }
        private void GetSemesteinfo()
        {
            try
            {
                var data = Logic.GetTableByQuery("Select SemesterId From Semester Where IsCurrentSemester!=0 ");
                semesterId = (data.Rows[0][0]).ToString();
                var details = Logic.GetTableByQuery("SELECT SemesterName FROM Semester WHERE SemesterId= " + semesterId);
                SemesterName = (details.Rows[0][0]).ToString();
                string date ="'"+ DateTime.Now.ToShortDateString()+"'";
                var HomeworkNO = Logic.GetTableByQuery("Select Count(SubjectId) from DailyHomeworks  where IsCompleted=0 and AssignedDate="+date);
                HomeworksNo = (HomeworkNO.Rows[0][0]).ToString();
                labelUserNameWithComent.Text = SemesterName + " " + DateTime.Now.ToShortDateString() + " : " + "You have "+ HomeworksNo + " incomplete homework today.";
            }
            catch (Exception)
            {
               // semesterId = "1";
            }
           
        }
     
        #region//Dragable Codes
        private bool _Dragging = false;
        private Point _start_point = new Point(0, 0);
        private void MouseToDown(object sender, MouseEventArgs e)
        {
            _Dragging = true;
            _start_point = new Point(e.X, e.Y);
        }

        private void MouseToUp(object sender, MouseEventArgs e)
        {
            _Dragging = false;
        }

        private void MouseToMove(object sender, MouseEventArgs e)
        {
            if (_Dragging)
            {
                Point p = PointToScreen(e.Location);
                Location = new Point(p.X - this._start_point.X, p.Y - this._start_point.Y);
            }
        }

        #endregion

        private void MakePannelDetailsVisible(bool COndition,string Button,Image image)
        {
            buttonPnnelLeft.Text = Button;
            buttonPnnelLeft.Image = image;
            panelDetails.Visible = true;
            if (!IsPannelDetailVisiblle && COndition)
            {
                if ((WindowState == FormWindowState.Maximized))
                {
                    panelDetails.Width = 300;
                    buttonUserDetails.Visible = true;
                }
                else if ((WindowState == FormWindowState.Normal))
                {
                    panelDetails.Width = 250;
                    buttonUserDetails.Visible = false;
                }
                IsPannelDetailVisiblle = false;
                panelBody.BackgroundImage = null;
            }
            else
            {
               // panelBody.BackgroundImage = null;
                panelBody.BackgroundImage = Resources.graduate;
                panelBody.Controls.Clear();
                panelDetails.Width = 0;
                IsPannelDetailVisiblle = false;
            }
            GetSemesteinfo();
        }

        private void ButtonSubjects_Click(object sender, EventArgs e)
        {
            try
            {
                dataGridViewSubjects.BringToFront();
                MakePannelDetailsVisible(true, "Subjects", Resources.Subjs);
                dataGridViewSubjects.DataSource = Logic.DisplaySubjectDetails();
                dataGridViewSubjects.Columns[0].Visible = false;
            }
            catch (Exception)
            {
            }
        }

        private void ButtonCurrentStudying_Click(object sender, EventArgs e)
        {
            try
            {
                isCurrentlyStudying = true;
                isHomework = false;
                isAssignment = false;
                dataGridViewCurrentlyStuding.BringToFront();
                MakePannelDetailsVisible(true, "Currently Studying", Resources.CurentSt);
                dataGridViewCurrentlyStuding.DataSource = Logic.DisplayCurrentlyStudingDetails(semesterId);
                dataGridViewCurrentlyStuding.Columns[0].Visible = false;
            }
            catch (Exception)
            {
            }
        }

        public void ButtonSemesters_Click(object sender, EventArgs e)
        {
            try
            {
                dataGridViewSemesters.BringToFront();
                MakePannelDetailsVisible(true, "Semesters", Resources.Sems);
                dataGridViewSemesters.DataSource = Logic.DisplaySemesterDetails();
                dataGridViewSemesters.Columns[0].Visible = false;
            }
            catch (Exception)
            {
            }
        }

        private void ButtonSettings_Click(object sender, EventArgs e)
        {
            MakePannelDetailsVisible(false, "", null);
            panelBody.BackgroundImage = null;
            panelBody.Controls.Clear();
            Settings settings = new Settings();
            panelBody.Controls.Add(settings);
            GetSemesteinfo();
        }

        private void ButtonUserDetails_Click(object sender, EventArgs e)
        {

            MakePannelDetailsVisible(false,"",null);
            panelBody.BackgroundImage = null;
            panelBody.Controls.Clear();
            UserInformation user = new UserInformation(Userid);
            panelBody.Controls.Add(user);
        }
        private void ButtonCalender_Click(object sender, EventArgs e)
        {
            MonthCalendar month = new MonthCalendar();
            month.SetCalendarDimensions(3, 3);
            MakePannelDetailsVisible(false, "",null);
            panelBody.Controls.Clear();
            panelBody.Controls.Add(month);
        }

        #region// Less used
        private void PanelBody_Click(object sender, EventArgs e)
        {
          //  MakePannelDetailsVisible(false, "");
        }

        private void PanelTop_Click(object sender, EventArgs e)
        {
            MakePannelDetailsVisible(false,"",null);
        }

        private void PanelSlogan_Click(object sender, EventArgs e)
        {
                MakePannelDetailsVisible(false,"",null);
        }

        private void PanelRight_Click(object sender, EventArgs e)
        {
            MakePannelDetailsVisible(false,"",null);
        }

        private void ButtonExit_Click(object sender, EventArgs e)
        {
            Application.ExitThread();
            Application.Exit();
        }

        private void ButtonMaximize_Click(object sender, EventArgs e)
        {
            if (WindowState == FormWindowState.Normal)
            {
                WindowState = FormWindowState.Maximized;
                buttonUserDetails.Visible = true;
            }
            else if (WindowState == FormWindowState.Maximized)
            {
                WindowState = FormWindowState.Normal;
                buttonUserDetails.Visible = false;
            }
        }

        private void ButtonMimize_Click(object sender, EventArgs e)
        {
            if (WindowState == FormWindowState.Normal)
            {
                WindowState = FormWindowState.Minimized;
            }
            else if (WindowState == FormWindowState.Maximized)
            {
                WindowState = FormWindowState.Minimized;
            }
        }

        private void ButtonSlider_Click(object sender, EventArgs e)
        {
            if (!sliderIsEnabled)
            {
                buttonSlider.Image = Resources.SL;
                buttonSlider.Text = "Hide";
                buttonCalender.Text = "Calender";
                buttonSemesters.Text = "Semesters";
                buttonSubjects.Text = "Subjects";
                buttonCurrentStudying.Text = "Currently Studying";
                buttonSettings.Text = "Settings";
                buttonLogoff.Text = "Log off";
                buttonAssignment.Text = "Assignment";
                buttonHomework.Text = "Homework";
                buttonChangeColor.Text = "Change Theme";

                if ((WindowState == FormWindowState.Maximized))
                {
                    panelRight.Width = 250;
                }
                else if ((WindowState == FormWindowState.Normal))
                {
                    panelRight.Width = 300;
                }
                sliderIsEnabled = true;
            }
            else if (sliderIsEnabled)
            {
                buttonSlider.Image = Resources.SR;
                buttonSlider.Text = "Show";
                buttonCalender.Text = "";
                buttonSemesters.Text = "";
                buttonSubjects.Text = "";
                buttonCurrentStudying.Text = "";
                buttonSettings.Text = "";
                buttonLogoff.Text = "";
                buttonHomework.Text = "";
                buttonAssignment.Text = "";
                buttonChangeColor.Text = "";
                panelRight.Width = 66;
                sliderIsEnabled = false;
            }
            GetSemesteinfo();
           // MakePannelDetailsVisible(false, "",null);
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            labelTime.Text = DateTime.Now.ToLongTimeString();
        }

        #endregion

        private void HomePage_Load(object sender, EventArgs e)
        {
            MakePannelDetailsVisible(false, "",null);
        }

        private void ButtonLogoff_Click(object sender, EventArgs e)
        {
            MakePannelDetailsVisible(false, "",null);
            DialogResult result = MessageBox.Show("Are you sure you want to logoff "+UserName+" ?","Log off",MessageBoxButtons.YesNo);
            if (result == DialogResult.Yes)
            {
                LoginForm login = new LoginForm();
                login.Show();
                this.Hide();
            }
            else { }
        }

        #region//Mouse hover and leave for buttons
        private void Button_MouseHover(Button button)
        {
            if (button.BackColor == Color.Black)
            {
                button.BackColor = Color.White;
            }
            else if (button.BackColor == Color.FromArgb(64,64,64))
            {
                button.BackColor = Color.Black;
            }
            else if (button.BackColor == Color.FromArgb(64, 64, 0))
            {
                button.BackColor = Color.FromArgb(102, 102, 3);
            }
            else if (button.BackColor == Color.Maroon)
            {
                button.BackColor = Color.Red;
            }
            else
            {

            }
        }

        private void Button_MouseLeave(Button button)
        {
            button.BackColor = Changes;
        }

        private void ButtonSlider_MouseHover(object sender, EventArgs e)
        {
            Button_MouseHover(buttonSlider);
        }

        private void ButtonSlider_MouseLeave(object sender, EventArgs e)
        {
            Button_MouseLeave(buttonSlider);
        }

        private void ButtonCalender_MouseHover(object sender, EventArgs e)
        {
            Button_MouseHover(buttonCalender);
        }

        private void ButtonCalender_MouseLeave(object sender, EventArgs e)
        {
            Button_MouseLeave(buttonCalender);
        }

        private void ButtonSemesters_MouseHover(object sender, EventArgs e)
        {
            Button_MouseHover(buttonSemesters);
        }

        private void ButtonSemesters_MouseLeave(object sender, EventArgs e)
        {
            Button_MouseLeave(buttonSemesters);
        }

        private void ButtonSubjects_MouseHover(object sender, EventArgs e)
        {
            Button_MouseHover(buttonSubjects);
        }

        private void ButtonSubjects_MouseLeave(object sender, EventArgs e)
        {
            Button_MouseLeave(buttonSubjects);
        }

        private void ButtonCurrentStudying_MouseHover(object sender, EventArgs e)
        {
            Button_MouseHover(buttonCurrentStudying);
        }

        private void ButtonCurrentStudying_MouseLeave(object sender, EventArgs e)
        {
            Button_MouseLeave(buttonCurrentStudying);
        }

        private void ButtonLogoff_MouseHover(object sender, EventArgs e)
        {
            Button_MouseHover(buttonLogoff);
        }

        private void ButtonLogoff_MouseLeave(object sender, EventArgs e)
        {
            Button_MouseLeave(buttonLogoff);
        }

        private void ButtonSettings_MouseHover(object sender, EventArgs e)
        {
            Button_MouseHover(buttonSettings);
        }

        private void ButtonSettings_MouseLeave(object sender, EventArgs e)
        {
            Button_MouseLeave(buttonSettings);
        }

        private void ButtonUserDetails_MouseHover(object sender, EventArgs e)
        {
            Button_MouseHover(buttonUserDetails);
        }

        private void ButtonUserDetails_MouseLeave(object sender, EventArgs e)
        {
            Button_MouseLeave(buttonUserDetails);
        }

        private void ButtonMimize_MouseHover(object sender, EventArgs e)
        {
            Button_MouseHover(buttonMimize);
        }

        private void ButtonMimize_MouseLeave(object sender, EventArgs e)
        {
            Button_MouseLeave(buttonMimize);
        }

        private void ButtonMaximize_MouseHover(object sender, EventArgs e)
        {
            Button_MouseHover(buttonMaximize);
        }

        private void ButtonMaximize_MouseLeave(object sender, EventArgs e)
        {
            Button_MouseLeave(buttonMaximize);
        }

        private void ButtonExit_MouseHover(object sender, EventArgs e)
        {
            Button_MouseHover(buttonExit);
            
        }

        private void ButtonExit_MouseLeave(object sender, EventArgs e)
        {
            Button_MouseLeave(buttonExit);
        }

        private void ButtonPnnelLeft_MouseHover(object sender, EventArgs e)
        {
            Button_MouseHover(buttonPnnelLeft);
        }

        private void ButtonPnnelLeft_MouseLeave(object sender, EventArgs e)
        {
            Button_MouseLeave(buttonPnnelLeft);
        }


        private void ButtonHomework_MouseHover(object sender, EventArgs e)
        {
            Button_MouseHover(buttonHomework);
        }

        private void ButtonHomework_MouseLeave(object sender, EventArgs e)
        {
            Button_MouseLeave(buttonHomework);
        }

        private void ButtonAssignment_MouseHover(object sender, EventArgs e)
        {
            Button_MouseHover(buttonAssignment);
        }

        private void ButtonAssignment_MouseLeave(object sender, EventArgs e)
        {
            Button_MouseLeave(buttonAssignment);
        }

        private void ButtonChangeColor_MouseHover(object sender, EventArgs e)
        {
            Button_MouseHover(buttonChangeColor);
        }

        private void ButtonChangeColor_MouseLeave(object sender, EventArgs e)
        {
            Button_MouseLeave(buttonChangeColor);
        }
        #endregion


        private void ButtonPnnelLeft_Click(object sender, EventArgs e)
        {
            if (buttonPnnelLeft.Text== "Semesters")
            {
                panelBody.BackgroundImage = null;
                panelBody.Controls.Clear();
                Semesters semesters = new Semesters();
                panelBody.Controls.Add(semesters);
                ButtonSemesters_Click(sender,e);
            }
             if (buttonPnnelLeft.Text == "Subjects")
            {
                panelBody.BackgroundImage = null;
                panelBody.Controls.Clear();
                Subject subject = new Subject();
                panelBody.Controls.Add(subject);
                ButtonSubjects_Click(sender, e);
            }
            if (buttonPnnelLeft.Text == "Homework")
            {
                panelBody.BackgroundImage = null;
                panelBody.Controls.Clear();
                Homework homework = new Homework(semesterId);
                panelBody.Controls.Add(homework);
                ButtonHomework_Click(sender, e);
            }
            if (buttonPnnelLeft.Text == "Assignment")
            {
                panelBody.BackgroundImage = null;
                panelBody.Controls.Clear();
                Assignments assignments = new Assignments(semesterId);
                panelBody.Controls.Add(assignments);
                ButtonAssignment_Click(sender, e);
            }
            if (buttonPnnelLeft.Text == "Currently Studying")
            {
                panelBody.BackgroundImage = null;
                panelBody.Controls.Clear();
                CurrentlyStudying currently = new CurrentlyStudying();
                panelBody.Controls.Add(currently);
                ButtonCurrentStudying_Click(sender, e);
            }
            GetSemesteinfo();
            //panelBody.BackgroundImage = Resources.graduate;
        }

        private void DataGridViewSemesters_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            Semesters sem = new Semesters();
            int id = Convert.ToInt32(dataGridViewSemesters.Rows[dataGridViewSemesters.CurrentRow.Index].Cells[0].Value);
            sem.labelid.Text = id.ToString();
            sem.comboBoxSemesterName.Text = dataGridViewSemesters.CurrentRow.Cells[1].Value.ToString();
            var data = Logic.GetTableByQuery("SELECT IsCompleted FROM Semester WHERE SemesterId = " + id);
            int Complete= Convert.ToInt32(data.Rows[0][0]);
            var datas = Logic.GetTableByQuery("SELECT IsCurrentSemester FROM Semester WHERE SemesterId = " + id);
            int current = Convert.ToInt32(datas.Rows[0][0]);
            if (Complete == 1)
            {
                sem.checkBoxIsCompleted.Checked = true;
            }
            else
            {
                sem.checkBoxIsCompleted.Checked = false;
            }
            if (current == 1)
            {
                sem.checkBoxIsCurrentSemester.Checked = true;
            }
            else
            {
                sem.checkBoxIsCurrentSemester.Checked = false;
            }
            sem.getSubjects(id.ToString());
            panelBody.Controls.Clear();
            panelBody.Controls.Add(sem);
        }

        private void DataGridViewSubjects_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            Subject sub = new Subject();
            int id = Convert.ToInt32(dataGridViewSubjects.Rows[dataGridViewSubjects.CurrentRow.Index].Cells[0].Value);
            sub.labelid.Text = id.ToString();
            var datam = Logic.GetTableByQuery("SELECT SemesterId from Subjects where SubjectId= " + id);
            int SemId = Convert.ToInt32(datam.Rows[0][0]);
            sub.comboBoxSemesterName.SelectedValue = SemId;
            var data = Logic.GetTableByQuery("SELECT SubjectName FROM Subjects WHERE SubjectId= " + id);
            string Subject =(data.Rows[0][0]).ToString();
            var datas = Logic.GetTableByQuery("SELECT SubjecTeacher FROM Subjects WHERE SubjectId  = " + id);
            string Teacher = (datas.Rows[0][0]).ToString();
            sub.textboxSubjectName.Text = Subject;
            sub.textBoxTeacherName.Text = Teacher;
            panelBody.Controls.Clear();
            panelBody.Controls.Add(sub);
        }

        private void ButtonHomework_Click(object sender, EventArgs e)
        {
            try
            {
                isHomework = true;
                isCurrentlyStudying = false;
                isAssignment = false;
                dataGridViewCurrentlyStuding.BringToFront();
                MakePannelDetailsVisible(true, "Homework", Resources.HWS);
                dataGridViewCurrentlyStuding.DataSource = Logic.DisplayCurrentlyStudingDetails(semesterId);
                dataGridViewCurrentlyStuding.Columns[0].Visible = false;
            }
            catch (Exception)
            {
            }
            GetSemesteinfo();
        }

        private void ButtonAssignment_Click(object sender, EventArgs e)
        {
            try
            {
                isAssignment = true;
                isHomework = false;
                isCurrentlyStudying = false;
                dataGridViewCurrentlyStuding.BringToFront();
                MakePannelDetailsVisible(true, "Assignment", Resources.Assignm);
                dataGridViewCurrentlyStuding.DataSource = Logic.DisplayCurrentlyStudingDetails(semesterId);
                dataGridViewCurrentlyStuding.Columns[0].Visible = false;
            }
            catch (Exception)
            {
            }
           
        }

        private void ShowToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Show();
        }

        private void ExitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.ExitThread();
            Application.Exit();
        }

        private void HomePage_Move(object sender, EventArgs e)
        {
            if (this.WindowState==FormWindowState.Minimized)
            {
                this.Hide();
                notifyIcon.ShowBalloonTip(1000, "Homework", "Homework is still running select the item in tray.", ToolTipIcon.Info);
            }
        }

        private void NotifyIcon_DoubleClick(object sender, EventArgs e)
        {
            this.Show();
        }

        private void LogOffToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Show();
            ButtonLogoff_Click(sender, e);
        }
        private void DataGridViewCurrentlyStuding_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (isCurrentlyStudying)
                {

                    CurrentlyStudying studying = new CurrentlyStudying();
                    studying.HomeworkOrAssignmentVisible();
                    int id = Convert.ToInt32(dataGridViewCurrentlyStuding.Rows[dataGridViewCurrentlyStuding.CurrentRow.Index].Cells[0].Value);
                    studying.labelid.Text = id.ToString();
                    if (studying.checkBoxIsHomework.Checked==true)
                    {
                        studying.textBoxHomeworkSubjectName.Text = dataGridViewCurrentlyStuding.Rows[dataGridViewCurrentlyStuding.CurrentRow.Index].Cells[1].Value.ToString();
                        var data = Logic.GetTableByQuery("Select MAX(AssignedDate) From DailyHomeworks");
                        string Date = (data.Rows[0][0]).ToString();
                        studying.dateTimePickerAssignedHomework.Text = Date;
                        var datas = Logic.GetTableByQuery("Select top 1 HomeworkTitle From DailyHomeworks where AssignedDate='" + Date + "' And SubjectId=" + id + " ORDER BY DailyHomeworksId DESC");
                        string HwTitle = (datas.Rows[0][0]).ToString();
                        studying.textboxHomeworkTitle.Text = HwTitle;
                        var datam = Logic.GetTableByQuery("Select top 1 Homework From DailyHomeworks where AssignedDate='" + Date + "' And SubjectId=" + id + " ORDER BY DailyHomeworksId DESC");
                        studying.dataGridViewHomework.DataSource = datam;
                        panelBody.Controls.Clear();
                        panelBody.Controls.Add(studying);
                    }
                     if (studying.checkBoxIsAssignment.Checked==false)
                    {
                        studying.textBoxAssignmentSubjectName.Text = dataGridViewCurrentlyStuding.Rows[dataGridViewCurrentlyStuding.CurrentRow.Index].Cells[1].Value.ToString();
                        var dataaa = Logic.GetTableByQuery("Select MAX(AssignedDate) From Assignment");
                        string Dateam = (dataaa.Rows[0][0]).ToString();
                        studying.dateTimePickerAssignedAssignment.Text = Dateam;
                        var Title = Logic.GetTableByQuery("Select top 1 AssignmentTitle From Assignment where AssignedDate='" + Dateam + "' And SubjectId=" + id + " ORDER BY AssignmentId DESC");
                        string ATitle = (Title.Rows[0][0]).ToString();
                        studying.textBoxAssignmentTitle.Text = ATitle;
                        var datamn = Logic.GetTableByQuery("Select top 1 Assignment From Assignment where AssignedDate='" + Dateam + "' And SubjectId=" + id + " ORDER BY AssignmentId DESC");
                        studying.dataGridViewAssignments.DataSource = datamn;
                        panelBody.Controls.Clear();
                        panelBody.Controls.Add(studying);
                    }
                }
                if (isHomework)
                {
                    Homework homework = new Homework(semesterId);
                    int id = Convert.ToInt32(dataGridViewCurrentlyStuding.Rows[dataGridViewCurrentlyStuding.CurrentRow.Index].Cells[0].Value);
                    homework.labelid.Text = id.ToString();
                    homework.comboBoxSubjectName.Text = dataGridViewCurrentlyStuding.Rows[dataGridViewCurrentlyStuding.CurrentRow.Index].Cells[1].Value.ToString();
                    homework.dataGridView.DataSource = Logic.DisplayHomeworkDetails(id);
                    homework.dataGridView.Columns[0].Visible = false;
                    panelBody.Controls.Clear();
                    panelBody.Controls.Add(homework);
                }
                if (isAssignment)
                {
                    Assignments assignments = new Assignments(semesterId);
                    int id = Convert.ToInt32(dataGridViewCurrentlyStuding.Rows[dataGridViewCurrentlyStuding.CurrentRow.Index].Cells[0].Value);
                    assignments.labelid.Text = id.ToString();
                    assignments.comboBoxSubjectName.Text = dataGridViewCurrentlyStuding.Rows[dataGridViewCurrentlyStuding.CurrentRow.Index].Cells[1].Value.ToString();
                    assignments.dataGridViewAssignments.DataSource = Logic.DisplayAssignmentDetails(id);
                    assignments.dataGridViewAssignments.Columns[0].Visible = false;
                    panelBody.Controls.Clear();
                    panelBody.Controls.Add(assignments);
                }
            }
            catch (Exception)
            {
            }
           
        }

        private void ButtonChangeColor_Click(object sender, EventArgs e)
        {
            try
            {
                ChangeColor changeColor = new ChangeColor(Userid);
                LoginForm login = new LoginForm();
                Signup signup = new Signup();
                changeColor.ShowDialog();
                Changes = changeColor.Getcolor();
                login.changecolor = Changes;
                signup.makecolor = Changes;
                panelRight.BackColor = Changes;
                panelTop.BackColor = Changes;
                panelDetails.BackColor = Changes;
                dataGridViewCurrentlyStuding.BackgroundColor = Changes;
                dataGridViewSemesters.BackgroundColor = Changes;
                dataGridViewSubjects.BackgroundColor = Changes;
            }
            catch (Exception)
            {      
            }
           
        }

    }
}
