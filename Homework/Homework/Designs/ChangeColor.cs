﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Homework
{
    public partial class ChangeColor : Form
    {
        int uid;
        public ChangeColor( int usi)
        {
            InitializeComponent();
            uid = usi;
        }

        private void PictureBox64640_Click(object sender, EventArgs e)
        {
            panelTest.BackColor= pictureBox64640.BackColor;
        }

        private void PictureBoxNavy_Click(object sender, EventArgs e)
        {
            panelTest.BackColor = pictureBoxNavy.BackColor;
        }

        private void PictureBoxMaroon_Click(object sender, EventArgs e)
        {
            panelTest.BackColor = pictureBoxMaroon.BackColor;
        }

        private void PictureBoxBlack_Click(object sender, EventArgs e)
        {
            panelTest.BackColor = pictureBoxBlack.BackColor;
        }

        private void PictureBox646464_Click(object sender, EventArgs e)
        {
            panelTest.BackColor = pictureBox646464.BackColor;
        }


        public Color Getcolor()
        {
            return panelTest.BackColor;
        }
        private void ButtonApplyTheme_Click(object sender, EventArgs e)
        {
                Getcolor();
        }
    }
}
