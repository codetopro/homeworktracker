﻿namespace Homework
{
    partial class HomePage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(HomePage));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panelTop = new System.Windows.Forms.Panel();
            this.buttonUserDetails = new System.Windows.Forms.Button();
            this.labelTime = new System.Windows.Forms.Label();
            this.labelUserNameWithComent = new System.Windows.Forms.Label();
            this.buttonMaximize = new System.Windows.Forms.Button();
            this.buttonMimize = new System.Windows.Forms.Button();
            this.buttonExit = new System.Windows.Forms.Button();
            this.panelRight = new System.Windows.Forms.Panel();
            this.buttonChangeColor = new System.Windows.Forms.Button();
            this.buttonAssignment = new System.Windows.Forms.Button();
            this.buttonHomework = new System.Windows.Forms.Button();
            this.buttonLogoff = new System.Windows.Forms.Button();
            this.buttonSettings = new System.Windows.Forms.Button();
            this.buttonCurrentStudying = new System.Windows.Forms.Button();
            this.buttonSubjects = new System.Windows.Forms.Button();
            this.buttonSemesters = new System.Windows.Forms.Button();
            this.buttonCalender = new System.Windows.Forms.Button();
            this.buttonSlider = new System.Windows.Forms.Button();
            this.panelSlogan = new System.Windows.Forms.Panel();
            this.labelSlogan = new System.Windows.Forms.Label();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.panelDetails = new System.Windows.Forms.Panel();
            this.buttonPnnelLeft = new System.Windows.Forms.Button();
            this.dataGridViewCurrentlyStuding = new System.Windows.Forms.DataGridView();
            this.dataGridViewSemesters = new System.Windows.Forms.DataGridView();
            this.dataGridViewSubjects = new System.Windows.Forms.DataGridView();
            this.contextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.showToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.logOffToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.notifyIcon = new System.Windows.Forms.NotifyIcon(this.components);
            this.panelBody = new System.Windows.Forms.Panel();
            this.panelTop.SuspendLayout();
            this.panelRight.SuspendLayout();
            this.panelSlogan.SuspendLayout();
            this.panelDetails.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewCurrentlyStuding)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewSemesters)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewSubjects)).BeginInit();
            this.contextMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelTop
            // 
            this.panelTop.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.panelTop.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelTop.Controls.Add(this.buttonUserDetails);
            this.panelTop.Controls.Add(this.labelTime);
            this.panelTop.Controls.Add(this.labelUserNameWithComent);
            this.panelTop.Controls.Add(this.buttonMaximize);
            this.panelTop.Controls.Add(this.buttonMimize);
            this.panelTop.Controls.Add(this.buttonExit);
            this.panelTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelTop.ForeColor = System.Drawing.Color.White;
            this.panelTop.Location = new System.Drawing.Point(0, 0);
            this.panelTop.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panelTop.Name = "panelTop";
            this.panelTop.Size = new System.Drawing.Size(1371, 48);
            this.panelTop.TabIndex = 0;
            this.panelTop.Click += new System.EventHandler(this.PanelTop_Click);
            this.panelTop.MouseDown += new System.Windows.Forms.MouseEventHandler(this.MouseToDown);
            this.panelTop.MouseMove += new System.Windows.Forms.MouseEventHandler(this.MouseToMove);
            this.panelTop.MouseUp += new System.Windows.Forms.MouseEventHandler(this.MouseToUp);
            // 
            // buttonUserDetails
            // 
            this.buttonUserDetails.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonUserDetails.BackColor = System.Drawing.Color.Transparent;
            this.buttonUserDetails.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.buttonUserDetails.FlatAppearance.BorderSize = 0;
            this.buttonUserDetails.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonUserDetails.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonUserDetails.ForeColor = System.Drawing.Color.White;
            this.buttonUserDetails.Image = global::Homework.Properties.Resources.Img;
            this.buttonUserDetails.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.buttonUserDetails.Location = new System.Drawing.Point(749, 2);
            this.buttonUserDetails.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonUserDetails.Name = "buttonUserDetails";
            this.buttonUserDetails.Size = new System.Drawing.Size(276, 41);
            this.buttonUserDetails.TabIndex = 17;
            this.buttonUserDetails.Text = "UserName";
            this.buttonUserDetails.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonUserDetails.UseCompatibleTextRendering = true;
            this.buttonUserDetails.UseVisualStyleBackColor = false;
            this.buttonUserDetails.Click += new System.EventHandler(this.ButtonUserDetails_Click);
            this.buttonUserDetails.MouseLeave += new System.EventHandler(this.ButtonUserDetails_MouseLeave);
            this.buttonUserDetails.MouseHover += new System.EventHandler(this.ButtonUserDetails_MouseHover);
            // 
            // labelTime
            // 
            this.labelTime.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelTime.AutoSize = true;
            this.labelTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTime.Location = new System.Drawing.Point(1030, 12);
            this.labelTime.Name = "labelTime";
            this.labelTime.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelTime.Size = new System.Drawing.Size(46, 20);
            this.labelTime.TabIndex = 3;
            this.labelTime.Text = "Time";
            // 
            // labelUserNameWithComent
            // 
            this.labelUserNameWithComent.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelUserNameWithComent.AutoSize = true;
            this.labelUserNameWithComent.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelUserNameWithComent.Location = new System.Drawing.Point(12, 12);
            this.labelUserNameWithComent.Name = "labelUserNameWithComent";
            this.labelUserNameWithComent.Size = new System.Drawing.Size(181, 20);
            this.labelUserNameWithComent.TabIndex = 1;
            this.labelUserNameWithComent.Text = "UserName and coment";
            // 
            // buttonMaximize
            // 
            this.buttonMaximize.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonMaximize.BackColor = System.Drawing.Color.Transparent;
            this.buttonMaximize.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.buttonMaximize.FlatAppearance.BorderSize = 0;
            this.buttonMaximize.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonMaximize.Image = global::Homework.Properties.Resources.Max;
            this.buttonMaximize.Location = new System.Drawing.Point(1244, 2);
            this.buttonMaximize.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonMaximize.Name = "buttonMaximize";
            this.buttonMaximize.Size = new System.Drawing.Size(53, 38);
            this.buttonMaximize.TabIndex = 2;
            this.buttonMaximize.TabStop = false;
            this.buttonMaximize.UseVisualStyleBackColor = false;
            this.buttonMaximize.Click += new System.EventHandler(this.ButtonMaximize_Click);
            this.buttonMaximize.MouseLeave += new System.EventHandler(this.ButtonMaximize_MouseLeave);
            this.buttonMaximize.MouseHover += new System.EventHandler(this.ButtonMaximize_MouseHover);
            // 
            // buttonMimize
            // 
            this.buttonMimize.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonMimize.BackColor = System.Drawing.Color.Transparent;
            this.buttonMimize.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.buttonMimize.FlatAppearance.BorderSize = 0;
            this.buttonMimize.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonMimize.Image = global::Homework.Properties.Resources.Min;
            this.buttonMimize.Location = new System.Drawing.Point(1184, 2);
            this.buttonMimize.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonMimize.Name = "buttonMimize";
            this.buttonMimize.Size = new System.Drawing.Size(53, 38);
            this.buttonMimize.TabIndex = 1;
            this.buttonMimize.TabStop = false;
            this.buttonMimize.UseVisualStyleBackColor = false;
            this.buttonMimize.Click += new System.EventHandler(this.ButtonMimize_Click);
            this.buttonMimize.MouseLeave += new System.EventHandler(this.ButtonMimize_MouseLeave);
            this.buttonMimize.MouseHover += new System.EventHandler(this.ButtonMimize_MouseHover);
            // 
            // buttonExit
            // 
            this.buttonExit.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonExit.BackColor = System.Drawing.Color.Transparent;
            this.buttonExit.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonExit.FlatAppearance.BorderSize = 0;
            this.buttonExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonExit.Image = ((System.Drawing.Image)(resources.GetObject("buttonExit.Image")));
            this.buttonExit.Location = new System.Drawing.Point(1304, 4);
            this.buttonExit.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonExit.Name = "buttonExit";
            this.buttonExit.Size = new System.Drawing.Size(53, 38);
            this.buttonExit.TabIndex = 0;
            this.buttonExit.TabStop = false;
            this.buttonExit.UseVisualStyleBackColor = false;
            this.buttonExit.Click += new System.EventHandler(this.ButtonExit_Click);
            this.buttonExit.MouseLeave += new System.EventHandler(this.ButtonExit_MouseLeave);
            this.buttonExit.MouseHover += new System.EventHandler(this.ButtonExit_MouseHover);
            // 
            // panelRight
            // 
            this.panelRight.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.panelRight.Controls.Add(this.buttonChangeColor);
            this.panelRight.Controls.Add(this.buttonAssignment);
            this.panelRight.Controls.Add(this.buttonHomework);
            this.panelRight.Controls.Add(this.buttonLogoff);
            this.panelRight.Controls.Add(this.buttonSettings);
            this.panelRight.Controls.Add(this.buttonCurrentStudying);
            this.panelRight.Controls.Add(this.buttonSubjects);
            this.panelRight.Controls.Add(this.buttonSemesters);
            this.panelRight.Controls.Add(this.buttonCalender);
            this.panelRight.Controls.Add(this.buttonSlider);
            this.panelRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelRight.Location = new System.Drawing.Point(1071, 48);
            this.panelRight.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panelRight.MaximumSize = new System.Drawing.Size(300, 0);
            this.panelRight.MinimumSize = new System.Drawing.Size(67, 0);
            this.panelRight.Name = "panelRight";
            this.panelRight.Size = new System.Drawing.Size(300, 668);
            this.panelRight.TabIndex = 1;
            this.panelRight.Click += new System.EventHandler(this.PanelRight_Click);
            // 
            // buttonChangeColor
            // 
            this.buttonChangeColor.BackColor = System.Drawing.Color.Transparent;
            this.buttonChangeColor.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.buttonChangeColor.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.buttonChangeColor.FlatAppearance.BorderSize = 0;
            this.buttonChangeColor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonChangeColor.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonChangeColor.ForeColor = System.Drawing.Color.White;
            this.buttonChangeColor.Image = ((System.Drawing.Image)(resources.GetObject("buttonChangeColor.Image")));
            this.buttonChangeColor.Location = new System.Drawing.Point(0, 494);
            this.buttonChangeColor.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonChangeColor.Name = "buttonChangeColor";
            this.buttonChangeColor.Size = new System.Drawing.Size(300, 58);
            this.buttonChangeColor.TabIndex = 24;
            this.buttonChangeColor.Text = "Change Theme";
            this.buttonChangeColor.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.buttonChangeColor.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.buttonChangeColor.UseVisualStyleBackColor = false;
            this.buttonChangeColor.Click += new System.EventHandler(this.ButtonChangeColor_Click);
            this.buttonChangeColor.MouseLeave += new System.EventHandler(this.ButtonChangeColor_MouseLeave);
            this.buttonChangeColor.MouseHover += new System.EventHandler(this.ButtonChangeColor_MouseHover);
            // 
            // buttonAssignment
            // 
            this.buttonAssignment.BackColor = System.Drawing.Color.Transparent;
            this.buttonAssignment.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.buttonAssignment.Dock = System.Windows.Forms.DockStyle.Top;
            this.buttonAssignment.FlatAppearance.BorderSize = 0;
            this.buttonAssignment.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonAssignment.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonAssignment.ForeColor = System.Drawing.Color.White;
            this.buttonAssignment.Image = global::Homework.Properties.Resources.Assignm;
            this.buttonAssignment.Location = new System.Drawing.Point(0, 348);
            this.buttonAssignment.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonAssignment.Name = "buttonAssignment";
            this.buttonAssignment.Size = new System.Drawing.Size(300, 58);
            this.buttonAssignment.TabIndex = 23;
            this.buttonAssignment.Text = "Assignment";
            this.buttonAssignment.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.buttonAssignment.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.buttonAssignment.UseVisualStyleBackColor = false;
            this.buttonAssignment.Click += new System.EventHandler(this.ButtonAssignment_Click);
            this.buttonAssignment.MouseLeave += new System.EventHandler(this.ButtonAssignment_MouseLeave);
            this.buttonAssignment.MouseHover += new System.EventHandler(this.ButtonAssignment_MouseHover);
            // 
            // buttonHomework
            // 
            this.buttonHomework.BackColor = System.Drawing.Color.Transparent;
            this.buttonHomework.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.buttonHomework.Dock = System.Windows.Forms.DockStyle.Top;
            this.buttonHomework.FlatAppearance.BorderSize = 0;
            this.buttonHomework.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonHomework.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonHomework.ForeColor = System.Drawing.Color.White;
            this.buttonHomework.Image = global::Homework.Properties.Resources.HWS;
            this.buttonHomework.Location = new System.Drawing.Point(0, 290);
            this.buttonHomework.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonHomework.Name = "buttonHomework";
            this.buttonHomework.Size = new System.Drawing.Size(300, 58);
            this.buttonHomework.TabIndex = 22;
            this.buttonHomework.Text = "Homework";
            this.buttonHomework.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.buttonHomework.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.buttonHomework.UseVisualStyleBackColor = false;
            this.buttonHomework.Click += new System.EventHandler(this.ButtonHomework_Click);
            this.buttonHomework.MouseLeave += new System.EventHandler(this.ButtonHomework_MouseLeave);
            this.buttonHomework.MouseHover += new System.EventHandler(this.ButtonHomework_MouseHover);
            // 
            // buttonLogoff
            // 
            this.buttonLogoff.BackColor = System.Drawing.Color.Transparent;
            this.buttonLogoff.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.buttonLogoff.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.buttonLogoff.FlatAppearance.BorderSize = 0;
            this.buttonLogoff.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonLogoff.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonLogoff.ForeColor = System.Drawing.Color.White;
            this.buttonLogoff.Image = global::Homework.Properties.Resources.logfinal;
            this.buttonLogoff.Location = new System.Drawing.Point(0, 552);
            this.buttonLogoff.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonLogoff.Name = "buttonLogoff";
            this.buttonLogoff.Size = new System.Drawing.Size(300, 58);
            this.buttonLogoff.TabIndex = 21;
            this.buttonLogoff.Text = "Log off";
            this.buttonLogoff.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.buttonLogoff.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.buttonLogoff.UseVisualStyleBackColor = false;
            this.buttonLogoff.Click += new System.EventHandler(this.ButtonLogoff_Click);
            this.buttonLogoff.MouseLeave += new System.EventHandler(this.ButtonLogoff_MouseLeave);
            this.buttonLogoff.MouseHover += new System.EventHandler(this.ButtonLogoff_MouseHover);
            // 
            // buttonSettings
            // 
            this.buttonSettings.BackColor = System.Drawing.Color.Transparent;
            this.buttonSettings.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.buttonSettings.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.buttonSettings.FlatAppearance.BorderSize = 0;
            this.buttonSettings.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSettings.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonSettings.ForeColor = System.Drawing.Color.White;
            this.buttonSettings.Image = global::Homework.Properties.Resources.Setting;
            this.buttonSettings.Location = new System.Drawing.Point(0, 610);
            this.buttonSettings.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonSettings.Name = "buttonSettings";
            this.buttonSettings.Size = new System.Drawing.Size(300, 58);
            this.buttonSettings.TabIndex = 20;
            this.buttonSettings.Text = "Settings";
            this.buttonSettings.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.buttonSettings.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.buttonSettings.UseVisualStyleBackColor = false;
            this.buttonSettings.Click += new System.EventHandler(this.ButtonSettings_Click);
            this.buttonSettings.MouseLeave += new System.EventHandler(this.ButtonSettings_MouseLeave);
            this.buttonSettings.MouseHover += new System.EventHandler(this.ButtonSettings_MouseHover);
            // 
            // buttonCurrentStudying
            // 
            this.buttonCurrentStudying.BackColor = System.Drawing.Color.Transparent;
            this.buttonCurrentStudying.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.buttonCurrentStudying.Dock = System.Windows.Forms.DockStyle.Top;
            this.buttonCurrentStudying.FlatAppearance.BorderSize = 0;
            this.buttonCurrentStudying.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonCurrentStudying.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonCurrentStudying.ForeColor = System.Drawing.Color.White;
            this.buttonCurrentStudying.Image = global::Homework.Properties.Resources.CurentSt;
            this.buttonCurrentStudying.Location = new System.Drawing.Point(0, 232);
            this.buttonCurrentStudying.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonCurrentStudying.Name = "buttonCurrentStudying";
            this.buttonCurrentStudying.Size = new System.Drawing.Size(300, 58);
            this.buttonCurrentStudying.TabIndex = 19;
            this.buttonCurrentStudying.Text = "Currently Studying";
            this.buttonCurrentStudying.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.buttonCurrentStudying.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.buttonCurrentStudying.UseVisualStyleBackColor = false;
            this.buttonCurrentStudying.Click += new System.EventHandler(this.ButtonCurrentStudying_Click);
            this.buttonCurrentStudying.MouseLeave += new System.EventHandler(this.ButtonCurrentStudying_MouseLeave);
            this.buttonCurrentStudying.MouseHover += new System.EventHandler(this.ButtonCurrentStudying_MouseHover);
            // 
            // buttonSubjects
            // 
            this.buttonSubjects.BackColor = System.Drawing.Color.Transparent;
            this.buttonSubjects.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.buttonSubjects.Dock = System.Windows.Forms.DockStyle.Top;
            this.buttonSubjects.FlatAppearance.BorderSize = 0;
            this.buttonSubjects.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSubjects.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonSubjects.ForeColor = System.Drawing.Color.White;
            this.buttonSubjects.Image = global::Homework.Properties.Resources.Subjs;
            this.buttonSubjects.Location = new System.Drawing.Point(0, 174);
            this.buttonSubjects.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonSubjects.Name = "buttonSubjects";
            this.buttonSubjects.Size = new System.Drawing.Size(300, 58);
            this.buttonSubjects.TabIndex = 18;
            this.buttonSubjects.Text = "Subjects";
            this.buttonSubjects.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.buttonSubjects.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.buttonSubjects.UseVisualStyleBackColor = false;
            this.buttonSubjects.Click += new System.EventHandler(this.ButtonSubjects_Click);
            this.buttonSubjects.MouseLeave += new System.EventHandler(this.ButtonSubjects_MouseLeave);
            this.buttonSubjects.MouseHover += new System.EventHandler(this.ButtonSubjects_MouseHover);
            // 
            // buttonSemesters
            // 
            this.buttonSemesters.BackColor = System.Drawing.Color.Transparent;
            this.buttonSemesters.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.buttonSemesters.Dock = System.Windows.Forms.DockStyle.Top;
            this.buttonSemesters.FlatAppearance.BorderSize = 0;
            this.buttonSemesters.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSemesters.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonSemesters.ForeColor = System.Drawing.Color.White;
            this.buttonSemesters.Image = global::Homework.Properties.Resources.Sems;
            this.buttonSemesters.Location = new System.Drawing.Point(0, 116);
            this.buttonSemesters.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonSemesters.Name = "buttonSemesters";
            this.buttonSemesters.Size = new System.Drawing.Size(300, 58);
            this.buttonSemesters.TabIndex = 17;
            this.buttonSemesters.Text = "Semesters";
            this.buttonSemesters.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.buttonSemesters.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.buttonSemesters.UseVisualStyleBackColor = false;
            this.buttonSemesters.Click += new System.EventHandler(this.ButtonSemesters_Click);
            this.buttonSemesters.MouseLeave += new System.EventHandler(this.ButtonSemesters_MouseLeave);
            this.buttonSemesters.MouseHover += new System.EventHandler(this.ButtonSemesters_MouseHover);
            // 
            // buttonCalender
            // 
            this.buttonCalender.BackColor = System.Drawing.Color.Transparent;
            this.buttonCalender.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.buttonCalender.Dock = System.Windows.Forms.DockStyle.Top;
            this.buttonCalender.FlatAppearance.BorderSize = 0;
            this.buttonCalender.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonCalender.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonCalender.ForeColor = System.Drawing.Color.White;
            this.buttonCalender.Image = global::Homework.Properties.Resources.Calendarfinal;
            this.buttonCalender.Location = new System.Drawing.Point(0, 58);
            this.buttonCalender.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonCalender.Name = "buttonCalender";
            this.buttonCalender.Size = new System.Drawing.Size(300, 58);
            this.buttonCalender.TabIndex = 16;
            this.buttonCalender.Text = "Calender";
            this.buttonCalender.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.buttonCalender.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.buttonCalender.UseVisualStyleBackColor = false;
            this.buttonCalender.Click += new System.EventHandler(this.ButtonCalender_Click);
            this.buttonCalender.MouseLeave += new System.EventHandler(this.ButtonCalender_MouseLeave);
            this.buttonCalender.MouseHover += new System.EventHandler(this.ButtonCalender_MouseHover);
            // 
            // buttonSlider
            // 
            this.buttonSlider.BackColor = System.Drawing.Color.Transparent;
            this.buttonSlider.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.buttonSlider.Dock = System.Windows.Forms.DockStyle.Top;
            this.buttonSlider.FlatAppearance.BorderSize = 0;
            this.buttonSlider.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSlider.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonSlider.ForeColor = System.Drawing.Color.White;
            this.buttonSlider.Image = global::Homework.Properties.Resources.SR;
            this.buttonSlider.Location = new System.Drawing.Point(0, 0);
            this.buttonSlider.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonSlider.Name = "buttonSlider";
            this.buttonSlider.Size = new System.Drawing.Size(300, 58);
            this.buttonSlider.TabIndex = 15;
            this.buttonSlider.Text = "Show";
            this.buttonSlider.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.buttonSlider.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.buttonSlider.UseVisualStyleBackColor = false;
            this.buttonSlider.Click += new System.EventHandler(this.ButtonSlider_Click);
            this.buttonSlider.MouseLeave += new System.EventHandler(this.ButtonSlider_MouseLeave);
            this.buttonSlider.MouseHover += new System.EventHandler(this.ButtonSlider_MouseHover);
            // 
            // panelSlogan
            // 
            this.panelSlogan.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.panelSlogan.Controls.Add(this.labelSlogan);
            this.panelSlogan.Location = new System.Drawing.Point(435, 683);
            this.panelSlogan.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panelSlogan.Name = "panelSlogan";
            this.panelSlogan.Size = new System.Drawing.Size(627, 30);
            this.panelSlogan.TabIndex = 2;
            this.panelSlogan.Click += new System.EventHandler(this.PanelSlogan_Click);
            // 
            // labelSlogan
            // 
            this.labelSlogan.AutoSize = true;
            this.labelSlogan.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.labelSlogan.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSlogan.Location = new System.Drawing.Point(0, 10);
            this.labelSlogan.Name = "labelSlogan";
            this.labelSlogan.Size = new System.Drawing.Size(496, 20);
            this.labelSlogan.TabIndex = 0;
            this.labelSlogan.Text = "Study more to know more. Lets start recording our achievements.";
            // 
            // timer
            // 
            this.timer.Enabled = true;
            this.timer.Tick += new System.EventHandler(this.Timer_Tick);
            // 
            // panelDetails
            // 
            this.panelDetails.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.panelDetails.Controls.Add(this.buttonPnnelLeft);
            this.panelDetails.Controls.Add(this.dataGridViewCurrentlyStuding);
            this.panelDetails.Controls.Add(this.dataGridViewSemesters);
            this.panelDetails.Controls.Add(this.dataGridViewSubjects);
            this.panelDetails.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelDetails.ForeColor = System.Drawing.Color.White;
            this.panelDetails.Location = new System.Drawing.Point(0, 48);
            this.panelDetails.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panelDetails.MaximumSize = new System.Drawing.Size(300, 0);
            this.panelDetails.Name = "panelDetails";
            this.panelDetails.Size = new System.Drawing.Size(289, 668);
            this.panelDetails.TabIndex = 0;
            // 
            // buttonPnnelLeft
            // 
            this.buttonPnnelLeft.BackColor = System.Drawing.Color.Transparent;
            this.buttonPnnelLeft.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.buttonPnnelLeft.Dock = System.Windows.Forms.DockStyle.Top;
            this.buttonPnnelLeft.FlatAppearance.BorderSize = 0;
            this.buttonPnnelLeft.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonPnnelLeft.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonPnnelLeft.ForeColor = System.Drawing.Color.White;
            this.buttonPnnelLeft.Location = new System.Drawing.Point(0, 0);
            this.buttonPnnelLeft.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonPnnelLeft.Name = "buttonPnnelLeft";
            this.buttonPnnelLeft.Size = new System.Drawing.Size(289, 81);
            this.buttonPnnelLeft.TabIndex = 17;
            this.buttonPnnelLeft.Text = "Pannel Left ";
            this.buttonPnnelLeft.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.buttonPnnelLeft.UseVisualStyleBackColor = false;
            this.buttonPnnelLeft.Click += new System.EventHandler(this.ButtonPnnelLeft_Click);
            this.buttonPnnelLeft.MouseLeave += new System.EventHandler(this.ButtonPnnelLeft_MouseLeave);
            this.buttonPnnelLeft.MouseHover += new System.EventHandler(this.ButtonPnnelLeft_MouseHover);
            // 
            // dataGridViewCurrentlyStuding
            // 
            this.dataGridViewCurrentlyStuding.AllowDrop = true;
            this.dataGridViewCurrentlyStuding.AllowUserToAddRows = false;
            this.dataGridViewCurrentlyStuding.AllowUserToDeleteRows = false;
            this.dataGridViewCurrentlyStuding.AllowUserToOrderColumns = true;
            this.dataGridViewCurrentlyStuding.AllowUserToResizeColumns = false;
            this.dataGridViewCurrentlyStuding.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Transparent;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.GradientActiveCaption;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewCurrentlyStuding.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridViewCurrentlyStuding.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewCurrentlyStuding.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridViewCurrentlyStuding.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.dataGridViewCurrentlyStuding.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridViewCurrentlyStuding.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.RaisedHorizontal;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.Transparent;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewCurrentlyStuding.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridViewCurrentlyStuding.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dataGridViewCurrentlyStuding.ColumnHeadersVisible = false;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.Transparent;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewCurrentlyStuding.DefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridViewCurrentlyStuding.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewCurrentlyStuding.Location = new System.Drawing.Point(0, 0);
            this.dataGridViewCurrentlyStuding.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dataGridViewCurrentlyStuding.MultiSelect = false;
            this.dataGridViewCurrentlyStuding.Name = "dataGridViewCurrentlyStuding";
            this.dataGridViewCurrentlyStuding.ReadOnly = true;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.Transparent;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewCurrentlyStuding.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dataGridViewCurrentlyStuding.RowHeadersVisible = false;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.Transparent;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.Black;
            this.dataGridViewCurrentlyStuding.RowsDefaultCellStyle = dataGridViewCellStyle5;
            this.dataGridViewCurrentlyStuding.RowTemplate.DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridViewCurrentlyStuding.RowTemplate.DefaultCellStyle.BackColor = System.Drawing.Color.White;
            this.dataGridViewCurrentlyStuding.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataGridViewCurrentlyStuding.RowTemplate.DefaultCellStyle.ForeColor = System.Drawing.Color.Black;
            this.dataGridViewCurrentlyStuding.RowTemplate.DefaultCellStyle.SelectionBackColor = System.Drawing.Color.Black;
            this.dataGridViewCurrentlyStuding.RowTemplate.DefaultCellStyle.SelectionForeColor = System.Drawing.Color.White;
            this.dataGridViewCurrentlyStuding.RowTemplate.Height = 24;
            this.dataGridViewCurrentlyStuding.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewCurrentlyStuding.Size = new System.Drawing.Size(289, 668);
            this.dataGridViewCurrentlyStuding.StandardTab = true;
            this.dataGridViewCurrentlyStuding.TabIndex = 19;
            this.dataGridViewCurrentlyStuding.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridViewCurrentlyStuding_CellClick);
            // 
            // dataGridViewSemesters
            // 
            this.dataGridViewSemesters.AllowDrop = true;
            this.dataGridViewSemesters.AllowUserToAddRows = false;
            this.dataGridViewSemesters.AllowUserToDeleteRows = false;
            this.dataGridViewSemesters.AllowUserToOrderColumns = true;
            this.dataGridViewSemesters.AllowUserToResizeColumns = false;
            this.dataGridViewSemesters.AllowUserToResizeRows = false;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.Transparent;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.GradientActiveCaption;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewSemesters.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle6;
            this.dataGridViewSemesters.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewSemesters.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewSemesters.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridViewSemesters.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.dataGridViewSemesters.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridViewSemesters.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.RaisedHorizontal;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.Transparent;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewSemesters.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.dataGridViewSemesters.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dataGridViewSemesters.ColumnHeadersVisible = false;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.Transparent;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewSemesters.DefaultCellStyle = dataGridViewCellStyle8;
            this.dataGridViewSemesters.Location = new System.Drawing.Point(0, 85);
            this.dataGridViewSemesters.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dataGridViewSemesters.MultiSelect = false;
            this.dataGridViewSemesters.Name = "dataGridViewSemesters";
            this.dataGridViewSemesters.ReadOnly = true;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.Color.Transparent;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewSemesters.RowHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.dataGridViewSemesters.RowHeadersVisible = false;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle10.BackColor = System.Drawing.Color.Transparent;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.Color.Black;
            this.dataGridViewSemesters.RowsDefaultCellStyle = dataGridViewCellStyle10;
            this.dataGridViewSemesters.RowTemplate.DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridViewSemesters.RowTemplate.DefaultCellStyle.BackColor = System.Drawing.Color.White;
            this.dataGridViewSemesters.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataGridViewSemesters.RowTemplate.DefaultCellStyle.ForeColor = System.Drawing.Color.Black;
            this.dataGridViewSemesters.RowTemplate.DefaultCellStyle.SelectionBackColor = System.Drawing.Color.Black;
            this.dataGridViewSemesters.RowTemplate.DefaultCellStyle.SelectionForeColor = System.Drawing.Color.White;
            this.dataGridViewSemesters.RowTemplate.Height = 24;
            this.dataGridViewSemesters.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewSemesters.Size = new System.Drawing.Size(289, 21959);
            this.dataGridViewSemesters.StandardTab = true;
            this.dataGridViewSemesters.TabIndex = 0;
            this.dataGridViewSemesters.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridViewSemesters_CellClick);
            // 
            // dataGridViewSubjects
            // 
            this.dataGridViewSubjects.AllowDrop = true;
            this.dataGridViewSubjects.AllowUserToAddRows = false;
            this.dataGridViewSubjects.AllowUserToDeleteRows = false;
            this.dataGridViewSubjects.AllowUserToOrderColumns = true;
            this.dataGridViewSubjects.AllowUserToResizeColumns = false;
            this.dataGridViewSubjects.AllowUserToResizeRows = false;
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle11.BackColor = System.Drawing.Color.Transparent;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle11.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.GradientActiveCaption;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewSubjects.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle11;
            this.dataGridViewSubjects.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewSubjects.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridViewSubjects.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.dataGridViewSubjects.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridViewSubjects.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.RaisedHorizontal;
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle12.BackColor = System.Drawing.Color.Transparent;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle12.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewSubjects.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle12;
            this.dataGridViewSubjects.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dataGridViewSubjects.ColumnHeadersVisible = false;
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle13.BackColor = System.Drawing.Color.Transparent;
            dataGridViewCellStyle13.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle13.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle13.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle13.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewSubjects.DefaultCellStyle = dataGridViewCellStyle13;
            this.dataGridViewSubjects.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewSubjects.Location = new System.Drawing.Point(0, 0);
            this.dataGridViewSubjects.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dataGridViewSubjects.MultiSelect = false;
            this.dataGridViewSubjects.Name = "dataGridViewSubjects";
            this.dataGridViewSubjects.ReadOnly = true;
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle14.BackColor = System.Drawing.Color.Transparent;
            dataGridViewCellStyle14.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle14.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle14.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle14.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewSubjects.RowHeadersDefaultCellStyle = dataGridViewCellStyle14;
            this.dataGridViewSubjects.RowHeadersVisible = false;
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle15.BackColor = System.Drawing.Color.Transparent;
            dataGridViewCellStyle15.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle15.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle15.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle15.SelectionForeColor = System.Drawing.Color.Black;
            this.dataGridViewSubjects.RowsDefaultCellStyle = dataGridViewCellStyle15;
            this.dataGridViewSubjects.RowTemplate.DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridViewSubjects.RowTemplate.DefaultCellStyle.BackColor = System.Drawing.Color.White;
            this.dataGridViewSubjects.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataGridViewSubjects.RowTemplate.DefaultCellStyle.ForeColor = System.Drawing.Color.Black;
            this.dataGridViewSubjects.RowTemplate.DefaultCellStyle.SelectionBackColor = System.Drawing.Color.Black;
            this.dataGridViewSubjects.RowTemplate.DefaultCellStyle.SelectionForeColor = System.Drawing.Color.White;
            this.dataGridViewSubjects.RowTemplate.Height = 24;
            this.dataGridViewSubjects.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewSubjects.Size = new System.Drawing.Size(289, 668);
            this.dataGridViewSubjects.StandardTab = true;
            this.dataGridViewSubjects.TabIndex = 18;
            this.dataGridViewSubjects.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridViewSubjects_CellClick);
            // 
            // contextMenuStrip
            // 
            this.contextMenuStrip.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.showToolStripMenuItem,
            this.logOffToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.contextMenuStrip.Name = "contextMenuStrip";
            this.contextMenuStrip.Size = new System.Drawing.Size(139, 88);
            // 
            // showToolStripMenuItem
            // 
            this.showToolStripMenuItem.BackColor = System.Drawing.Color.Transparent;
            this.showToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Bold);
            this.showToolStripMenuItem.Name = "showToolStripMenuItem";
            this.showToolStripMenuItem.Size = new System.Drawing.Size(138, 28);
            this.showToolStripMenuItem.Text = "Show";
            this.showToolStripMenuItem.Click += new System.EventHandler(this.ShowToolStripMenuItem_Click);
            // 
            // logOffToolStripMenuItem
            // 
            this.logOffToolStripMenuItem.BackColor = System.Drawing.Color.Transparent;
            this.logOffToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Bold);
            this.logOffToolStripMenuItem.Name = "logOffToolStripMenuItem";
            this.logOffToolStripMenuItem.Size = new System.Drawing.Size(138, 28);
            this.logOffToolStripMenuItem.Text = "Log off";
            this.logOffToolStripMenuItem.Click += new System.EventHandler(this.LogOffToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.BackColor = System.Drawing.Color.Transparent;
            this.exitToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Bold);
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F4)));
            this.exitToolStripMenuItem.ShowShortcutKeys = false;
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(138, 28);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.ExitToolStripMenuItem_Click);
            // 
            // notifyIcon
            // 
            this.notifyIcon.ContextMenuStrip = this.contextMenuStrip;
            this.notifyIcon.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon.Icon")));
            this.notifyIcon.Text = "Settings";
            this.notifyIcon.Visible = true;
            this.notifyIcon.DoubleClick += new System.EventHandler(this.NotifyIcon_DoubleClick);
            // 
            // panelBody
            // 
            this.panelBody.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelBody.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panelBody.BackgroundImage = global::Homework.Properties.Resources.graduate;
            this.panelBody.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.panelBody.Location = new System.Drawing.Point(295, 50);
            this.panelBody.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panelBody.Name = "panelBody";
            this.panelBody.Size = new System.Drawing.Size(1003, 629);
            this.panelBody.TabIndex = 3;
            this.panelBody.Click += new System.EventHandler(this.PanelBody_Click);
            // 
            // HomePage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ClientSize = new System.Drawing.Size(1371, 716);
            this.ControlBox = false;
            this.Controls.Add(this.panelDetails);
            this.Controls.Add(this.panelSlogan);
            this.Controls.Add(this.panelRight);
            this.Controls.Add(this.panelTop);
            this.Controls.Add(this.panelBody);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "HomePage";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Home ";
            this.Load += new System.EventHandler(this.HomePage_Load);
            this.Move += new System.EventHandler(this.HomePage_Move);
            this.panelTop.ResumeLayout(false);
            this.panelTop.PerformLayout();
            this.panelRight.ResumeLayout(false);
            this.panelSlogan.ResumeLayout(false);
            this.panelSlogan.PerformLayout();
            this.panelDetails.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewCurrentlyStuding)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewSemesters)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewSubjects)).EndInit();
            this.contextMenuStrip.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button buttonMaximize;
        private System.Windows.Forms.Button buttonMimize;
        private System.Windows.Forms.Button buttonExit;
        private System.Windows.Forms.Button buttonSlider;
        private System.Windows.Forms.Panel panelSlogan;
        private System.Windows.Forms.Label labelSlogan;
        private System.Windows.Forms.Label labelUserNameWithComent;
        private System.Windows.Forms.Label labelTime;
        private System.Windows.Forms.Timer timer;
        private System.Windows.Forms.Panel panelBody;
        private System.Windows.Forms.Button buttonCalender;
        private System.Windows.Forms.Button buttonCurrentStudying;
        private System.Windows.Forms.Button buttonSubjects;
        private System.Windows.Forms.Button buttonSemesters;
        private System.Windows.Forms.Button buttonSettings;
        private System.Windows.Forms.Button buttonLogoff;
        private System.Windows.Forms.Button buttonPnnelLeft;
        public System.Windows.Forms.DataGridView dataGridViewSemesters;
        public System.Windows.Forms.DataGridView dataGridViewSubjects;
        public System.Windows.Forms.DataGridView dataGridViewCurrentlyStuding;
        private System.Windows.Forms.Button buttonAssignment;
        private System.Windows.Forms.Button buttonHomework;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem showToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem logOffToolStripMenuItem;
        public System.Windows.Forms.Button buttonUserDetails;
        private System.Windows.Forms.NotifyIcon notifyIcon;
        private System.Windows.Forms.Panel panelRight;
        private System.Windows.Forms.Panel panelDetails;
        private System.Windows.Forms.Button buttonChangeColor;
        private System.Windows.Forms.Panel panelTop;
    }
}