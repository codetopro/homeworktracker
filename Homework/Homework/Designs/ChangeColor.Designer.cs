﻿namespace Homework
{
    partial class ChangeColor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ChangeColor));
            this.groupBoxTheme = new System.Windows.Forms.GroupBox();
            this.panelTest = new System.Windows.Forms.Panel();
            this.pictureBox646464 = new System.Windows.Forms.PictureBox();
            this.pictureBoxBlack = new System.Windows.Forms.PictureBox();
            this.pictureBoxMaroon = new System.Windows.Forms.PictureBox();
            this.pictureBoxNavy = new System.Windows.Forms.PictureBox();
            this.pictureBox64640 = new System.Windows.Forms.PictureBox();
            this.buttonApplyTheme = new System.Windows.Forms.Button();
            this.labelCollorNotinf = new System.Windows.Forms.Label();
            this.groupBoxTheme.SuspendLayout();
            this.panelTest.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox646464)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxBlack)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxMaroon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxNavy)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox64640)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBoxTheme
            // 
            this.groupBoxTheme.BackColor = System.Drawing.Color.Transparent;
            this.groupBoxTheme.Controls.Add(this.panelTest);
            this.groupBoxTheme.Controls.Add(this.buttonApplyTheme);
            this.groupBoxTheme.Controls.Add(this.labelCollorNotinf);
            this.groupBoxTheme.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBoxTheme.Location = new System.Drawing.Point(0, 0);
            this.groupBoxTheme.Name = "groupBoxTheme";
            this.groupBoxTheme.Size = new System.Drawing.Size(500, 192);
            this.groupBoxTheme.TabIndex = 3;
            this.groupBoxTheme.TabStop = false;
            // 
            // panelTest
            // 
            this.panelTest.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.panelTest.Controls.Add(this.pictureBox646464);
            this.panelTest.Controls.Add(this.pictureBoxBlack);
            this.panelTest.Controls.Add(this.pictureBoxMaroon);
            this.panelTest.Controls.Add(this.pictureBoxNavy);
            this.panelTest.Controls.Add(this.pictureBox64640);
            this.panelTest.Location = new System.Drawing.Point(8, 40);
            this.panelTest.Name = "panelTest";
            this.panelTest.Size = new System.Drawing.Size(486, 88);
            this.panelTest.TabIndex = 15;
            // 
            // pictureBox646464
            // 
            this.pictureBox646464.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.pictureBox646464.Location = new System.Drawing.Point(378, 14);
            this.pictureBox646464.Name = "pictureBox646464";
            this.pictureBox646464.Size = new System.Drawing.Size(59, 55);
            this.pictureBox646464.TabIndex = 4;
            this.pictureBox646464.TabStop = false;
            this.pictureBox646464.Click += new System.EventHandler(this.PictureBox646464_Click);
            // 
            // pictureBoxBlack
            // 
            this.pictureBoxBlack.BackColor = System.Drawing.Color.Black;
            this.pictureBoxBlack.Location = new System.Drawing.Point(293, 14);
            this.pictureBoxBlack.Name = "pictureBoxBlack";
            this.pictureBoxBlack.Size = new System.Drawing.Size(59, 55);
            this.pictureBoxBlack.TabIndex = 3;
            this.pictureBoxBlack.TabStop = false;
            this.pictureBoxBlack.Click += new System.EventHandler(this.PictureBoxBlack_Click);
            // 
            // pictureBoxMaroon
            // 
            this.pictureBoxMaroon.BackColor = System.Drawing.Color.Maroon;
            this.pictureBoxMaroon.Location = new System.Drawing.Point(208, 14);
            this.pictureBoxMaroon.Name = "pictureBoxMaroon";
            this.pictureBoxMaroon.Size = new System.Drawing.Size(59, 55);
            this.pictureBoxMaroon.TabIndex = 2;
            this.pictureBoxMaroon.TabStop = false;
            this.pictureBoxMaroon.Click += new System.EventHandler(this.PictureBoxMaroon_Click);
            // 
            // pictureBoxNavy
            // 
            this.pictureBoxNavy.BackColor = System.Drawing.Color.Navy;
            this.pictureBoxNavy.Location = new System.Drawing.Point(123, 14);
            this.pictureBoxNavy.Name = "pictureBoxNavy";
            this.pictureBoxNavy.Size = new System.Drawing.Size(59, 55);
            this.pictureBoxNavy.TabIndex = 1;
            this.pictureBoxNavy.TabStop = false;
            this.pictureBoxNavy.Click += new System.EventHandler(this.PictureBoxNavy_Click);
            // 
            // pictureBox64640
            // 
            this.pictureBox64640.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.pictureBox64640.Location = new System.Drawing.Point(38, 14);
            this.pictureBox64640.Name = "pictureBox64640";
            this.pictureBox64640.Size = new System.Drawing.Size(59, 55);
            this.pictureBox64640.TabIndex = 0;
            this.pictureBox64640.TabStop = false;
            this.pictureBox64640.Click += new System.EventHandler(this.PictureBox64640_Click);
            // 
            // buttonApplyTheme
            // 
            this.buttonApplyTheme.BackColor = System.Drawing.Color.Transparent;
            this.buttonApplyTheme.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonApplyTheme.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonApplyTheme.Font = new System.Drawing.Font("Century Gothic", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonApplyTheme.Location = new System.Drawing.Point(6, 134);
            this.buttonApplyTheme.Name = "buttonApplyTheme";
            this.buttonApplyTheme.Size = new System.Drawing.Size(269, 55);
            this.buttonApplyTheme.TabIndex = 12;
            this.buttonApplyTheme.TabStop = false;
            this.buttonApplyTheme.Text = "Apply Theme";
            this.buttonApplyTheme.UseVisualStyleBackColor = false;
            this.buttonApplyTheme.Click += new System.EventHandler(this.ButtonApplyTheme_Click);
            // 
            // labelCollorNotinf
            // 
            this.labelCollorNotinf.AutoSize = true;
            this.labelCollorNotinf.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCollorNotinf.Location = new System.Drawing.Point(9, 12);
            this.labelCollorNotinf.Name = "labelCollorNotinf";
            this.labelCollorNotinf.Size = new System.Drawing.Size(234, 25);
            this.labelCollorNotinf.TabIndex = 3;
            this.labelCollorNotinf.Text = "Manage Theme Color :";
            // 
            // ChangeColor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(500, 192);
            this.Controls.Add(this.groupBoxTheme);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ChangeColor";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Change Theme";
            this.groupBoxTheme.ResumeLayout(false);
            this.groupBoxTheme.PerformLayout();
            this.panelTest.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox646464)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxBlack)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxMaroon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxNavy)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox64640)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBoxTheme;
        private System.Windows.Forms.Panel panelTest;
        private System.Windows.Forms.PictureBox pictureBox646464;
        private System.Windows.Forms.PictureBox pictureBoxBlack;
        private System.Windows.Forms.PictureBox pictureBoxMaroon;
        private System.Windows.Forms.PictureBox pictureBoxNavy;
        private System.Windows.Forms.PictureBox pictureBox64640;
        private System.Windows.Forms.Button buttonApplyTheme;
        private System.Windows.Forms.Label labelCollorNotinf;
    }
}