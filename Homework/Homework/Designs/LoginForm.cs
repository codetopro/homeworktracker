﻿using Homework.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using LogicLayer;

namespace Homework
{
    public partial class LoginForm : Form
    {
        Logic logic = new Logic();
        public Color changecolor;

        public LoginForm()
        {
            InitializeComponent();
                textBoxPassword.Visible = false;
                    CheckboxShowHidePassword.Visible = false;
                buttonLogIn.Visible = false;
            labelPassword.Visible = false;
        }

        #region//Dragable Codes
        private bool _Dragging = false;
        private Point _start_point = new Point(0, 0);
        private void MouseToDown(object sender, MouseEventArgs e)
        {
            _Dragging = true;
                _start_point = new Point(e.X, e.Y);
        }

        private void MouseToUp(object sender, MouseEventArgs e)
        {
            _Dragging = false;
        }

        private void MouseToMove(object sender, MouseEventArgs e)
        {
            if (_Dragging)
            {
                Point p = PointToScreen(e.Location);
                    Location = new Point(p.X - this._start_point.X, p.Y - this._start_point.Y);
            }
        }

        #endregion

        private void ButtonExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void TextboxUserName_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar == (char)13) && textboxUserName.Text != "")
            {
                labelPassword.Visible = true;
                    textBoxPassword.Visible = true;
                        CheckboxShowHidePassword.Visible = true;
                    textBoxPassword.Clear();
                textBoxPassword.Focus();
            }
            else
            {
                labelPassword.Visible = false;
                    textBoxPassword.Visible = false;
                    CheckboxShowHidePassword.Visible = false;
                buttonLogIn.Visible = false;
            }
        }

        private void TextBoxPassword_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar == (char)13) && textBoxPassword.Text != "")
            {
                buttonLogIn.Visible = true;
                    buttonLogIn.Focus();
            }
            else
            {
                buttonLogIn.Visible = false;
            }
        }   

        private void CheckboxShowHidePassword_CheckedChanged(object sender, EventArgs e)
        {
            if (CheckboxShowHidePassword.Checked)
            {
                textBoxPassword.UseSystemPasswordChar = false;
                    CheckboxShowHidePassword.Image = Resources.Hide;
            }
            else
            {
                textBoxPassword.UseSystemPasswordChar = true;
                    CheckboxShowHidePassword.Image = Resources.Show;
            }
        }
   
        private void ButtonLogIn_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(textboxUserName.Text) && !string.IsNullOrEmpty(textBoxPassword.Text))
            {
                if (!string.IsNullOrWhiteSpace(textboxUserName.Text) && !string.IsNullOrWhiteSpace(textBoxPassword.Text))
                {
                   int login=Convert.ToInt32(logic.GetAccess(textboxUserName.Text, textBoxPassword.Text));
                    if (login!= 0)
                    {
                        HomePage page = new HomePage(login,changecolor);
                            page.Show();
                        this.Hide();
                    }
                    else
                    {
                        MessageBox.Show("Username or password incorrect!", "Warining");
                    }
                }
                else
                {
                    MessageBox.Show("Please enter username and password!","Warining");
                }
            }
            else
            {
                MessageBox.Show("Please enter username and password!", "Warining");
            }
           
        }

        private void LabelNoAccount_Click(object sender, EventArgs e)
        {
            Signup signup = new Signup();
                signup.Show();
            this.Hide();
        }
    }
}
