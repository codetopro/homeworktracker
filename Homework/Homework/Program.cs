﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace Homework
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new LoginForm());
           // 1,System.Drawing.Color.FromArgb(64, 64, 0)
        }
    }
}
