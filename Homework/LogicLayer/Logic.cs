﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

namespace LogicLayer
{
    public class Logic
    {
        public string SQLCON = System.Configuration.ConfigurationManager.ConnectionStrings["Connection"].ConnectionString.ToString();
        string GetAccessStatus, GetAccessId, GetAccessResult;
        public static string Generate()
        {
            int minilenth = 8;
                int maxlenth = 12;
                    string generat;
                        string charavailable = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUWXYZ123456789";
                            StringBuilder password = new StringBuilder();
                        Random random = new Random();
                    int passwordlength = random.Next(minilenth, maxlenth + 1);
                 while (passwordlength-- > 0){
              password.Append(charavailable[random.Next(charavailable.Length)]);}
            generat = password.ToString();
                return generat;
        }

      
        public static SqlConnection DBConnect()
        {
            var conn = new SqlConnection("Data Source=.;Initial Catalog=Homework;Integrated Security=True");
               if (conn.State != ConnectionState.Open){
                  conn.Open();}
            return conn;
        }

        public static DataTable GetTableByQuery(string SqlQuery)
        {
            try
            {
                    SqlCommand command = new SqlCommand();
                        command.Connection = DBConnect();
                            command.CommandText = SqlQuery;
                                command.CommandType = CommandType.Text;
                            SqlDataAdapter adapter =
                        new SqlDataAdapter(command);
                    DataTable dt = new DataTable();
                        adapter.Fill(dt);
                           return dt;
            }
            catch (Exception)
            {
                   throw;
            }

        }
        public static void ExecuteNonQuery(string SqlQuery)
        {   
            try
            {
                SqlCommand command = new SqlCommand();
                    command.Connection = DBConnect();
                        command.CommandText = SqlQuery;
                    command.CommandType = CommandType.Text;
                command.ExecuteNonQuery();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public string GetAccess(string username, string password)
        {
            using (SqlConnection con = new SqlConnection(SQLCON))
                    using (SqlCommand command = new SqlCommand("GetAccess", con)){
                        command.CommandType = CommandType.StoredProcedure;
                            command.Parameters.Add("@username", SqlDbType.NVarChar, 40).Value = username;
                                command.Parameters.Add("@password", SqlDbType.NVarChar, 40).Value = password;
                                    command.Parameters.Add("@status", SqlDbType.NVarChar, 20);
                                command.Parameters["@status"].Direction = ParameterDirection.Output;
                            command.Parameters.Add("@id", SqlDbType.Int);
                        command.Parameters["@id"].Direction = ParameterDirection.Output;
                    if (con.State == ConnectionState.Closed){
                        con.Open();
                           command.ExecuteNonQuery();
                    GetAccessStatus = command.Parameters["@status"].Value.ToString().Trim();
                    GetAccessId = command.Parameters["@id"].Value.ToString().Trim();
                        con.Close();
                         }}
                     if (GetAccessStatus == "Success"){
                 GetAccessResult = GetAccessId.ToString();}
               else{
             GetAccessResult ="0";}
            return GetAccessResult;
        }
        public static DataTable DisplayUserDetails()
        {
            return GetTableByQuery("Select UserId,FirstName,MiddleName,Surname,Gmail,Contact from StudentDetails");
        }
        public bool CreateAccount(string firstName,string middelName,string surName,string contact,
            string gmail,string userName,string password)
        {
            bool result= false;
                if (!result){
                    ExecuteNonQuery("INSERT INTO StudentDetails(FirstName,MiddleName,Surname,Gmail,Contact,Users,PasswordHash) VALUES('" + firstName+"','"+middelName+"','"+surName+"','"+gmail+"',"+contact+" ,'" +userName+ "','" +password+"')");
                      result = true;}
               else{
                    result = false;}
          return result;
        }

        public bool UpdateAccount(string firstName, string middelName, string surName, string contact,string gmail, string userName, string password,int Userid)
        {
            bool result = false;
            if (!result)
            {
                ExecuteNonQuery("UPDATE StudentDetails SET FirstName='" + firstName + "',MiddleName='" + middelName + "',Surname='" + surName + "',Gmail='" + gmail + "',Contact=" + contact + " ,Users='" + userName + "',PasswordHash='" + password + "' WHERE UserId="+Userid);
                result = true;
            }
            else
            {
                result = false;
            }
            return result;
        }

        public string GetFullName(int id)
        {
            string Name = "select FirstName+' '+MiddleName+' '+Surname from StudentDetails where UserId ="+id;
            var data = GetTableByQuery(Name);  
            return (data.Rows[0][0]).ToString();
        }

        public static DataTable DisplaySemesterDetails()
        {
           return GetTableByQuery("select SemesterId,SemesterName from Semester");
        }
        public bool UpdateSemester(string semesterName,int Current,int completed,string id)
        {
            bool result = false;
            if (!result)
            {
                ExecuteNonQuery("UPDATE Semester set SemesterName='"+ semesterName + "',IsCurrentSemester="+ Current + ",IsCompleted="+ completed + " where SemesterId="+ id);
                result = true;
            }
            else
            {
                result = false;
            }
            return result;
        }
        public bool InsertSemester(string semesterName, int Current, int completed)
        {
            bool result = false;
            if (!result)
            {
                ExecuteNonQuery("INSERT INTO Semester(SemesterName,IsCurrentSemester,IsCompleted) VALUES('" + semesterName + "'," + Current + "," + completed + ")");
                result = true;
            }
            else
            {
                result = false;
            }
            return result;
        }

        public static DataTable DisplaySubjectDetails()
        {
            return GetTableByQuery("select  SubjectId,SubjectName from Subjects ");
        }
        public bool InsertSubject(string Subject, string teacher , int SemesterId)
        {
            bool result = false;
            if (!result)
            {
                ExecuteNonQuery("INSERT INTO Subjects(SubjectName,SubjecTeacher,SemesterId) VALUES ('" + Subject + "','" + teacher + "'," + SemesterId + ")");
                result = true;
            }
            else
            {
                result = false;
            }
            return result;
        }
        public bool UpdateSubject(string Subject, string teacher, int SemesterId,string SubjectId)
        {
            bool result = false;
            if (!result)
            {
                ExecuteNonQuery("UPDATE Subjects set  SubjectName='"+ Subject + "',SubjecTeacher='"+ teacher + "',SemesterId="+ SemesterId + " WHERE SubjectId=" + SubjectId);
                result = true;
            }
            else
            {
                result = false;
            }
            return result;
        }
        public static DataTable DisplayCurrentlyStudingDetails(string id)
        {
            return GetTableByQuery("SELECT SubjectId,SubjectName FROM Subjects WHERE SemesterId=" + id);
        }
        public static DataTable DisplayHomeworkDetails(string SemesterId)
        {
            return GetTableByQuery("SELECT H.DailyHomeworksId,S.SubjectName,H.HomeworkTitle,H.AssignedDate,H.CompletedDate FROM DailyHomeworks H INNER JOIN Subjects S ON S.SubjectId=H.SubjectId  WHERE S.SemesterId="+SemesterId+ " ORDER BY H.DailyHomeworksId DESC");
        }
        public static DataTable DisplayHomeworkDetails(int SubjectId)
        {
            return GetTableByQuery("SELECT H.DailyHomeworksId,S.SubjectName,H.HomeworkTitle,H.AssignedDate,H.CompletedDate FROM DailyHomeworks H INNER JOIN Subjects S ON S.SubjectId=H.SubjectId WHERE S.SubjectId="+SubjectId+ " ORDER BY H.DailyHomeworksId DESC");
        }
        public static DataTable DisplayHomeworkDetails(DateTime AssignedDate)
        {
            return GetTableByQuery("SELECT H.DailyHomeworksId,S.SubjectName,H.HomeworkTitle,H.AssignedDate,H.CompletedDate FROM DailyHomeworks H INNER JOIN Subjects S ON S.SubjectId=H.SubjectId  WHERE H.AssignedDate = ' " + AssignedDate + " ' ORDER BY H.DailyHomeworksId DESC");
        }

        public bool InsertHomework(int SubjectId, string HomeworkTitle, string Homework)
        {
            bool result = false;
            if (!result)
            {
                ExecuteNonQuery("INSERT INTO DailyHomeworks (SubjectId,HomeworkTitle,Homework) VALUES ("+SubjectId+",'"+HomeworkTitle+"','"+Homework+"')");
                result = true;
            }
            else
            {
                result = false;
            }
            return result;
        }
        public bool UpdateHomework(int SubjectId, string HomeworkTitle, string Homework,DateTime Completed, int IScompleted,int HomeworkId)
        {
            bool result = false;
            if (!result)
            {  
                ExecuteNonQuery(" UPDATE DailyHomeworks SET SubjectId ="+ SubjectId + " , HomeworkTitle = '"+ HomeworkTitle + "', Homework = '"+ Homework + "', CompletedDate = '"+Completed+"', IsCompleted ="+IScompleted+" WHERE DailyHomeworksId ="+HomeworkId);
                result = true;
            }
            else
            {
                result = false;
            }
            return result;
        }

        public static DataTable DisplayAssignmentDetails(string SemesterId)
        {
            return GetTableByQuery("SELECT A.AssignmentId,S.SubjectName,A.AssignmentTitle,A.AssignedDate,A.CompletedDate FROM Assignment A INNER JOIN Subjects S ON S.SubjectId=A.SubjectId  WHERE S.SemesterId=" + SemesterId + " ORDER BY A.AssignmentId DESC");
        }
        public static DataTable DisplayAssignmentDetails(int SubjectId)
        {
            return GetTableByQuery("SELECT A.AssignmentId,S.SubjectName,A.AssignmentTitle,A.AssignedDate,A.CompletedDate FROM Assignment A INNER JOIN Subjects S ON S.SubjectId=A.SubjectId WHERE S.SubjectId=" + SubjectId + " ORDER BY A.AssignmentId DESC");
        }
        public static DataTable DisplayAssignmentDetails(DateTime AssignedDate)
        {
            return GetTableByQuery("SELECT A.AssignmentId,S.SubjectName,A.AssignmentTitle,A.AssignedDate,A.CompletedDate FROM Assignment A INNER JOIN Subjects S ON S.SubjectId=A.SubjectId WHERE A.AssignedDate = ' " + AssignedDate + " ' ORDER BY A.AssignmentId DESC");
        }

        public bool InsertAssignment(int SubjectId, string AssignmentTitle, string Assignment)
        {
            bool result = false;
            if (!result)
            {
                ExecuteNonQuery("INSERT INTO Assignment(SubjectId,AssignmentTitle,Assignment) VALUES (" + SubjectId + ",'" + AssignmentTitle + "','" + Assignment + "')");
                result = true;
            }
            else
            {
                result = false;
            }
            return result;
        }

        public bool UpdateAssignment(int SubjectId, string AssignmentTitle, string Assignment, DateTime Completed, int IScompleted, int AssignmentID,string FileLocation)
        {
            bool result = false;
            if (!result)
            {
                ExecuteNonQuery("UPDATE Assignment SET SubjectId =" + SubjectId + " , AssignmentTitle = '"+AssignmentTitle+"', Assignment = '"+Assignment+"', CompletedDate = '"+ Completed + "', IsCompleted ="+ IScompleted + ",FileLocation='"+ FileLocation + "' WHERE AssignmentId = " + AssignmentID);
                result = true;
            }
            else
            {
                result = false;
            }
            return result;
        }

        /* public bool IntegerValidatior(string input)// valading the string
         {
             string pattern = "[^0-9]";//Regex = regular expression  //search for an string pattern
                 if (Regex.IsMatch(input, pattern))//comparing input and pattern
                     return true;
                else
                     return false;
         }
         public int GetNumber(string input)
         {
           string FinalStringGetNumber;
             int outputNumberofGetNumber;
                 int lengthOfString = input.Length;
                    char[] NewString = input.ToCharArray();
                        string pattern = "[^0-9]";
                            for (int i = 0; i < lengthOfString; i++)
                            {
                                if (Regex.IsMatch(NewString[i].ToString(), pattern))
                                    {
                                FinalStringGetNumber += NewString[i];
                              break;
                                     }
                            }
             outputNumberofGetNumber = Convert.ToInt32(FinalStringGetNumber);
           return outputNumberofGetNumber;
         }Test codes
                 string hasing = "H0mEwo@K";
        string Hasedvalue;
        private void Hasing(object sender,EventArgs e)
        {

            byte[] data = UTF8Encoding.UTF8.GetBytes(textBoxRetypePassword.Text);
            using (MD5CryptoServiceProvider md5=new MD5CryptoServiceProvider())
            {
                byte[] keys = md5.ComputeHash(UTF8Encoding.UTF8.GetBytes(hasing));
                using (TripleDESCryptoServiceProvider triple=new TripleDESCryptoServiceProvider() { Key=keys,Mode=CipherMode.ECB,Padding=PaddingMode.PKCS7})
                {
                    ICryptoTransform transform = triple.CreateEncryptor();
                    byte[] result = transform.TransformFinalBlock(data, 0, data.Length);
                    Hasedvalue = Convert.ToBase64String(result, 0, result.Length);
                }
            }
        }
         */
    }

}
