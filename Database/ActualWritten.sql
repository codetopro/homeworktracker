USE [master]
GO
EXEC master.sys.xp_create_subdir 'D:\Homework\Database\'
GO
EXEC master.sys.xp_create_subdir 'D:\Homework\AssignmentFiles\'
GO
DROP DATABASE IF EXISTS Homework
GO
USE [master]
GO
/****** Object:  Database Homework    Script Date: 4/24/2019 1:54:16 PM ******/
CREATE DATABASE Homework
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Homework', FILENAME = N'D:\Homework\Database\Homework.mdf' , SIZE = 12288KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB ), 
 FILEGROUP [Secondary] 
( NAME = N'Homework_Secondary', FILENAME = N'D:\Homework\Database\Homework_Secondary.ndf' , SIZE = 12288KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB ), 
 FILEGROUP [Ternary] 
( NAME = N'Homework_Ternary', FILENAME = N'D:\Homework\Database\Homework_Ternary.ndf' , SIZE = 12288KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'Homework_log', FILENAME = N'D:\Homework\Database\Homework_log.ldf' , SIZE = 12288KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE Homework SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC Homework.[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE Homework SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE Homework SET ANSI_NULLS OFF 
GO
ALTER DATABASE Homework SET ANSI_PADDING OFF 
GO
ALTER DATABASE Homework SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE Homework SET ARITHABORT OFF 
GO
ALTER DATABASE Homework SET AUTO_CLOSE OFF 
GO
ALTER DATABASE Homework SET AUTO_SHRINK OFF 
GO
ALTER DATABASE Homework SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE Homework SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE Homework SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE Homework SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE Homework SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE Homework SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE Homework SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE Homework SET  DISABLE_BROKER 
GO
ALTER DATABASE Homework SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE Homework SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE Homework SET TRUSTWORTHY OFF 
GO
ALTER DATABASE Homework SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE Homework SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE Homework SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE Homework SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE Homework SET RECOVERY FULL 
GO
ALTER DATABASE Homework SET  MULTI_USER 
GO
ALTER DATABASE Homework SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE Homework SET DB_CHAINING OFF 
GO
ALTER DATABASE Homework SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE Homework SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE Homework SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE Homework SET QUERY_STORE = OFF
GO
GO
USE Homework
GO
/*====================================================================================*/
/*						Foreign Key: FK_Subjects_REFRENCE_Semester 					  */
/*====================================================================================*/		
IF EXISTS (SELECT 1
	FROM sys.sysreferences R JOIN sys.sysobjects O ON (O.id =R.constid AND O.type='F')
	WHERE R.fkeyid=OBJECT_ID('Subjects') AND O.name='FK_Subjects_REFRENCE_Semester')
ALTER TABLE Subjects
	DROP CONSTRAINT FK_Subjects_REFRENCE_Semester
GO
/*====================================================================================*/
/*						Foreign Key: FK_DailyHomeworks_REFRENCE_Subjects 			  */
/*====================================================================================*/		
IF EXISTS (SELECT 1
	FROM sys.sysreferences R JOIN sys.sysobjects O ON (O.id =R.constid AND O.type='F')
	WHERE R.fkeyid=OBJECT_ID('DailyHomeworks') AND O.name='FK_DailyHomeworks_REFRENCE_Subjects')
ALTER TABLE DailyHomeworks
	DROP CONSTRAINT FK_DailyHomeworks_REFRENCE_Subjects
GO
/*====================================================================================*/
/*						Foreign Key: FK_Assignment_REFRENCE_Subjects 				  */
/*====================================================================================*/		
IF EXISTS (SELECT 1
	FROM sys.sysreferences R JOIN sys.sysobjects O ON (O.id =R.constid AND O.type='F')
	WHERE R.fkeyid=OBJECT_ID('Assignment') AND O.name='FK_Assignment_REFRENCE_Subjects')
ALTER TABLE Assignment
	DROP CONSTRAINT FK_Assignment_REFRENCE_Subjects
GO

/*====================================================================================*/
/*								INDEX: IndexStudentDetails 							  */
/*====================================================================================*/
IF EXISTS (SELECT 1
			FROM sysindexes
			WHERE id=OBJECT_ID('StudentDetails')
			AND name ='IndexStudentDetails'
			AND indid>0
			AND indid<255)
	DROP INDEX StudentDetails.IndexStudentDetails
GO
IF EXISTS (SELECT 1
			FROM sysobjects
			WHERE id=OBJECT_ID('StudentDetails')
			AND type='U')
	DROP TABLE StudentDetails
GO
/*====================================================================================*/
/*								INDEX: IndexSemester 					    		  */
/*====================================================================================*/
IF EXISTS (SELECT 1
			FROM sysindexes
			WHERE id=OBJECT_ID('Semester')
			AND name ='IndexSemester'
			AND indid>0
			AND indid<255)
	DROP INDEX Semester.IndexSemester
GO
IF EXISTS (SELECT 1
			FROM sysobjects
			WHERE id=OBJECT_ID('Semester')
			AND type='U')
	DROP TABLE Semester
GO
/*====================================================================================*/
/*								INDEX: IndexSubjects 					    		  */
/*====================================================================================*/
IF EXISTS (SELECT 1
			FROM sysindexes
			WHERE id=OBJECT_ID('Subjects')
			AND name ='IndexSubjects'
			AND indid>0
			AND indid<255)
	DROP INDEX Subjects.IndexSubjects
GO
IF EXISTS (SELECT 1
			FROM sysobjects
			WHERE id=OBJECT_ID('Subjects')
			AND type='U')
	DROP TABLE Subjects
GO
/*====================================================================================*/
/*								INDEX: IndexDailyHomeworks 					   		  */
/*====================================================================================*/
IF EXISTS (SELECT 1
			FROM sysindexes
			WHERE id=OBJECT_ID('DailyHomeworks')
			AND name ='IndexDailyHomeworks'
			AND indid>0
			AND indid<255)
	DROP INDEX DailyHomeworks.IndexDailyHomeworks
GO
IF EXISTS (SELECT 1
			FROM sysobjects
			WHERE id=OBJECT_ID('DailyHomeworks')
			AND type='U')
	DROP TABLE DailyHomeworks
GO
/*====================================================================================*/
/*								INDEX: IndexAssignment 					    		  */
/*====================================================================================*/
IF EXISTS (SELECT 1
			FROM sysindexes
			WHERE id=OBJECT_ID('Assignment')
			AND name ='IndexAssignment'
			AND indid>0
			AND indid<255)
	DROP INDEX Assignment.IndexAssignment
GO
IF EXISTS (SELECT 1
			FROM sysobjects
			WHERE id=OBJECT_ID('Assignment')
			AND type='U')
	DROP TABLE Assignment
GO

/*====================================================================================*/
/*								Table: StudentDetails      				 			  */
/*====================================================================================*/
CREATE TABLE StudentDetails(
		UserId      INT		IDENTITY(1,1)		PRIMARY KEY ,
		FirstName	NVARCHAR(40)	NOT NULL,
		MiddleName	NVARCHAR(40)	NULL,
		Surname		NVARCHAR(40)	NOT NULL,
		Gmail       NVARCHAR(120)	NOT NULL UNIQUE,
		Contact		BIGINT			NOT NULL UNIQUE,
		Users	    VARCHAR(300)	NOT NULL UNIQUE,
	    PasswordHash	VARCHAR(300)	NOT NULL
);
GO
/*====================================================================================*/
/*								Index: IndexStudentDetails             				  */
/*====================================================================================*/
CREATE INDEX IndexStudentDetails ON StudentDetails(
FirstName ASC,
Gmail ASC,
Contact ASC
);
GO

/*====================================================================================*/
/*								Table: Semester         				 			  */
/*====================================================================================*/
CREATE TABLE Semester(
		SemesterId      INT				PRIMARY KEY IDENTITY(1,1),
		SemesterName	NVARCHAR(50)	NOT NULL UNIQUE,
		IsCurrentSemester BIT			NOT NULL DEFAULT 0,
		IsCompleted		BIT				NOT NULL DEFAULT 0
);
GO
/*====================================================================================*/
/*								Index: IndexSemester                				  */
/*====================================================================================*/
CREATE INDEX IndexSemester ON Semester(
SemesterId ASC,
SemesterName ASC
);
GO

/*====================================================================================*/
/*								Table: Subject         				 			  */
/*====================================================================================*/
CREATE TABLE Subjects(
		SubjectId      INT				PRIMARY KEY IDENTITY(1,1),
		SubjectName   NVARCHAR(70)	    NOT NULL UNIQUE,
		SubjecTeacher  NVARCHAR(70)	    NOT NULL,
		SemesterId		INT				NOT NULL 
);
GO
/*====================================================================================*/
/*								Index: IndexSubjects                				  */
/*====================================================================================*/
CREATE INDEX IndexSubjects ON Subjects(
SemesterId ASC,
SubjectName ASC
);
GO

/*====================================================================================*/
/*								Table: DailyHomeworks         			 			  */
/*====================================================================================*/
CREATE TABLE DailyHomeworks(
		DailyHomeworksId      INT				PRIMARY KEY IDENTITY(1,1),
		SubjectId			  INT				NOT NULL ,
		HomeworkTitle		  NVARCHAR(70)	    NOT NULL,
		Homework			  NVARCHAR(MAX)	    NOT NULL,
		AssignedDate		  DATE				NOT NULL DEFAULT GETDATE(),
		CompletedDate		  DATE				NULL,
		IsCompleted			  BIT				NOT NULL DEFAULT 0
		
);
GO
/*====================================================================================*/
/*								Index: IndexDailyHomeworks                			  */
/*====================================================================================*/
CREATE INDEX IndexDailyHomeworks ON DailyHomeworks(
DailyHomeworksId ASC,
SubjectId ASC,
IsCompleted ASC
);
GO

/*====================================================================================*/
/*								Table: Assignment           			 			  */
/*====================================================================================*/
CREATE TABLE Assignment(
		AssignmentId		  INT				PRIMARY KEY IDENTITY(1,1),
		SubjectId			  INT				NOT NULL ,
		AssignmentTitle		  NVARCHAR(70)	    NOT NULL,
		Assignment			  NVARCHAR(MAX)	    NOT NULL,
		AssignedDate		  DATE			    NOT NULL DEFAULT GETDATE(),
		CompletedDate		  DATE			    NULL,
		IsCompleted			  BIT				NOT NULL DEFAULT 0,
		FileLocation		  NVARCHAR(MAX)	    NULL
);
GO
/*====================================================================================*/
/*								Index: IndexAssignment                				  */
/*====================================================================================*/
CREATE INDEX IndexAssignment ON Assignment(
AssignmentId ASC,
SubjectId ASC,
IsCompleted ASC
);
GO

/*====================================================================================*/
/*								Foraign Keys                        				  */
/*====================================================================================*/
ALTER TABLE Subjects
	ADD CONSTRAINT FK_Subjects_REFRENCE_Semester FOREIGN KEY (SemesterId)
		REFERENCES Semester (SemesterId)
GO
ALTER TABLE DailyHomeworks
	ADD CONSTRAINT FK_DailyHomeworks_REFRENCE_Subjects FOREIGN KEY (SubjectId)
		REFERENCES Subjects (SubjectId)
GO
ALTER TABLE Assignment
	ADD CONSTRAINT FK_Assignment_REFRENCE_Subjects FOREIGN KEY (SubjectId)
		REFERENCES Subjects (SubjectId)
GO

/*====================================================================================*/
/*								Stored Procedures                      				  */
/*====================================================================================*/
CREATE PROCEDURE GetAccess 
(
		@username nvarchar(50) ,
		@password NVARCHAR(50),
		@status nvarchar(20) OUTPUT,
		@id int output
)
AS
BEGIN 
		If exists (Select Users 
			from StudentDetails where 
				Users=@username 
					and
						PasswordHash=@password)
				set @status='Success'
		ELSE
				set @status='Failure'
			print @status
		Select @id=UserId 
			from StudentDetails 
				where Users=@username 
					and
				PasswordHash=@password
			print @id
END
GO 
CREATE PROCEDURE GetUserInfo
(	
		@FirstName NVARCHAR(70) OUTPUT,
		@MiddelName NVARCHAR(70) OUTPUT,
		@SurName NVARCHAR(70) OUTPUT,
		@Gmail NVARCHAR(90) OUTPUT,
		@Contact NVARCHAR(40) OUTPUT,
		@Username NVARCHAR(40) OUTPUT,
		@UserId int
)
AS
BEGIN
	SELECT @FirstName=FirstName,@MiddelName=MiddleName,@SurName=Surname,@Gmail=Gmail,@Contact=Contact,@Username=Users FROM StudentDetails WHERE UserId=@UserId
	Print @FirstName
	Print @MiddelName
	Print @SurName
	Print @Gmail
	Print @Contact
	Print @Username
END
